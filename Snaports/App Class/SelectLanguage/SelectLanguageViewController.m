//
//  SelectLanguageViewController.m
//  Snaports
//
//  Created by mac-00018 on 23/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SelectLanguageViewController.h"
#import "SelectLanguageTableViewCell.h"
#import "LoginViewController.h"

@interface SelectLanguageViewController ()
{
    NSArray *arrLanguage;
    NSString *strLanguageSelected;
}

@end

@implementation SelectLanguageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setData];
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Click Action
- (IBAction)btnConfirmClick:(id)sender {
    
    LoginViewController *vcLogin = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    [self.navigationController pushViewController:vcLogin animated:YES];
    

}

//- (IBAction)btnToggleSelected:(id)sender {
//    
//    UIButton *btn = (UIButton *)sender;
//    strLanguageSelected = [arrLanguage objectAtIndex:btn.tag];
//    [self.tblSelectLanguage reloadData];
//}


#pragma mark - TableView Delegate and DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.152 * CScreenWidth;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrLanguage.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *strCellId = @"Cell";
    
    SelectLanguageTableViewCell *cell = (SelectLanguageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:strCellId];
    
    if (cell == nil) {
        
        NSArray *nib =[[NSBundle mainBundle] loadNibNamed:@"SelectLanguageTableViewCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
        
    }
    
    NSString *strCurrent = [arrLanguage objectAtIndex:indexPath.row];
    cell.lblName.text = CLocalize(strCurrent);
    cell.imgFlag.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@Logo.png", strCurrent]];
    
    cell.btnSelect.selected = NO;
//    if ([strLanguageSelected isEqualToString:strCurrent]) {
//        cell.btnSelect.selected = YES;
//    }
    cell.btnSelect.selected = [strCurrent isEqualToString:strLanguageSelected];
    [cell.btnSelect touchUpInsideClicked:^{
        
        cell.btnSelect.selected = !cell.btnSelect.selected;
        strLanguageSelected = strCurrent;
        [self UpdateValues];

    }];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    SelectLanguageTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.btnSelect.selected = !cell.btnSelect.selected;
    strLanguageSelected = [arrLanguage objectAtIndex:indexPath.row];
    [self UpdateValues];

}

- (void)UpdateValues
{
    //Set updated value for selected language
    
    if ([strLanguageSelected isEqualToString:CArabic]) {
        appDelegate.isArabic = YES;
        self.lblSelectLanguage.textAlignment = NSTextAlignmentRight;
    }
    else
    {
        appDelegate.isArabic = NO;
        self.lblSelectLanguage.textAlignment = NSTextAlignmentLeft;

    }
    [appDelegate changeLanguage];

    self.lblSelectLanguage.text = CSelectLaguage;
    [self.btnConfirm setTitle:CConfirm forState:UIControlStateNormal];
    
    [self.tblSelectLanguage reloadData]; //Deselct rest all values


}


- (void)setData
{
    
    arrLanguage = @[@"English", @"Arabic"];
//    strLanguageSelected = @"English";

    strLanguageSelected = [appDelegate getLanguage];
    [self.tblSelectLanguage reloadData];
    
    self.tblSelectLanguage.layer.borderColor = CRGB(230, 231, 232).CGColor;
    self.tblSelectLanguage.layer.borderWidth = 1.0;
    
    
    self.layoutTblHight.constant = arrLanguage.count * 0.152 * CScreenWidth ;
}
@end
