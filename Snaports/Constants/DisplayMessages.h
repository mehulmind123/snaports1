
// # MI-r3


#define CMessageErrorSlowInternet @"Please check your internet connection. and try again later!"
#define CMessageCongratulation @"Congratulations!"
#define CMessageSorry @"Sorry!"
#define CMessageLoading @"Loading..."
#define CMessageSearching @"Searching..."
#define CMessageVerifying @"Verifying..."
#define CMessageWait @"Please Wait..."
#define CMessageAuthenticating @"Authenticating..."

#define CMessageProfilePicture @"Please select profile picture."
#define CMessageSelectGroup @"Please select group."
#define CMessageLogin @"Please enter email & username."
#define CMessageLoginFirst @"Please login to access this."


#define CMessageLastName @"Please enter last name."
#define CMessageMobileNo @"Please enter mobile number."
#define CMessageGender @"Please select gender."
#define CMessageAccept @"Please accept terms and conditions."


//--------------Email-----------
#define CMessageEmail                   CLocalize(@"Please enter your email.")

#define CMessageValidEmail              CLocalize(@"Please enter a valid email address.")
#define CMessageUsedEmail               CLocalize(@"Email is not registered with us. Please enter correct email address or register yourself.")
#define CUnregisteredEmail              CLocalize(@"Email is not registered with us.")
#define CUsedEmail                      CLocalize(@"Email is already registered with us. Please enter unique email address.")

//---------------Name------------------
#define CMessageFirstName               CLocalize(@"First name can not be blank.")
#define CMessageValidUsername           CLocalize(@"Username is invalid. Should have minimum 6 alphanumeric characters.")

//-------------Password and OTP--------------
#define CMessageVerificationCode        CLocalize(@"Please enter received OTP.")
#define CMessagePassword                CLocalize(@"Password can not be blank.")
#define CConfirmPassword                CLocalize(@"Confirm password can not be blank.")
#define CMessageValidOTP                CLocalize(@"Please enter a valid OTP.")
#define CMessageValidPassword           CLocalize(@"Password is invalid. Should have minimum 6 alphanumeric characters.")
#define CMessageCompareNewandConfirmPassword CLocalize(@"Password and confirm password combination does not match.")

//--------------------
#define CMessageUsername @"Please enter username."
#define CMessagePhone @"Please enter mobile number."
#define CMessageCategory @"Please select a category."
#define CMessageOldPassword @"Please enter old password."
#define CMessageNewPassword @"Please enter new password."
#define CMessageConfirmPassword @"Please confirm your new password."
#define CMessageCompareOldandNewPassword @"New Password can not be your old password"
#define CMessagePasswordDontMatch @"Passwords do not match."
#define CMessageFullname @"Full Name is required"
#define CMessageSelectPicture @"Please select a photo."
#define CMessageText @"You forgot to write something"
#define CMessageSafariCantOpen @"Safari can't open this URL."
#define CMessageDOB @"Please enter date of birth."

#define CMessageCompanName @"Please enter your company name."
#define CMessageCompanyAddress @"Please enter your company full address."
#define CMessageCompanyProvideService @"Please enter List primary industries your company provide service."
#define CMessageCompanyOperationHours @"Please enter Operating buisness hours."
#define CMessageCompanyWebsiteInfo @"Please enter website information."
#define CMessageCompanyNameDetails @"Please enter Name & Contact details of authorized person who will manage your company's iKYDen account."
#define CMessageCompanyListProductService @"Please enter List Product & Services provided by your company."

//#define CCurrentLatitude @"Latitude"
//#define CCurrentLongitude @"Longitude"

#define CMessageSelectCategory @"Please select category."

// In Action Sheet
#define CAlertTitleForSpam @"Why are you reporting this comment?"
#define CAlertTitleForProfilePhoto @"Change Profile Picture"
#define CAlertTitleForCoverPhoto @"Change Cover Picture"
#define CAlertTitleForPostPhoto @"Post Picture"



// Setting Page
#define CProfile                CLocalize(@"Profile")
#define CMyProfile              CLocalize(@"My Profile")

#define CNotification           CLocalize(@"Notifications")
#define CAppNotification        CLocalize(@"App Notifications")

#define CSelectLaguage          CLocalize(@"Select Language")
#define CEnglish                CLocalize(@"English")
#define CArabic                 CLocalize(@"Arabic")

#define CLegal                  CLocalize(@"Legal")
#define CSupport                CLocalize(@"Support")
#define CPrivacyPolicy          CLocalize(@"Privacy Policy")
#define CConfirm                CLocalize(@"CONFIRM")
#define CSendToSelected         CLocalize(@"You selected")

#pragma mark - Title
//-------------------------------
#define CResetPassword              CLocalize(@"RESET PASSWORD")
#define CSignUp                     CLocalize(@"SIGN UP")
#define CForgotPassword             CLocalize(@"FORGOT PASSWORD")
#define CEventsLive                 CLocalize(@"LIVE EVENTS")
#define CSendTo                     CLocalize(@"SEND TO")
#define CSettings                   CLocalize(@"SETTINGS")
#define CEditProfile                CLocalize(@"EDIT PROFILE")
//#define CMyProfile @"MY PROFILE"
#define CFootballMatches            CLocalize(@"SOCCER MATCHES")
#define CAllActivities              CLocalize(@"ALL ACTIVITIES")
#define CNotifications              CLocalize(@"NOTIFICATIONS")
#define CSeasonList                 CLocalize(@"SEASON LIST")
#define CPoints                     CLocalize(@"POINTS")

#define COK                         CLocalize(@"OK")
#define CLogout                     CLocalize(@"Log Out")

#define CError                      CLocalize(@"Error")
#define CAlertVideoAsset            CLocalize(@"Please Load a Video Asset First")
#define CVideoSavingFailed          CLocalize(@"Video Saving Failed")
#define CVideoSaved                 CLocalize(@"Video Saved")
#define CSavedToPhotoAlbum          CLocalize(@"Saved To Photo Album")
#define CFinifshSavingWithError     CLocalize(@"Finished saving video with error")
//#define CSavedToPhotoAlbum                      CLocalize(@"Media has been sent")
#define CExporting                  CLocalize(@"Exporting....")
#define CClose                      CLocalize(@"Close")
#define CSuccess                    CLocalize(@"Success")

#define CErrorWhileSavingImage      CLocalize(@"Image could not be saved.Please try again")
#define CimageSavedSuccessfully     CLocalize(@"Image is successfully saved in your photo album")



//-------------Login---------------------------
#define CAskSignUp                  CLocalize(@"Don't have an account? Sign Up")
#define CBoldSignUp                 CLocalize(@"Sign Up")
