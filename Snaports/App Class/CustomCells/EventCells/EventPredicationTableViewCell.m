//
//  EventPredicationTableViewCell.m
//  Snaports
//
//  Created by mac-0004 on 27/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "EventPredicationTableViewCell.h"

@implementation EventPredicationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [_lblOne addBorder:UIPositionBottom color:[UIColor colorWithWhite:0 alpha:0.3] width:1];
    [_lblType addBorder:UIPositionLeft color:[UIColor colorWithWhite:0 alpha:0.3] width:1];
//    
    appDelegate.configureFlipToMirror(self.contentView,true);
    appDelegate.configureFlipToMirror(self.viewContent,false);
    appDelegate.configureFlipToMirror(self.viewSideButtons,false);
    _btnSwipe.transform= CGAffineTransformIdentity;
    
    [_btnDraw setBackgroundImage:[self imageWithColor:CRGB(187, 216, 242)] forState:UIControlStateSelected];
    [_btnTeam1 setBackgroundImage:[self imageWithColor:CRGB(187, 216, 242)] forState:UIControlStateSelected];
    [_btnTeam2 setBackgroundImage:[self imageWithColor:CRGB(187, 216, 242)] forState:UIControlStateSelected];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
@end
