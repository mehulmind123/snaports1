//
//  SelectLanguageViewController.h
//  Snaports
//
//  Created by mac-00018 on 23/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectLanguageViewController : SuperViewController<UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UILabel *lblSelectLanguage;
@property (weak, nonatomic) IBOutlet UITableView *tblSelectLanguage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutTblHight;
@property (weak, nonatomic) IBOutlet CommonButton *btnConfirm;

@end
