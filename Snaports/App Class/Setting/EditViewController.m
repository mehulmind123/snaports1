//
//  EditViewController.m
//  Snaports
//
//  Created by mac-0004 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "EditViewController.h"

@interface EditViewController ()

@end

@implementation EditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:CEditProfile];
    [self setData];
    
    imgProfile.layer.cornerRadius = CViewHeight(imgProfile)/2;
    imgProfile.clipsToBounds = true;
    
    [txtContryCode setPickerForCountryCodes:^(NSManagedObject *item, NSInteger row, NSInteger component) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)setData
{
    txtFirstName.text = @"Martin";
    txtLastName.text = @"Wazosky";
    txtUserName.text = @"Martin_Wazo";
    txtEmailAddress.text = @"Martin_wazo56@gmail.com";
    txtBirthDay.text = @"14 Martch, 1995";
    txtContryCode.text = @"+91";
    txtMobile.text = @"989898989898";
    txtCountry.text = @"India";
    txtCity.text = @"Mumbai";
    txtFavTeam.text = @"Gujarat Lions";
    
    [txtEmailAddress setEnabled:NO];
    
}

-(IBAction)btnUploadImage:(id)sender
{
    [self selectImage:^(UIImage *image) {
        if (image)
            imgProfile.image = image;
    }];
    
}
-(IBAction)btnSaveClicked:(id)sender
{
    if(![txtFirstName.text isBlankValidationPassed]) //Blank first name
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageFirstName withTitle:@""];
    else if ([txtUserName.text isBlankValidationPassed] && ![txtUserName.text isValidPassword])
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageValidUsername withTitle:@""];
    
}

@end
