//
//  SendToViewController.m
//  Snaports
//
//  Created by mac-0004 on 23/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SendToViewController.h"
#import "SendToTableViewCell.h"
#import "PreviewViewController.h"
@interface SendToViewController ()
{
    NSArray *aryEventList;
    NSInteger selectedCell;
}
@end

@implementation SendToViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [tblSendTo registerNib:[UINib nibWithNibName:@"SendToTableViewCell" bundle:nil] forCellReuseIdentifier:@"SendToTableViewCell"];

    
    aryEventList = @[@"India Vs Pakistan",
                     @"LaLiga Final",
                     @"New Zealand Vs Australia",
                     @"Portugal Vs Argentina",
                     @"Shri Lanka Vs Bangladesh"];
    
    selectedCell = -1;
    [self setNavigationBarbuttons];
    tblSendTo.layer.borderColor = CTextFieldBorderColor.CGColor;
    tblSendTo.layer.borderWidth = 1;
    btnSend.transform = CGAffineTransformMakeScale(1.2, 1.2);
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = true;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Set Navigaiton bar buttons
- (void)setNavigationBarbuttons
{
    [self setTitle:CSendTo];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search_black"]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(showSearchBar)];
    
    self.navigationItem.rightBarButtonItem = rightButton;
    
}

- (void)showSearchBar
{
    if (layoutSearchTop.constant<0) {
        layoutSearchTop.constant = 0;
        [searchBarTop becomeFirstResponder];
    }
    else
    {
        layoutSearchTop.constant = -CSearchBarHeight;        
        [searchBarTop resignFirstResponder];
    }
}

#pragma mark - TableView Delegate and DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return aryEventList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier =@"SendToTableViewCell";
    SendToTableViewCell *cell = (SendToTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
        cell = [[SendToTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    
    cell.lblName.text = aryEventList[indexPath.row];
    cell.btnSelect.selected = (selectedCell == indexPath.row);
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 0.001;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedCell = indexPath.row;
    lblSelectedValue.text =  [NSString stringWithFormat:@"%@ %@", CSendToSelected,aryEventList[selectedCell]];
    [tableView reloadData];
}
-(void)scrollViewDidScroll:(UIScrollView *)sender
{
    [searchBarTop resignFirstResponder];
}
#pragma mark - Action Events
-(IBAction)btnNext:(id)sender
{
    if(lblSelectedValue.text.length == 0)
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:@"Please select any filter"];
        return;
    }
    ((PreviewViewController *)(self.navigationController.viewControllers[self.navigationController.viewControllers.count - 2])).SelectedFilter = lblSelectedValue.text;
    [self.navigationController popViewControllerAnimated:true];
}
@end
