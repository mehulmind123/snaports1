//
//  SelectLanguageTableViewCell.m
//  Snaports
//
//  Created by mac-00018 on 23/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SelectLanguageTableViewCell.h"

@implementation SelectLanguageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    if (appDelegate.configureFlipToMirror) {
        appDelegate.configureFlipToMirror(self.viwContent, true);
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.lblCount.layer.cornerRadius = CViewWidth(self.lblCount)/2;
    self.lblCount.layer.masksToBounds = YES;

}
@end
