//
//  UIApplication+Browser.h
//  Master
//
//  Created by mac-0001 on 19/12/14.
//  Copyright (c) 2014 mac-0001. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KINWebBrowserViewController.h"


@interface UIApplication (Browser)

+(KINWebBrowserViewController *)openURLWithinApplication:(NSString *)url;
-(KINWebBrowserViewController *)openURLWithinApplication:(NSString *)url;



@end
