//
//  EventDetailsViewController.h
//  Snaports
//
//  Created by mac-00018 on 27/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SuperNavigationController.h"
#import <MediaPlayer/MediaPlayer.h>
//#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "TouchDetector.h"

@interface EventDetailsViewController : SuperViewController<UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionviwDetails;
@property NSArray *arrData;

@end
