//
//  MIApplicationViewController.m
//  
//
//  Created by Mehul Rajput on 06/08/15.
//
//

#import <UIKit/UIKit.h>

#import "NSObject+NewProperty.h"

#import "Master.h"





#pragma mark - MILogDetailViewController


@interface MILogDetailViewController : UIViewController

@property (nonatomic,strong) NSString *filePath;

@end

@implementation MILogDetailViewController

@synthesize filePath;

-(void)viewDidLoad
{
    self.view.backgroundColor = [UIColor whiteColor];
    
    UITextView *txtDetail = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    txtDetail.editable = NO;
    txtDetail.text = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    [self.view addSubview:txtDetail];
}

@end










#pragma mark - LogViewController


@interface MILogsViewController : UIViewController

@property (nonatomic,strong) NSString *directoryPath;

@end

@implementation MILogsViewController

@synthesize directoryPath;

-(void)viewDidLoad
{
    self.view.backgroundColor = [UIColor whiteColor];

    [self setMainTableView];
}

-(void)setMainTableView
{
    NSMutableArray *arrList = [[NSMutableArray alloc] init];
    
    [arrList addObjectsFromArray:[[NSFileManager defaultManager] contentsOfDirectoryAtPath:directoryPath error:nil]];
    
    if (arrList.count>0)
        [arrList removeObjectAtIndex:0];
    
    
    UITableView *tblMainList = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    [self.view addSubview:tblMainList];
    
    [tblMainList registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [tblMainList registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"Header"];
    
    FetchedResultsTableDataSource *datasource = [self setFetchResultsController:tblMainList cellIdentifier:@"Cell" section:1 row:[arrList count] cell:^(UITableViewCell *cell, id item, NSIndexPath *indexPath) {
        
        cell.textLabel.text = [arrList objectAtIndex:arrList.count-indexPath.row-1];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }];
    
    [datasource setDidSelectRowAtIndexPath:^(UITableViewCell *cell, id item, NSIndexPath *indexPath){
        [tblMainList deselectRowAtIndexPath:indexPath animated:YES];
        
        MILogDetailViewController *logDetails = [[MILogDetailViewController alloc] init];
        logDetails.filePath = [directoryPath stringByAppendingPathComponent:cell.textLabel.text];
        logDetails.title = cell.textLabel.text;
        [self.navigationController pushViewController:logDetails animated:YES];
        
    }];
    
    
    [tblMainList addDataSource:datasource];
    [tblMainList addDelegate:datasource];
}

@end

























@interface UIDetailTableViewCell : UITableViewCell

@end


@implementation UIDetailTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    return [super initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:reuseIdentifier];
}

@end












#import "MIApplicationViewController.h"

static NSString *const GESTURESTATUS = @"GESTURESTATUS";

@implementation MIApplicationViewController

#pragma mark - Gesture Methods

+(void)load
{
    [[UIApplication sharedApplication] setInteger:0 forKey:GESTURESTATUS];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        {
        UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] init];
        swipeGesture.numberOfTouchesRequired = IS_IPHONE_SIMULATOR?1:2;
        swipeGesture.direction = UISwipeGestureRecognizerDirectionUp;
        [swipeGesture addTargetBlock:^{
            
            NSInteger status = [[UIApplication sharedApplication] integerForKey:GESTURESTATUS];

            if (status == 2)
            {
                [self presentView];
            }
            else if(status==0)
            {
                [[UIApplication sharedApplication] setInteger:status+1 forKey:GESTURESTATUS];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] setInteger:0 forKey:GESTURESTATUS];
                });
            }

        }];
        [[[UIApplication sharedApplication] keyWindow] addGestureRecognizer:swipeGesture];
        }
        
        {
        UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] init];
        swipeGesture.numberOfTouchesRequired = IS_IPHONE_SIMULATOR?1:2;
        swipeGesture.direction = UISwipeGestureRecognizerDirectionDown;
        [swipeGesture addTargetBlock:^{
            
            NSInteger status = [[UIApplication sharedApplication] integerForKey:GESTURESTATUS];
            
            if (status == 1)
            {
                [[UIApplication sharedApplication] setInteger:status+1 forKey:GESTURESTATUS];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] setInteger:0 forKey:GESTURESTATUS];
                });
            }

            
        }];
        [[[UIApplication sharedApplication] keyWindow] addGestureRecognizer:swipeGesture];
        }
        
    });
}

+(void)presentView
{
    [[UIApplication sharedApplication] setInteger:0 forKey:GESTURESTATUS];
    
    [[UIApplication topMostController] presentViewController:[[UINavigationController alloc] initWithRootViewController:[[MIApplicationViewController alloc] init]] animated:YES completion:nil];
}






#pragma mark - ViewController Details



-(void)viewDidLoad
{
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = @"MIApplication";
    
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:nil action:nil];
    [cancelButton clicked:^{
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [self.navigationItem setLeftBarButtonItem:cancelButton];

    
    [self setMainTableView];
}

-(void)setMainTableView
{
    NSMutableArray *arrList = [[NSMutableArray alloc] init];
    
    [arrList addObject:@{@"title":@"Console",@"rows":@[@"Logs",@"Crashes"]}];
    
    
    
    
    NSMutableArray *arrRows = [[NSMutableArray alloc] init];
    
    
    if (CApplicationName)
        [arrRows addObject:@{@"title":CApplicationName,@"value":[CVersionNumber stringByAppendingFormat:@"/%@",CBuildNumber]}];
    
    
    [arrRows addObject:@{@"title":@"Master",@"value":[NSString stringWithFormat:@"%@",MASTER_VERSION]}];

    
    
    if (NSClassFromString(@"SocialNetworks"))
    {
        NSString *title = @"SocialNetworks";
        
// To support older versio, when there is no _SocialNetworks_H was define, In earlier version than 2.0
#ifdef _SocialNetworks_H
        [arrRows addObject:@{@"title":title,@"value":[NSString stringWithFormat:@"%@",SOCIAL_NETWORKS_VERSION]}];
#else
        [arrRows addObject:@{@"title":title,@"value":[NSString stringWithFormat:@"%@",[NSClassFromString(@"SocialNetworks") instancesRespondToSelector:@selector(sdkVersion)]?@"1.0":@"2.0"]}];
#endif
    }
    
    if (NSClassFromString(@"MIAFNetworking"))
    {
        NSString *title = @"MIAFNetworking";

// To support older versio, when there is no _MIAFNetworking_H was define, In earlier version than 2.0
#ifdef _MIAFNetworking_H
        [arrRows addObject:@{@"title":title,@"value":[NSString stringWithFormat:@"%@",AFNETWORKING_VERSION]}];
#else
        [arrRows addObject:@{@"title":title,@"value":[NSString stringWithFormat:@"%@",NSClassFromString(@"MIAFNetworking")?@"2.0":@"1.0"]}];
#endif
    }


    
    
    
    [arrList addObject:@{@"title":@"About",@"rows":arrRows}];
    
    
    
    
    UITableView *tblMainList = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    [self.view addSubview:tblMainList];
    
    
    [tblMainList registerClass:[UIDetailTableViewCell class] forCellReuseIdentifier:@"Cell"];
    [tblMainList registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"Header"];
    
    FetchedResultsTableDataSource *datasource = [self setFetchResultsController:tblMainList cellIdentifier:@"Cell" section:[arrList count] row:0 cell:^(UITableViewCell *cell, id item, NSIndexPath *indexPath) {
        
        
        switch (indexPath.section)
        {
            case 0:
                cell.textLabel.text = [[[arrList objectAtIndex:indexPath.section] valueForKey:@"rows"] objectAtIndex:indexPath.row];
                switch (indexPath.row)
                {
                    case 0:
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        break;
                    case 1:
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        break;
                }
                break;
            case 1:
                cell.textLabel.text = [[[[arrList objectAtIndex:indexPath.section] valueForKey:@"rows"] objectAtIndex:indexPath.row] valueForKey:@"title"];
                cell.detailTextLabel.text = [[[[arrList objectAtIndex:indexPath.section] valueForKey:@"rows"] objectAtIndex:indexPath.row] valueForKey:@"value"];
                break;
        }
        
    }];

    [tblMainList setSectionHeaderHeight:30];
    
    [datasource setSectionHeaderIdentifier:@"Header"];
    [datasource setViewForHeaderInSection:^(UITableViewHeaderFooterView *section, id <NSFetchedResultsSectionInfo> info, NSInteger sectionIndex){
    
        section.textLabel.text = [[arrList objectAtIndex:sectionIndex] valueForKey:@"title"];
        
    }];
    
    
    [datasource setNumberOfRowsInSection:^NSInteger(id <NSFetchedResultsSectionInfo> info, NSInteger sectionIndex){
        return [[[arrList objectAtIndex:sectionIndex] valueForKey:@"rows"] count];
    }];
    
    [datasource setDidSelectRowAtIndexPath:^(UITableViewCell *cell, id item, NSIndexPath *indexPath){
        [tblMainList deselectRowAtIndexPath:indexPath animated:YES];
        
        switch (indexPath.section)
        {
            case 0:
                switch (indexPath.row)
            {
                case 0:
                {
                    MILogsViewController *logsController = [[MILogsViewController alloc] init];
                    logsController.title = cell.textLabel.text;
                    logsController.directoryPath = [CCachesDirectory stringByAppendingPathComponent:@"MIApplication/Log"];
                    [self.navigationController pushViewController:logsController animated:YES];
                }
                    break;
                case 1:
                {
                    MILogsViewController *crashesController = [[MILogsViewController alloc] init];
                    crashesController.title = cell.textLabel.text;
                    crashesController.directoryPath = [CCachesDirectory stringByAppendingPathComponent:@"MIApplication/Crash"];
                    [self.navigationController pushViewController:crashesController animated:YES];
                }
                    break;
            }
                break;
        }

    }];
    
    
    [tblMainList addDataSource:datasource];
    [tblMainList addDelegate:datasource];
}

@end

