
// # MI-r6
// Configuration settings will be stored

#define CBundleVersionOfLastInstalled @"BundleVersionOfLastInstalled"
#define CDeviceToken @"DeviceToken"

#define CNotificationsUserProfile @"notificationuserprofile"
#define CUserLoggedin @"userLoggedin"

#define CProductionMode 0

#if CProductionMode

#define CVersion @"1"

#define BASEURL @"http://52.19.138.143/api/"

#else

#define BASEURL @"http://magentodevelopments.com/zoharii/api/v1/"
#define CVersion @"1"

#endif

