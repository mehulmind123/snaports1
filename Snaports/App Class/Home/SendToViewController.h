//
//  SendToViewController.h
//  Snaports
//
//  Created by mac-0004 on 23/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendToViewController : SuperViewController
{
    IBOutlet UITableView *tblSendTo;
    IBOutlet UILabel *lblTitle,*lblSelectedValue;
    IBOutlet UIButton *btnSend;
    IBOutlet NSLayoutConstraint *layoutSearchTop;
    IBOutlet UISearchBar *searchBarTop;
    
}
@end
