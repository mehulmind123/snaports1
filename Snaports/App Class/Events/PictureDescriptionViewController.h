//
//  PictureDescriptionViewController.h
//  Snaports
//
//  Created by mac-00018 on 28/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SuperViewController.h"

@interface PictureDescriptionViewController : SuperViewController<UIWebViewDelegate, UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webviwDesciption;

@property (nonatomic, strong) NSString *strURL;
@end
