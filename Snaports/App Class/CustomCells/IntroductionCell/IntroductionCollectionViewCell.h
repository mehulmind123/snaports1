//
//  IntroductionCollectionViewCell.h
//  Snaports
//
//  Created by mac-00018 on 10/01/17.
//  Copyright © 2017 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroductionCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgIntroduction;
@property (weak, nonatomic) IBOutlet UIButton *btnprevious;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;

@end
