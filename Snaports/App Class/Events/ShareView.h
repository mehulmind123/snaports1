//
//  ShareView.h
//  Snaports
//
//  Created by mac-00018 on 28/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lblShare;
- (IBAction)btnFacebookClicked:(id)sender;
- (IBAction)btnTwitterClicked:(id)sender;
- (IBAction)btnInstagramClicked:(id)sender;
- (IBAction)btnBackClicked:(id)sender;

@end
