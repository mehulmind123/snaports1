//
//  EventsLiveTableViewCell.m
//  Snaports
//
//  Created by mac-00018 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "EventsLiveTableViewCell.h"
#import "EventsCollectionViewCell.h"

@implementation EventsLiveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    if (appDelegate.configureFlipToMirror) {
        appDelegate.configureFlipToMirror(self.viwChannels, true);
        appDelegate.configureFlipToMirror(self.viwRecentLive, true);
//        appDelegate.configureFlipToMirror(self.viwLIVE, true);

    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (!(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) return nil;
    
    
    
    
    return self;
    
}


/*
 Commented for now
 - (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
 {
 if (!(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) return nil;
 
 [self.collectionviwChannels registerNib:[UINib nibWithNibName:@"EventsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:CollectionViewCellIdentifier];
 
 UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
 //        layout.sectionInset = UIEdgeInsetsMake(10, 10, 9, 10);
 //        layout.itemSize = CGSizeMake(44, 44);
 layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
 
 UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
 //        [flowLayout setItemSize:CGSizeMake((self.collectionviwChannels.frame.size.height-2)/4, (self.collectionviwChannels.frame.size.height - 2)/4)];
 [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
 [self.collectionviwChannels setCollectionViewLayout:flowLayout];
 
 //        [self.collectionviwChannels registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:CollectionViewCellIdentifier];
 //        self.collectionviwChannels.backgroundColor = [UIColor whiteColor];
 self.collectionviwChannels.showsHorizontalScrollIndicator = NO;
 //        [self.contentView addSubview:self.collectionView];
 
 return self;
 
 }

 */

@end
