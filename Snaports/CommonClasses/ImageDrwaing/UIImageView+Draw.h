//
//  UIView+Draw.h
//  GTImageViewDraw
//
//  Created by Gianluca Tursi on 25/03/14.
//  Copyright (c) 2014 Gianluca Tursi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Draw)

- (void)startDrawing;
-(void)stopDrawing;
-(void)resetImage;
-(void)setLineBroadWidth:(CGFloat)size;
-(void)setRubberSize:(CGFloat)size;
-(void) setColor:(UIColor *) color;
- (UIImage*)imageByCombiningImage:(UIImage*)firstImage withImage:(UIImage*)secondImage;
-(UIImage *)save:(bool)isTosave withFilterImage:(UIImage *)FilterImage;
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
-(void)selectRubber;
-(void)DeselectRubber;
-(void)DoUndo;
-(void)DoRedo;
-(void)MakePause:(BOOL)isPause;

@end
