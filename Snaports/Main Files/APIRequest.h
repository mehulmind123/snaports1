//
//  APIRequest.h
//  AFNetworking iOS Example
//
//  Created by mac-0001 on 5/11/14.
//  Copyright (c) 2014 uVite LLC. All rights reserved.
//

// Staus/Response Constants
// Framework

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "UIImageView+AFNetworking.h"



@interface APIRequest : NSObject

+ (id)request;

#pragma mark - LRF

//- (void)login:(NSString *)email password:(NSString *)password isSocial:(int)isSocial facebookId:(NSString*)facebookId gplusId:(NSString*)gplusId completed:(void (^)(id responseObject,NSError *error))completion;


@end
