//
//  TouchDetector.m
//  Snapports
//
//  Created by mac-0004 on 19/12/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import "TouchDetector.h"
#import <UIKit/UIGestureRecognizerSubclass.h>
@implementation TouchDetector

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.enabled) {
        self.state = UIGestureRecognizerStateBegan;
    }
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.enabled) {
        self.state = UIGestureRecognizerStateEnded;
    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.enabled) {
        self.state = UIGestureRecognizerStateEnded;
    }
}


@end
