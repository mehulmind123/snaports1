//
//  RegisterViewController.m
//  Snaports
//
//  Created by mac-00018 on 23/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "RegisterViewController.h"
#import "CameraViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"SIGN UP";
    
    if (appDelegate.configureFlipToMirror) {
        appDelegate.configureFlipToMirror(viwContent, true);
    }
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width/2;
    imgProfile.clipsToBounds = true;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}


#pragma mark - Action Events

-(IBAction)btnUploadImage:(id)sender
{
    [self selectImage:^(UIImage *image) {
        if (image)
            imgProfile.image = image;
    }];
    
}
-(IBAction)btnSignUpClicked:(id)sender
{
    if(![txtFirstName.text isBlankValidationPassed]) //Blank first name
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageFirstName withTitle:@""];
    else if ([txtUserName.text isBlankValidationPassed] && ![txtUserName.text isValidPassword])
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageValidUsername withTitle:@""];
    else if(![txtEmailAddress.text isBlankValidationPassed]) //Blank Email
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageEmail withTitle:@""];
    else if(![txtEmailAddress.text isValidEmailAddress]) //Valid Email
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageValidEmail withTitle:@""];
    else if(![txtPaasword.text isBlankValidationPassed])  //Blank password /confirm password
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessagePassword withTitle:@""];
    else if(![txtConfiPassword.text isBlankValidationPassed])  //Blank password /confirm password
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CConfirmPassword withTitle:@""];
    else if (![txtPaasword.text isValidPassword] && ![txtConfiPassword.text isValidPassword])//Valid password and confirm password
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageValidPassword withTitle:@""];
    else if (![txtPaasword.text isEqualToString:txtConfiPassword.text])//Password and confirm password should match
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageCompareNewandConfirmPassword withTitle:@""];
    else
    {
        CameraViewController *vcCameraController = [[CameraViewController alloc] initWithNibName:@"CameraViewController" bundle:nil];
        [self.navigationController pushViewController:vcCameraController animated:YES];

    }

}

@end
