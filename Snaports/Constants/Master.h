//
//  Master.h
//  Master
//
//  Created by mac-0001 on 28/11/14.
//  Copyright (c) 2014 mac-0001. All rights reserved.
//


#ifndef _Master_H
    #define _Master_H
#endif

#define MASTER_VERSION @"4.0"

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import "Constants.h"
#import "BasicAppDelegate.h"


#import "PPAlerts.h"
#import "MBProgressHUD.h"
#import "PPLoader.h"
//#import "FBShimmeringView.h"

#import "PopUpViewController.h"
#import "UIViewController+ADVOverlay.h"

#import "LocationManager.h"
#import "UIViewController+LocationManager.h"
//#import "BeaconManager.h"

#import "Store.h"
#import "NSManagedObject+Helper.h"
#import "NSManagedObjectContext+Helper.h"
#import "FetchedResultsTableDataSource.h"
#import "UIViewController+NSFetchedResultsController.h"

//#import "JASidePanelController.h"
//#import "UIViewController+JASidePanel.h"

#import "KINWebBrowserViewController.h"
#import "UIApplication+Browser.h"
#import "UIViewController+Browser.h"

#import "KeychainWrapper.h"

#import "SoundManager.h"

#import "LocalizationSystem.h"

//#import "MIEvent.h"

#import "SHCMulticastDelegate.h"

#import "SKPSMTPMessage.h"


#import "MIQuery.h"
#import "NSManagedObject+Mapping.h"

#import "Extensions.h"


static NSString *const NSLocationAlwaysUsageDescription = @"NSLocationAlwaysUsageDescription";
static NSString *const NSLocationWhenInUseUsageDescription = @"NSLocationWhenInUseUsageDescription";


// ================================================================================================================

// Localizable Strings

// ================================================================================================================

/*

 @"ImagePickerActionSheetCancelButton", @"Cancel"
 @"ImagePickerActionSheetCameraButton", @"Take New Photo"
 @"ImagePickerActionSheetPhotoLibraryButton",@"Choose from Existing Photos"

 
 @"ScanningViewControllerTitle", @"Scanning..."
 
 @"ImagePickerControllerPhotoLibrarySupportError", @"Your device dosen't support photo library!"
 @"ImagePickerControllerPhotoLibraryPermissionRestrictionError", @"You don't have permission to use Photo Library"
 @"ImagePickerControllerPhotoLibraryPermissionDeniedError", @"Please enable Photo Library to use")
 
 @"ImagePickerControllerCameraSupportError", @"No Camera Available"
 @"ImagePickerControllerCameraDeniedError", @"Please enable Camera to use"
 @"ImagePickerControllerCameraRestrictionError", @"You don't have permission to use Camera"

 
 
 @"AlertButtonOK", @"OK"
 
 
 @"LoaderLoadingKey", @"Loading..."
 
 
 @"InAppPurchaseCanNotMakePayments", @"You can not make purchase,Please check your purchase settings."
 

 @"MFMailComposeViewControllerCanNotSendMail", @"Can not send Mail"
 
 
 @"MFMessageComposeViewControllerCanNotSendMail",@"Can not send Message"
 
*/





// ================================================================================================================

// Version Changes

// ================================================================================================================

/*
 
// 4.0
 
 - Must Call "loadData" Method for using fetchedResultsController after setting all properties
 ## - Must add Import statement for  all other frameworks, like AFNetworking, SocialNetworks, FacebookSDK, FDSDKCoreKit, TwitterKit, Google, Pinterest, Parse, etc...
 
*/




