//
//  ResetPasswordViewController.m
//  Snaports
//
//  Created by mac-00018 on 23/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "EventsLiveViewController.h"

@interface ResetPasswordViewController ()

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:CResetPassword];
    
    if (appDelegate.configureFlipToMirror) {
        appDelegate.configureFlipToMirror(self.viwContent, true);
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Button Click Action
- (IBAction)btnRegisterClick:(id)sender {
    
    if(![self.txtOTPCode.text isBlankValidationPassed]) //Blank OTP
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageVerificationCode withTitle:@""];
    else if(![self.txtOTPCode.text isValidationOTP]) //Valid OTP
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageValidOTP withTitle:@""];
    else if(![self.txtNewPassword.text isBlankValidationPassed] || ![self.txtConfirmPassword.text isBlankValidationPassed]) //nlank new and COnfirm Password
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessagePassword withTitle:@""];
    else if(![self.txtConfirmPassword.text isBlankValidationPassed]) //nlank new and COnfirm Password
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CConfirmPassword withTitle:@""];
    else if (![self.txtNewPassword.text isValidPassword]) //Valid New and Confirm password
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageValidPassword withTitle:@""];
        
    }
    else if (![self.txtNewPassword.text isEqualToString:self.txtConfirmPassword.text])
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageCompareNewandConfirmPassword withTitle:@""];
    }
    else
    {
        EventsLiveViewController *vcEvents = [[EventsLiveViewController alloc] initWithNibName:@"EventsLiveViewController" bundle:nil];
        [self.navigationController pushViewController:vcEvents animated:YES];

    }

    
}


@end
