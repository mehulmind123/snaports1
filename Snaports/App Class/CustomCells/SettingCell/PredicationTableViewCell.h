//
//  PredicationTableViewCell.h
//  Snaports
//
//  Created by mac-0004 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "shadowView.h"

@interface PredicationTableViewCell : UITableViewCell

@property IBOutlet UIImageView *imgProfile;
@property IBOutlet UIView *viewImgOuter;
@property IBOutlet UILabel *lblDescription, *lblTime,*lblVs,*lblTeam1,*lblDraw,*lblTeam2,*lblTeam1Per,*lblDrawPer,*lblTeam2Per;
@property IBOutlet UIView *viewTeam1,*ViewTeam2,*viewDraw,*viewTeam1Ins,*ViewTeam2Ins,*viewDrawIns;
@property IBOutlet NSLayoutConstraint *ContWidthTeam1,*ContWidthTeam2,*ContWidthDraw;
@property (weak, nonatomic) IBOutlet shadowView *viwContent;
@property (weak, nonatomic) IBOutlet UIView *viwPointsBar;

@end
