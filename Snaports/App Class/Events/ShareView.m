//
//  ShareView.m
//  Snaports
//
//  Created by mac-00018 on 28/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "ShareView.h"

@implementation ShareView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)btnFacebookClicked:(id)sender {
}

- (IBAction)btnTwitterClicked:(id)sender {
}

- (IBAction)btnInstagramClicked:(id)sender {
}

- (IBAction)btnBackClicked:(id)sender {
    
    [[UIApplication topMostController] dismissOverlayViewControllerAnimated:YES completion:nil];
}
@end
