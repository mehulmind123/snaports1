//
//  AllActivitiewViewController.m
//  Snaports
//
//  Created by mac-0004 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "AllActivitiewViewController.h"
#import "PredicationTableViewCell.h"
#import "EventActivityTableViewCell.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>

@interface AllActivitiewViewController ()

@end

@implementation AllActivitiewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:CAllActivities];
    
    [tblActivities registerNib:[UINib nibWithNibName:@"PredicationTableViewCell" bundle:nil] forCellReuseIdentifier:@"PredicationTableViewCell"];
    [tblActivities registerNib:[UINib nibWithNibName:@"EventActivityTableViewCell" bundle:nil] forCellReuseIdentifier:@"EventActivityTableViewCell"];
    
    [tblActivities setEstimatedRowHeight:225];
    [tblActivities setRowHeight:UITableViewAutomaticDimension];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - TableView Delegate and DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 0.001;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 30;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row %4 == 0) {
        
        static NSString *simpleTableIdentifier =@"PredicationTableViewCell";
        PredicationTableViewCell *cell = (PredicationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
            cell = [[PredicationTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
        
        cell.ContWidthTeam1.constant = CViewWidth(cell.viewTeam1) * 0.1;
        cell.ContWidthDraw.constant = CViewWidth(cell.viewDraw) * 0.2;
        cell.ContWidthTeam2.constant = CViewWidth(cell.ViewTeam2) * 0.7;
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             [cell layoutIfNeeded]; // Called on parent view
                         }];
        
        
        return cell;
    }
    else
    {
        static NSString *simpleTableIdentifier =@"EventActivityTableViewCell";
        EventActivityTableViewCell *cell = (EventActivityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
            cell = [[EventActivityTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
        
        cell.imgProfile.image = [UIImage imageNamed:[NSString stringWithFormat:@"%ld.jpg",(long)indexPath.row%5]];
        cell.imgPlayIcon.hidden = indexPath.row % 3 == 0;
        
        [cell.btnPlay touchUpInsideClicked:^{
            
            NSBundle *bundle = [NSBundle mainBundle];
            NSString *moviePath = [bundle pathForResource:@"temp" ofType:@"mp4"];
            NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
            
            AVPlayer *player = [AVPlayer playerWithURL:movieURL];
            
            AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
            [self presentViewController:controller animated:YES completion:nil];
            controller.player = player;
            [player play];

        }];
        
        return cell;
        
    }
}


@end
