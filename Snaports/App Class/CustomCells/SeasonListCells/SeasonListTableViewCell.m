//
//  SeasonListTableViewCell.m
//  Snaports
//
//  Created by mac-00018 on 29/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SeasonListTableViewCell.h"

@implementation SeasonListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    if (appDelegate.configureFlipToMirror) {
        appDelegate.configureFlipToMirror(self.contentView, true);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
