//
//  AppDelegate.m
//  Snaports
//
//  Created by mac-00018 on 22/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "AppDelegate.h"
#import "CameraViewController.h"
#import "EventsLiveViewController.h"
#import "PredicationViewController.h"
#import "IntrouctionViewController.h"
#import "SelectLanguageViewController.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    

    [appDelegate gotoHomeScreen:YES];
//    [self setupRootViewControllerForWindow];
    
#pragma mark - Transform/Flip content for Arabic

    __weak typeof(self) weakSelf = self;

    //FLip subviews inside view
    self.configureFlipSingleView = ^(UIView *view)
    {
        view.transform = CGAffineTransformMake(-1, 0, 0, 1, 0, 0);
        
        if ([view isKindOfClass:[UILabel class]] || [view isKindOfClass:[UITextView class]] || [view isKindOfClass:[UITextField class]]) {
            
            if(((UILabel *)view).textAlignment == NSTextAlignmentRight)
            {
                ((UILabel *)view).textAlignment = NSTextAlignmentLeft;
            }
            else if(((UILabel *)view).textAlignment == NSTextAlignmentLeft)
            {
                ((UILabel *)view).textAlignment = NSTextAlignmentRight;
            }
        }
        
    };

    //Flip VIew
    self.configureFlipToMirror = ^(UIView *viewOriginal, BOOL IsRotateMain)
    {
        if (appDelegate.isArabic) {
            if (IsRotateMain)
                viewOriginal.transform = CGAffineTransformMake(-1, 0, 0, 1, 0, 0);
            else
                viewOriginal.transform = CGAffineTransformIdentity;
            
            for (UIView *view in viewOriginal.subviews) {
                
                if ([view isKindOfClass:[UIView class]]) {
                }
                if (weakSelf.configureFlipSingleView) {
                    weakSelf.configureFlipSingleView(view);
                }
            }
        }

    };
    
    _isArabic = [[CUserDefaults objectForKey:CCureentLanguageStore] isEqual:@1];
    
    //    isArebic = true;
    if (_isArabic)
        [[LocalizationSystem sharedLocalSystem] setLanguage:CLanguageArebic];

    
    return YES;
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)gotoHomeScreen:(BOOL)isSignOut
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    SuperNavigationController *nav;

    if (isSignOut && false) {
        
        IntrouctionViewController *vcSelectlanguage = [[IntrouctionViewController alloc] initWithNibName:@"IntrouctionViewController" bundle:nil];

        nav = [[SuperNavigationController alloc] initWithRootViewController:vcSelectlanguage];
        self.window.rootViewController = nav;
       
    }
    else
    {
        [self setupRootViewControllerForWindow];
    }
    [self.window makeKeyAndVisible];
}


- (void)changeLanguage
{
    [CUserDefaults setObject:_isArabic ? @1 : @0 forKey:CCureentLanguageStore];
    [[LocalizationSystem sharedLocalSystem] setLanguage:appDelegate.isArabic ? CLanguageArebic :CLanguageEnglish];
    
    [self UpdateRootViewControllerForWindow];
    
}

- (NSString *)getLanguage
{
    NSString *strLanguage = [[CUserDefaults objectForKey:CCureentLanguageStore] isEqual:@1]?@"Arabic":@"English";
    return strLanguage;
}


- (void)setupRootViewControllerForWindow
{
    self.swipeBetweenVC = [YZSwipeBetweenViewController new];
    
    UIViewController *vc1 = [[CameraViewController alloc] initWithNibName:@"CameraViewController" bundle:nil];
    SuperNavigationController *navCon1 =
    [[SuperNavigationController alloc]initWithRootViewController:vc1];
    
    UIViewController *vc2 = [[EventsLiveViewController alloc] initWithNibName:@"EventsLiveViewController" bundle:nil];
    SuperNavigationController *navCon2 =
    [[SuperNavigationController alloc] initWithRootViewController:vc2];
    
    UIViewController *vc3 = [[PredicationViewController alloc] initWithNibName:@"PredicationViewController" bundle:nil];
    SuperNavigationController *navCon3 =
    [[SuperNavigationController alloc] initWithRootViewController:vc3];
    
    self.swipeBetweenVC.viewControllers = @[navCon1, navCon2, navCon3];
    self.swipeBetweenVC.currentIndex =  0;//(NSUInteger)self.swipeBetweenVC.viewControllers.count/2;
    
    self.window.rootViewController = self.swipeBetweenVC;
}

- (void)UpdateRootViewControllerForWindow
{
    
    
    UIViewController *vc2 = [[EventsLiveViewController alloc] initWithNibName:@"EventsLiveViewController" bundle:nil];
    SuperNavigationController *navCon2 =
    [[SuperNavigationController alloc] initWithRootViewController:vc2];
    
    UIViewController *vc3 = [[PredicationViewController alloc] initWithNibName:@"PredicationViewController" bundle:nil];
    SuperNavigationController *navCon3 =
    [[SuperNavigationController alloc] initWithRootViewController:vc3];
    
    
    SuperNavigationController *navCon1  = self.swipeBetweenVC.viewControllers[0];
    
    self.swipeBetweenVC.viewControllers = @[navCon1, navCon2, navCon3];
    self.swipeBetweenVC.currentIndex =  0;//(NSUInteger)self.swipeBetweenVC.viewControllers.count/2;
    
    self.window.rootViewController = self.swipeBetweenVC;
}


@end
