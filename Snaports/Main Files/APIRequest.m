
//
//  APIRequest.m
//  AFNetworking iOS Example
//
//  Created by mac-0001 on 5/11/14.
//  Copyright (c) 2014 uVite LLC. All rights reserved.
//

#import "APIRequest.h"
//#import "AFHTTPRequestOperationManager.h"
//#import "AFHTTPRequestOperation.h"
#import "MIAFNetworking.h"

#define CLogin  @"login"

static APIRequest *request = nil;

@implementation APIRequest

+ (id)request
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        request = [[APIRequest alloc] init];
        [[MIAFNetworking sharedInstance] setBaseURL:BASEURL];
        NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:[[[[MIAFNetworking sharedInstance] sessionManager] responseSerializer] acceptableStatusCodes]];
        [indexSet addIndex:400];
        [[[MIAFNetworking sharedInstance] sessionManager] responseSerializer].acceptableStatusCodes = indexSet;
    });
    
    return request;
}

#pragma mark - LRF

- (void)login:(NSString *)email password:(NSString *)password isSocial:(int)isSocial facebookId:(NSString*)facebookId gplusId:(NSString*)gplusId completed:(void (^)(id responseObject,NSError *error))completion
{
    
    [[MIAFNetworking sharedInstance] POST:@"/api/user/login" parameters:@{}
     
                                  success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if ([self checkResponseStatusAndShowAlert:true andsucessmessage:false data:responseObject api:@
                                "" withResignKeyboard:true])
         {
             NSDictionary *dicData = [responseObject valueForKey:CJsonData];
             
            
             
//             [self saveLoginUserData:[responseObject valueForKey:CJsonData] isRegistered:YES];
             
             if (completion)
                 completion(responseObject,nil);
         }
     }
                                  failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [self actionOnAPIFailure:error showAlert:false api:@"Login"];
     }];
}

-(void)actionOnAPIFailure:(NSError *)error showAlert:(BOOL)isShowAlert api:(NSString *)api
{
    NSLog(@"API Error = %@ == %@",api,error);
}

-(BOOL)checkResponseStatusAndShowAlert:(BOOL)showAlert andsucessmessage:(BOOL)Successmessage data:(NSDictionary *)responseobject api:(NSString *)api withResignKeyboard:(BOOL)isresign
{
    if (isresign) {
        [[[UIApplication sharedApplication] keyWindow] endEditing:true];
    }
    
    if (responseobject && [responseobject isKindOfClass:[NSDictionary class]])
    {
        if([[responseobject stringValueForJSON:CJsonStatus] isEqualToString:CStatusOne] ||  [[responseobject numberForJson:CJsonStatus] isEqual:@1])
        {
            if (Successmessage)
                [[PPAlerts sharedAlerts] showAlertWithType:3 withMessage:[responseobject stringValueForJSON:CJsonMessage]];
            return YES;
        }
        if([[responseobject stringValueForJSON:CJsonStatus] isEqualToString:@"9"] || [[responseobject numberForJson:CJsonStatus] isEqual:@9])
        {
            [[PPAlerts sharedAlerts] showAlertWithType:3 withMessage:[responseobject stringValueForJSON:CJsonMessage]];
//            [appDelegate logoutUser:true];
            return NO;
        }
        if(([[responseobject stringValueForJSON:CJsonStatus] isEqualToString:@"10"] || [[responseobject numberForJson:CJsonStatus] isEqual:@10]))
        {
            [[PPAlerts sharedAlerts] showAlertWithType:3 withMessage:[responseobject stringValueForJSON:CJsonMessage]];
//            [appDelegate logoutUser:true];
            return NO;
        }
    }
    
    if (showAlert)
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:[responseobject stringValueForJSON:CJsonMessage] withTitle:CMessageSorry];
    
    return NO;
}
@end
