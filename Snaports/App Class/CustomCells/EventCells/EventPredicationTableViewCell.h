//
//  EventPredicationTableViewCell.h
//  Snaports
//
//  Created by mac-0004 on 27/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventPredicationTableViewCell : UITableViewCell

@property IBOutlet UIView *viewTime,*viewLnFT,*viewContent,*viewSideButtons;

//viewTime
@property IBOutlet UILabel *lblTime;
//viewLnFT
@property IBOutlet UILabel *lblOne,*lblTwo,*lblType;


@property IBOutlet UILabel *lblTeam1,*lblTeam2;

@property IBOutlet UIButton *btnSwipe;
@property IBOutlet UIButton *btnTeam1,*btnTeam2,*btnDraw;
@property IBOutlet UILabel *lblTeam1Pre,*lblTeam2Pre,*lblDrawPre;


@property IBOutlet NSLayoutConstraint *layoutMainViewTrailing;
@end
