//
//  EventDetailsViewController.m
//  Snaports
//
//  Created by mac-00018 on 27/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "EventDetailsViewController.h"
#import "EventDetailsCollectionViewCell.h"
#import "PictureDescriptionViewController.h"
#import "ShareView.h"

#define CollectionViewCellIdentifier @"CollectionViewCellIdentifier"
@interface EventDetailsViewController ()

@end

@implementation EventDetailsViewController
@synthesize arrData;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    //Go back when swiped from top to bottom
    //Add Swipe gesture only if details are available
    UISwipeGestureRecognizer *mSwipeUpRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(HideDetails)];
    mSwipeUpRecognizer.direction = (UISwipeGestureRecognizerDirectionDown);
    [self.view addGestureRecognizer:mSwipeUpRecognizer];
    
    [self setArray];
    
    [self.collectionviwDetails registerNib:[UINib nibWithNibName:@"EventDetailsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:CollectionViewCellIdentifier];
    
    
    //Set flow layout for collectionview
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(CMainScreen.bounds.size.width , CMainScreen.bounds.size.height);
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;

    [self.collectionviwDetails setCollectionViewLayout:flowLayout];
    
    
    self.collectionviwDetails.delegate = self;
    self.collectionviwDetails.dataSource = self;
//    [self.collectionviwDetails reloadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self setNeedsStatusBarAppearanceUpdate];

}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
}


#pragma mark - Collection View Delegate and DataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrData.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    EventDetailsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier forIndexPath:indexPath];
    
    [self setRoundedCorners:cell.viwTitle];
    
    
    NSDictionary *dictAtIndex = [arrData objectAtIndex:indexPath.row];
    
    //Remove all gestures to cell
    for (UISwipeGestureRecognizer *gr in cell.gestureRecognizers) {
        [cell removeGestureRecognizer:gr];
    }
    
    [cell.viwTitle setHidden:YES];
    [cell.btnShowDetails setHidden:YES];
    [cell.imgShowMore setHidden:YES];

    //Add Swipe gesture only if details are available
    UISwipeGestureRecognizer *mSwipeUpRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    mSwipeUpRecognizer.direction = (UISwipeGestureRecognizerDirectionUp);
    [mSwipeUpRecognizer setObject:dictAtIndex forKey:@"Details"];

    //Check if title is not empty
    if (![[dictAtIndex valueForKey:@"ImgTitle"] isEqualToString:@""]) {
        cell.lblTitle.text = [dictAtIndex valueForKey:@"ImgTitle"];
        [cell.viwTitle setHidden:NO];

    }

    //Check if video or image description is not empty
    if (![[dictAtIndex valueForKey:@"video"] isEqualToString:@""]) {
        
        [cell.btnShowDetails setHidden:NO];
        [cell.imgShowMore setHidden:NO];
        [cell addGestureRecognizer:mSwipeUpRecognizer];

        [cell.btnShowDetails setTitle:@"WATCH" forState:UIControlStateNormal];
        
    }
    else if (![[dictAtIndex valueForKey:@"text"] isEqualToString:@""])
    {
        [cell.btnShowDetails setHidden:NO];
        [cell.imgShowMore setHidden:NO];
        [cell addGestureRecognizer:mSwipeUpRecognizer];

        [cell.btnShowDetails setTitle:@"READ" forState:UIControlStateNormal];

    }
    
    
    //Show details
    [cell.btnShowDetails touchUpInsideClicked:^{
        
        [self showDetails:dictAtIndex];
        
    }];
    
    [cell.btnShare touchUpInsideClicked:^{
        
        ShareView *vwShare = [ShareView viewFromXib];
        CViewSetWidth(vwShare, CScreenWidth);
        CViewSetY(vwShare, CScreenHeight - CViewHeight(vwShare));
        
        CATransition *transition =[CATransition animation];
        transition.duration = 0.25;//kAnimationDuration
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromTop ;
        [vwShare.layer addAnimation:transition forKey:nil];
        [self.view addSubview:vwShare];
        [self.navigationController presentView:vwShare];


    }];
    
    //Cancel option
    [cell.btnCancel touchUpInsideClicked:^{
        [self HideDetails];
    }];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)handleGesture:(UISwipeGestureRecognizer *)gest
{
    if ([gest objectForKey:@"Details"]) {
        [self showDetails:[gest objectForKey:@"Details"]];
    }
}

#pragma mark - Hide details
- (void)HideDetails
{
    [self.navigationController popViewControllerAnimated:YES];

}
#pragma mark - SHow detail screen
- (void)showDetails:(NSDictionary *)dictAtIndex
{
    if (![[dictAtIndex valueForKey:@"video"] isEqualToString:@""]) {
        
        //Play default video
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *moviePath = [bundle pathForResource:@"temp" ofType:@"mp4"];
        NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
        
        // create an AVPlayer
        AVPlayer *player = [AVPlayer playerWithURL:movieURL];
        
        AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
        [self presentViewController:controller animated:YES completion:nil];
        controller.player = player;
        [player play];
        
    }
    else
    {
        //Open webpage
        PictureDescriptionViewController *vcPictureDescription = [[PictureDescriptionViewController alloc] initWithNibName:@"PictureDescriptionViewController" bundle:nil];
        vcPictureDescription.strURL = @"https://en.wikipedia.org/wiki/IOS";
        [self presentViewController:vcPictureDescription animated:YES completion:nil];
        
    }

}

#pragma mark - Helper Fumctions
- (void)setRoundedCorners:(UIView *)viw
{
    viw.layer.cornerRadius = viw.frame.size.height/4;
    viw.layer.masksToBounds = YES;
}

- (void)setArray
{
//    arrData = @[
//                @{
//                    @"imgPath":@"",
//                    @"ImgTitle" : @"BALLON D'OR SHORTLIST:FIRST10NAMES NSNNNSSDD",
//                    @"video":@"",
//                    @"text":@"fdfgdfgdfgdfgdfgdfgdfgdfgdfg"
//                    
//                    },
//                @{
//                    @"imgPath":@"",
//                    @"ImgTitle" : @"MOU SLAMS COUNTE:IT'S HUMILIATING FOR US",
//                    @"video":@"gfdgdfgdfgdfgdfgdfgdfgdfgdgdfgdfgdf",
//                    @"text":@""
//                    
//                    },
//                @{
//                    @"imgPath":@"",
//                    @"ImgTitle" : @"",
//                    @"video":@"",
//                    @"text":@""
//                    
//                    }
//                ];
}


@end
