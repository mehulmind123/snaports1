//
//  imageLayerCollectionViewCell.h
//  WaterMarkOnVideoDemo
//
//  Created by mac-0004 on 09/12/16.
//  Copyright © 2016 mac-0004. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface imageLayerCollectionViewCell : UICollectionViewCell
@property IBOutlet UIImageView *imgVFilter;

@end
