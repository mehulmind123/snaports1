//
//  EventsCollectionViewCell.h
//  Snaports
//
//  Created by mac-00018 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *viwDesciption;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannels;
@property (weak, nonatomic) IBOutlet UILabel *lblDescriptions;

@end
