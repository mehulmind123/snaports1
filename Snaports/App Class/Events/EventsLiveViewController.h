//
//  EventsLiveViewController.h
//  Snaports
//
//  Created by mac-00018 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsLiveViewController : SuperViewController<UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, CAAnimationDelegate, UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblEvents; //TEST

@end
