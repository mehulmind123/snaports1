//
//  MKMapView+MapExtension.h
//  Only Appointment
//
//  Created by mac-00015 on 8/7/15.
//  Copyright (c) 2015 mac-0009. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (MapExtension)

/*** To zoom mapview with mile distance parameter ***/
- (void)setZoomViewByMile:(MKMapView*)mapView mile:(double)mile location:(CLLocationCoordinate2D)location;



/*** To set up region for all the annotations contained in mapview ***/
- (MKCoordinateRegion)regionForAnnotations:(NSArray *)annotations mapView:(MKMapView*)mapView fromLat:(double)fromLat fromLong:(double)fromLong;

@end
