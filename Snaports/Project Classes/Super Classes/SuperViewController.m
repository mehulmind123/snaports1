//
//  SuperViewController.m
//  ChamRDV
//
//  Created by mac-0005 on 12/21/13.
//  Copyright (c) 2013 MIND INVENTORY. All rights reserved.
//

    #import "SuperViewController.h"

@interface SuperViewController ()

@end

@implementation SuperViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];


    [self.navigationController.navigationBar.topItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backArraow"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self.navigationController
                                                                  action:@selector(popViewControllerAnimated:)];
    self.navigationItem.leftBarButtonItem = backButton;
    

     [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor], NSFontAttributeName:CFontSolidoMedium(20)}];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self setNeedsStatusBarAppearanceUpdate];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Pull To Refresh

-(void)initializePullToRefresh
{
    //////// pull to refresh
    
    
    
}

- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(CGPoint *)ltargetContentOffset
{
    dragging = NO;
    
    if (shouldUpdate)
    {
        queue = [NSOperationQueue new];
        NSInvocationOperation *updateOperation = [[NSInvocationOperation alloc] initWithTarget:self  selector:@selector(updateMethod)  object:nil];
        [queue addOperation:updateOperation];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        superTable.contentInset = UIEdgeInsetsMake(60, 0, 0, 0);
        superCollection.contentInset = UIEdgeInsetsMake(60, 0, 0, 0);
        [UIView commitAnimations];
    }
    
    if (ltargetContentOffset->y>=scrollView.contentSize.height-240-scrollView.frame.size.height && ltargetContentOffset->y<scrollView.contentOffset.y)
        [self loadMoreData];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (sender.contentOffset.y>0)
        spinner.hidden = YES;
    else
        spinner.hidden = NO;
    
    if (sender.isDragging && sender.contentOffset.y>=sender.contentSize.height-240-sender.frame.size.height && lastScrollOffset<sender.contentOffset.y && !dragging)
    {
        dragging = YES;
        [self loadMoreData];
    }
    
    lastScrollOffset = sender.contentOffset.y;
    
    //    if (pullToRefresh)
    //    {
    //        offset = superTable.contentOffset.y;
    //        offset *= -1;
    
    ////        if (offset < 60) {
    ////            if (shouldPlaySound) {
    ////                queue = [NSOperationQueue new];
    ////                NSInvocationOperation *playOperation = [[NSInvocationOperation alloc] initWithTarget:self  selector:@selector(playAudioWithAudioFileName:)  object:CPullToRefreshReleaseSoundPath];
    ////                [queue addOperation:playOperation];
    ////                shouldPlaySound = NO;
    ////            }
    ////        }
    ////        else if (offset >= 60) {
    ////            if (!shouldUpdate && !isUpdating) {
    ////                queue = [NSOperationQueue new];
    ////                NSInvocationOperation *playOperation = [[NSInvocationOperation alloc] initWithTarget:self  selector:@selector(playAudioWithAudioFileName:)  object:CPullToRefreshPullSoundPath];
    ////                [queue addOperation:playOperation];
    ////            }
    ////        }
    //
    //        if (offset > 0 && offset < 60) {
    //
    //            if(!isUpdating) updateLabel.text = @"Pull down to refresh...";
    //            [UIView beginAnimations:nil context:NULL];
    //            [UIView setAnimationBeginsFromCurrentState:YES];
    //            [UIView setAnimationDuration:0.2];
    //            updateImageView.transform = CGAffineTransformMakeRotation(0);
    //            [UIView commitAnimations];
    //            shouldUpdate = NO;
    //        }
    //
    //        if (offset >= 60) {
    //
    //            if(!isUpdating) updateLabel.text = @"Release to refresh...";
    //            [UIView beginAnimations:nil context:NULL];
    //            [UIView setAnimationBeginsFromCurrentState:YES];
    //            [UIView setAnimationDuration:0.2];
    //            updateImageView.transform = CGAffineTransformMakeRotation(3.14159265);
    //            [UIView commitAnimations];
    //            shouldUpdate = YES;
    //            shouldPlaySound = YES;
    //        }
    //
    //        if (isUpdating) {
    //            shouldUpdate = NO;
    //        }
    //    }
}

- (void) setUpdateDate
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSDate *now = [[NSDate alloc] init];
    NSString *dateString = [dateFormat stringFromDate:now];
    NSString *objectString = [[NSString alloc] initWithFormat:@"%@ %@",@"Last updated on",dateString];
    lastUpdateLabel.text = objectString;
//    NSString *documentsFolderPath = [CCachesDirectory stringByAppendingPathComponent:fileStorageName];
//    [objectString writeToFile:documentsFolderPath atomically:YES encoding:NSStringEncodingConversionAllowLossy error:nil];
}

- (void) stopSpinner
{
    [spinner removeFromSuperview];
    updateImageView.hidden = NO;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    superTable.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    superCollection.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [UIView commitAnimations];
    isUpdating = NO;
}

- (void) startSpinner
{
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.color = [UIColor lightGrayColor];
    //	spinner.center = CGPointMake(updateImageView.center.x,updateLabel.center.y);
    //    spinner.center = updateImageView.center;
    spinner.frame = CGRectMake(40, superTable.frame.origin.y+15, 40, 40);
    updateImageView.hidden = YES;
    [spinner startAnimating];
    //[self.view addSubview:spinner];
    //   [self .view sendSubviewToBack:spinner];
    updateLabel.text = @"Updating...";
    isUpdating = YES;
}

- (void)refreshData
{
    
}

- (void)loadMoreData
{
    
}

- (void)updateMethod
{
    [self refreshData];
    [self performSelectorOnMainThread:@selector(startSpinner) withObject:nil waitUntilDone:NO];
}

- (void)stopUpdating
{
    [self performSelectorOnMainThread:@selector(stopSpinner) withObject:nil waitUntilDone:NO];
    [self setUpdateDate];
}


@end
