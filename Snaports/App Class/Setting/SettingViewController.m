//
//  SettingViewController.m
//  Snaports
//
//  Created by mac-0004 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SettingViewController.h"
#import "SelectLanguageTableViewCell.h"
#import "MyProfileViewController.h"
#import "NotificationViewController.h"
#import "LoginViewController.h"

@interface SettingViewController ()
{
    NSArray *arrayData;
    NSString *selectedLanguage;
    BOOL isNavigatedAlready;
    CGFloat defaultHeight;
}
@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    defaultHeight = 53.00 * CScreenWidth/375.00;
    
    selectedLanguage = [appDelegate getLanguage];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backArraow"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(navigateToCamera)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    //Left swipe
    UISwipeGestureRecognizer *gestureRecognizerLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(navigateToCamera)];
    [gestureRecognizerLeft setDirection:(UISwipeGestureRecognizerDirectionUp)];
    gestureRecognizerLeft.delegate = self;
    [self.view addGestureRecognizer:gestureRecognizerLeft];

    [self setData];

}

- (void)navigateToCamera
{
    
    isNavigatedAlready = YES;
    CATransition *transition = [CATransition animation];
    transition.duration = 0.30;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop;
    self.view.window.backgroundColor = [UIColor whiteColor];
    self.view.window.tintColor = [UIColor whiteColor];
    
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:false];
    [appDelegate setupRootViewControllerForWindow];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)viewWillAppear:(BOOL)animated
{
    isNavigatedAlready = false;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];

}

#pragma mark - TableView Delegate and DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return defaultHeight;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 0.001;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return arrayData.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ((NSArray *)[arrayData[section] valueForKey:@"Data"]).count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier =@"SelectLanguageTableViewCell";
    SelectLanguageTableViewCell *cell = (SelectLanguageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        
        NSArray *nib =[[NSBundle mainBundle] loadNibNamed:@"SelectLanguageTableViewCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
        
    }
    NSDictionary *Item =  ((NSArray *)[arrayData[indexPath.section] valueForKey:@"Data"])[indexPath.row];
    
    cell.lblName.text = [Item valueForKey:@"RowName"];
    cell.lblName.font = CFontSolidoLight(18);
    cell.imgFlag.image = [UIImage imageNamed: [Item valueForKey:@"Image"]];
    cell.lblCount.hidden = YES;
    
    if ([[Item valueForKey:@"RowName"] isEqualToString:CAppNotification]) {
        cell.lblCount.hidden = NO;
        
    }
    cell.btnSelect.userInteractionEnabled = false;
    [cell.btnSelect setImage:[UIImage imageNamed: [[Item valueForKey:@"Type"] integerValue] == 0 ? (appDelegate.isArabic ? @"Setting_Expands_Rotate": @"Setting_Expands") : @"unselectBox"] forState:UIControlStateNormal];
    
    [cell.btnSelect setImage:[UIImage imageNamed: [[Item valueForKey:@"Type"] integerValue] == 0 ? (appDelegate.isArabic ? @"Setting_Expands_Rotate": @"Setting_Expands") : @"selectedBox"] forState:UIControlStateSelected];
    
    cell.btnSelect.selected = [cell.lblName.text isEqualToString:CLocalize(selectedLanguage)];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    return defaultHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
{
    float height = 54.0 * CScreenWidth/375.00;
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CScreenWidth, height)];
    
    UIView *viewTopLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CScreenWidth, 1)];
    UIView *viewBottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, height-1, CScreenWidth, 1)];
    UILabel *lblSection  = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, CScreenWidth-30, height)];
    lblSection.text = [arrayData[section] valueForKey:@"SectionName"];
    lblSection.font = CFontSolidoLight(18);
    lblSection.textAlignment = NSTextAlignmentLeft;

    if (appDelegate.isArabic) {
        lblSection.textAlignment = NSTextAlignmentRight;

    }
    
    lblSection.textColor = CRGB(68, 63, 59);
    view.backgroundColor = CRGB(249, 249, 249);
    viewTopLine.backgroundColor = CRGB(231, 231, 231);
    viewBottomLine.backgroundColor = CRGB(231, 231, 231);
    [view addSubview:lblSection];
    [view addSubview:viewTopLine];
    [view addSubview:viewBottomLine];
    return view;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *strSection = [arrayData[indexPath.section] valueForKey:@"SectionName"];
    
    NSDictionary *Item =  ((NSArray *)[arrayData[indexPath.section] valueForKey:@"Data"])[indexPath.row];
    
    if ([strSection isEqualToString:CProfile]) {
        // my profile
        
        MyProfileViewController *objMyProfile = [[MyProfileViewController alloc] initWithNibName:@"MyProfileViewController" bundle:nil];
        [self.navigationController pushViewController:objMyProfile animated:true];
    }
    else if ([strSection isEqualToString:CNotification]) //Notification
    {
        NotificationViewController *vcNotification = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
        [self.navigationController pushViewController:vcNotification animated:true];

    }
    else if ([strSection isEqualToString:CSelectLaguage])
    {
        SelectLanguageTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.btnSelect.selected = !cell.btnSelect.selected;
        selectedLanguage = [Item valueForKey:@"RowName"];
        [self UpdateValues];
    }
    else if ([strSection isEqualToString:CLegal])
    {
        if ([[Item valueForKey:@"RowName"] isEqualToString:CSupport]) {
            // support
        }
        else if ([[Item valueForKey:@"RowName"] isEqualToString:CLogout]) {
            
            UIAlertController *alrt = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure want to logout" preferredStyle:UIAlertControllerStyleAlert];
            
            [alrt addAction:[UIAlertAction actionWithTitle:COK style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                
                for (UIViewController *controller in self.navigationController.viewControllers)
                {
                    if ([controller isKindOfClass:[LoginViewController class]])
                    {
                        [self.navigationController popToViewController:controller
                                                              animated:YES];
                        break;
                    }
                }
                
            }]];
            
            [alrt addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alrt animated:true completion:nil];
            

        }
        else
        {
            //Privacy Policy
        }
    }
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
     if (tblSetting.contentSize.height > tblSetting.frame.size.height)
    {
    if (scrollView.contentOffset.y + tblSetting.frame.size.height > tblSetting.contentSize.height + 60.0)
    {
        if(!isNavigatedAlready)
            [self navigateToCamera];
    }
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    
    if (tblSetting.contentSize.height > tblSetting.frame.size.height)
        return NO;
    return YES;
}

- (void)UpdateValues
{
    //Set updated value for selected language
    if ([selectedLanguage isEqualToString:CArabic]) {
        appDelegate.isArabic = YES;
    }
    else
    {
        appDelegate.isArabic = NO;
    }
    [appDelegate changeLanguage];
    
    [self setData];
    
    
}

- (void)setData
{
    [self setTitle:CSettings];

    arrayData = @[@{@"SectionName":CProfile,
                    @"Data":@[
                            @{@"Image":@"Settting_MyProfile",
                              @"RowName":CMyProfile,
                              @"Type":@0 // 0 for > and 1 for checkbox (*** for now language only)
                              }
                            ]},
                  @{@"SectionName":CNotification,
                    @"Data":@[
                            @{@"Image":@"Eetting_Notification",
                              @"RowName":CAppNotification,
                              @"Type":@0
                              }
                            ]},
                  @{@"SectionName":CSelectLaguage,
                    @"Data":@[
                            @{@"Image":@"EnglishLogo",
                              @"RowName":CEnglish,
                              @"Type":@1
                              },
                            @{@"Image":@"ArabicLogo",
                              @"RowName":CArabic,
                              @"Type":@1
                              }
                            ]},
                  @{@"SectionName":CLegal,
                    @"Data":@[
                            @{@"Image":@"Setting_Support",
                              @"RowName":CSupport,
                              @"Type":@0
                              },
                            @{@"Image":@"Setting_privacyPolicy",
                              @"RowName":CPrivacyPolicy,
                              @"Type":@0
                              },
                            @{@"Image":@"",
                              @"RowName":CLogout,
                              @"Type":@0
                              }
                            ]}
                  ];

    [tblSetting reloadData]; //Deselct rest all values


}
-(IBAction)btnLogoutClk:(id)sender
{
    for (UIViewController *controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:[LoginViewController class]])
        {
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            break;
        }
    }

}


@end
