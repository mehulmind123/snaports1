//
//  PreviewViewController.h
//  Snaports
//
//  Created by mac-0004 on 29/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SuperViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "HPGrowingTextView.h"
#import "DKVerticalColorPicker.h"
#import "UIImageView+Draw.h"
@interface PreviewViewController : SuperViewController<HPGrowingTextViewDelegate>
{
    MPMoviePlayerController *theMoviPlayer;
    IBOutlet UIView *viewPlayer;
    IBOutlet UICollectionView *colVImgLayer;
    
    IBOutlet UIView *viewMainButtons;
    IBOutlet UIView *viewImageDrawing;
    IBOutlet HPGrowingTextView *txtLabel;
    IBOutlet NSLayoutConstraint *layoutTxtY,*layoutTxtHeight;
    
    
//    IBOutlet UIImageView *imgView;
    IBOutlet UIButton *btnBack,*btnApply,*btnText,*btnDownload;
    
    // Image Drawing
    IBOutlet UIButton *btnEdit;
    IBOutlet UIButton *btnUpload;
    IBOutlet UIButton *btnEraser;
    IBOutlet UIButton *btnNext;
    IBOutlet UIView *viewTopShadow;
    
}
@property NSURL *videoURL;
@property UIImage *ClickedImage;
@property NSString *SelectedFilter;
@property AVURLAsset* videoAsset;
@property (nonatomic) AVPlayer *avPlayer;

//Image Drawing
@property IBOutlet DKVerticalColorPicker *vertPicker;
@property (strong, nonatomic) IBOutlet UIImageView *myImage;
@property (strong, nonatomic) IBOutlet UIButton *btnUndo;
@property (strong, nonatomic) IBOutlet UIButton *btnRedo;

- (IBAction)start:(id)sender;
- (IBAction)reset:(id)sender;
- (IBAction)save:(id)sender;
- (IBAction)selectRubber:(id)sender;
@end
