//
//  PreviewViewController.m
//  Snaports
//
//  Created by mac-0004 on 29/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "PreviewViewController.h"
#import "imageLayerCollectionViewCell.h"
#import "SendToViewController.h"
@interface PreviewViewController ()
{
    // Image Drawing
    BOOL isFirstTime;
    int currentMode; // 0 for filter 1 for Drawing 3 for Text
    UIImage *drawingImage;
    CGPoint panCoord;
}
@end

@implementation PreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.avPlayer currentItem]];
    
    [colVImgLayer registerNib:[UINib nibWithNibName:@"imageLayerCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"imageLayerCollectionViewCell"];
    
//    [self.myImage resetImage];
    if(_videoURL)
    {
         [self VideoPlayinView];
        //_myImage.hidden = true;
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//           
//        });
        
    }
    else
    {
        viewPlayer.hidden = true;
        _myImage.image = _ClickedImage;
        
    }
    [self initializedImageDrawing];
    [self EneterInExitFromEditMode];
    [self setLableProperty];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    [gradient setFrame:CGRectMake(0, 0, CScreenWidth, viewTopShadow.frame.size.height)];
    gradient.colors = [NSArray arrayWithObjects:(id)[CRGBA(0, 0, 0, 0.3) CGColor], (id)[[UIColor clearColor] CGColor],nil];
    [viewTopShadow.layer insertSublayer:gradient atIndex:0];
    
    btnNext.transform = CGAffineTransformMakeScale(1.2, 1.2);
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self forceStopAndQuite];
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self setNeedsStatusBarAppearanceUpdate];

    if (self.SelectedFilter.length > 0) {
        [self setForEdit];
    }
    else
    {
        [self setForPreview];
    }
     if(_videoURL && ![self isVideoPlaying])
     {
          [self.avPlayer play];
     }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - initialize
-(void)setForPreview
{
    colVImgLayer.hidden = true;
    btnEdit.hidden = true;
    btnText.hidden = true;
    btnDownload.hidden = true;
}
-(void)setForEdit
{
    colVImgLayer.hidden = false;
    btnEdit.hidden = false;
    btnText.hidden = false;
    btnDownload.hidden = false;
}

#pragma mark - Action Events

-(IBAction)btnBack:(id)sender
{
    if(currentMode == 1)
    {
        [self start:nil];
        currentMode = 0;
        return;
    }
    [self.avPlayer pause];
    [self.navigationController popViewControllerAnimated:false];
    
}
-(IBAction)btnVideoEffect:(UIButton *)sender
{
    [self videoOutput];
}
-(IBAction)btnShowhideeffects:(id)sender
{
    colVImgLayer.hidden = !colVImgLayer.hidden;
}

-(IBAction)btnNextClk:(id)sender
{
    if (self.SelectedFilter.length > 0) {
      //  [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:@"Media has been sent"];
        [self.navigationController popViewControllerAnimated:true];
    }
    else
    {
        SendToViewController *objSend = [[SendToViewController alloc] initWithNibName:@"SendToViewController" bundle:nil];
        [self.navigationController pushViewController:objSend animated:true];
    }
}
-(IBAction)btnNextToGallary:(id)sender
{
    
}
-(IBAction)btnCloseClk:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}
-(IBAction)btnTextEditClk:(id)sender
{
    [self showLabel];
}

#pragma mark -
#pragma mark - Image Drawing 
-(void)initializedImageDrawing
{
    self.vertPicker.hidden = self.btnRedo.hidden = self.btnUndo.hidden = YES;
    
    self.vertPicker.layer.shadowColor = [UIColor blackColor].CGColor;
    self.vertPicker.layer.shadowOffset = CGSizeMake(2, 1);
    self.vertPicker.layer.shadowOpacity = .4f;
    
    isFirstTime = YES;
    
    [self.myImage setLineBroadWidth:7];
    [self.myImage setRubberSize:20];
}

#pragma mark action events:

- (IBAction)start:(UIButton *)sender
{
    if(sender.selected)
    {
        return;
    }
    [self.myImage DeselectRubber];
    
    if (!btnEdit.selected)
    {
        currentMode = 1;
        self.vertPicker.hidden = self.btnUndo.hidden = self.btnRedo.hidden = NO;
        
        if (isFirstTime)
        {
            isFirstTime = NO;
            [self.myImage startDrawing];
        }
        [self.myImage MakePause:NO];
        btnNext.selected = btnUpload.selected = btnEraser.selected = NO;
        btnEdit.selected =YES;
    }
    else
    {
         currentMode = 0;
        self.vertPicker.hidden = self.btnUndo.hidden = self.btnRedo.hidden = YES;
        
        [self.myImage MakePause:YES];
        btnEdit.selected = btnNext.selected = btnUpload.selected = btnEraser.selected = NO;
    }
    [self EneterInExitFromEditMode];
}

- (IBAction)reset:(id)sender
{
    self.vertPicker.hidden = NO;
    self.btnUndo.hidden = self.btnRedo.hidden  = YES;
    [self.myImage resetImage];
}

- (IBAction)save:(id)sender
{
    [[PPLoader sharedLoader] ShowHudLoaderWithText:@"Exporting...."];
    imageLayerCollectionViewCell *cell= (imageLayerCollectionViewCell *)[[colVImgLayer visibleCells] firstObject];
    drawingImage = [self.myImage save:false withFilterImage:cell.imgVFilter.image]; // bgimage + draw imge + filter image
    
    if (txtLabel.text.length > 0) {
        UIImage *txtImage = [self imageWithView:txtLabel];
        drawingImage = [self BGImage:drawingImage withtxtImg:txtImage];
    }
    
    if (self.videoURL) {
        [self videoOutput];
    }
    else
    {
         UIImageWriteToSavedPhotosAlbum(drawingImage, self,@selector(image:didFinishSavingWithError:contextInfo:), nil);
        // drawing image to photo libarary
    }
    self.vertPicker.hidden = true;
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    [[PPLoader sharedLoader] HideHudLoader];
    if (error != NULL)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Image could not be saved.Please try again"  delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Close", nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Image is successfully saved in your photo album"  delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Okay", nil];
        [alert show];
    }
}
- (UIImage*)BGImage:(UIImage *)backgroundImage withtxtImg:(UIImage *)txtImg {
    
    CGSize ImageSize = backgroundImage.size;
    UIGraphicsBeginImageContext(ImageSize);
    [backgroundImage drawInRect:CGRectMake(0, 0, ImageSize.width, ImageSize.height)];
    
    float y = CViewY(txtLabel) * ImageSize.height / CScreenHeight;
    float height = CViewHeight(txtLabel) * ImageSize.height / CScreenHeight;
    float width = CViewWidth(txtLabel) * ImageSize.width / CScreenWidth;
    [txtImg drawInRect:CGRectMake(0, y, width, height)];
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}


-(void)colorPicked:(UIColor *)color
{
    [self.myImage setColor:color];
}

- (IBAction)selectRubber:(id)sender
{
    if (!btnEraser.selected)
    {
        self.vertPicker.hidden =NO;
        
        btnNext.selected = btnUpload.selected = btnEdit.selected = NO;
        btnEraser.selected =YES;
        self.vertPicker.hidden = true;
        [self.myImage selectRubber];
    }
    else
    {
        btnNext.selected = btnUpload.selected = btnEraser.selected = NO;
        btnEdit.selected  = YES;
        self.vertPicker.hidden = false;
        [self.myImage DeselectRubber];
    }
}
- (IBAction)selectBrush:(id)sender
{
    self.vertPicker.hidden = NO;
    [self.myImage selectRubber];
}

-(IBAction)btnTabClicked:(UIButton *)sender
{
    switch (sender.tag) {
        case 100:
        {
            [self.myImage DoUndo];
        }
            break;
        case 101:
        {
            [self.myImage DoRedo];
        }
            break;
            
        default:
            break;
    }
}

-(void)EneterInExitFromEditMode
{
    if(currentMode == 1)//entring
    {
        //enabled
        _btnUndo.hidden = false;
        _btnRedo.hidden = false;
        
        
        //disabled
        btnText.hidden = true;
        colVImgLayer.userInteractionEnabled = false;
        txtLabel.userInteractionEnabled = false;
        
        
    }
    else
    {
        _btnUndo.hidden = true;
        _btnRedo.hidden = true;
        btnText.hidden = false;
        colVImgLayer.userInteractionEnabled = true;
        txtLabel.userInteractionEnabled = true;
    }
}
#pragma mark - Image Done


#pragma mark -
#pragma mark - Helper Method Video
-(void)VideoPlayinView
{
    
    NSString *outputPath = _videoURL.path;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (!self.avPlayer) {
        self.avPlayer = [AVPlayer playerWithURL:_videoURL];
        
        self.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
        videoLayer.frame = CScreenRect;//self.view.bounds;
        videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        [viewPlayer.layer addSublayer:videoLayer];
        
        
    }
    [self VideoPlayPause:[self isVideoPlaying]];
    
}
-(BOOL)isVideoPlaying
{
    
    if (self.avPlayer && (self.avPlayer.rate != 0) && (self.avPlayer.error == nil)) {
        NSLog(@"rate : %f",self.avPlayer.rate);
        return true;
        // player is playing
    }
    return false;
}
-(void)VideoPlayPause:(BOOL)isPlaying
{
    if (isPlaying) {
        [self.avPlayer pause];
//        btnPlay.selected = false;
    }
    else
    {
        [self.avPlayer play];
//        btnPlay.selected = true;
    }
}
-(void)forceStopAndQuite
{
    @try {
        [self.avPlayer pause];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[self.avPlayer currentItem]];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}
#pragma mark -

- (void)videoOutput
{
    // 1 - Early exit if there's no video file selected
    self.videoAsset = [[AVURLAsset alloc]initWithURL:self.videoURL options:nil];
    
    
    if (!self.videoAsset) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Load a Video Asset First"
                                                       delegate:nil cancelButtonTitle:COK otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    // 2 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
    AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
    
    // 3 - Video track
    AVMutableCompositionTrack *videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
                                                                        preferredTrackID:kCMPersistentTrackID_Invalid];
    
    [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, self.videoAsset.duration)
                        ofTrack:[[self.videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0]
                         atTime:kCMTimeZero error:nil];
    
    // 4- audio track
    AVMutableCompositionTrack *compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    
    [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, _videoAsset.duration)
                                   ofTrack:[[_videoAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0]
                                    atTime:kCMTimeZero error:nil];
    // 3.1 - Create AVMutableVideoCompositionInstruction
    AVMutableVideoCompositionInstruction *mainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    mainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, self.videoAsset.duration);
    
    // 3.2 - Create an AVMutableVideoCompositionLayerInstruction for the video track and fix the orientation.
    AVMutableVideoCompositionLayerInstruction *videolayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    AVAssetTrack *videoAssetTrack = [[self.videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    UIImageOrientation videoAssetOrientation_  = UIImageOrientationUp;
    BOOL isVideoAssetPortrait_  = NO;
    CGAffineTransform videoTransform = videoAssetTrack.preferredTransform;
    if (videoTransform.a == 0 && videoTransform.b == 1.0 && videoTransform.c == -1.0 && videoTransform.d == 0) {
        videoAssetOrientation_ = UIImageOrientationRight;
        isVideoAssetPortrait_ = YES;
    }
    if (videoTransform.a == 0 && videoTransform.b == -1.0 && videoTransform.c == 1.0 && videoTransform.d == 0) {
        videoAssetOrientation_ =  UIImageOrientationLeft;
        isVideoAssetPortrait_ = YES;
    }
    if (videoTransform.a == 1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == 1.0) {
        videoAssetOrientation_ =  UIImageOrientationUp;
    }
    if (videoTransform.a == -1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == -1.0) {
        videoAssetOrientation_ = UIImageOrientationDown;
    }
    [videolayerInstruction setTransform:videoAssetTrack.preferredTransform atTime:kCMTimeZero];
    [videolayerInstruction setOpacity:0.0 atTime:self.videoAsset.duration];
    
    // 3.3 - Add instructions
    mainInstruction.layerInstructions = [NSArray arrayWithObjects:videolayerInstruction,nil];
    
    AVMutableVideoComposition *mainCompositionInst = [AVMutableVideoComposition videoComposition];
    //AVMutableAudioMix *mainAudioInst = [AVMutableAudioMix audioMix];
    
    CGSize naturalSize;
    if(isVideoAssetPortrait_){
        naturalSize = CGSizeMake(videoAssetTrack.naturalSize.height, videoAssetTrack.naturalSize.width);
    } else {
        naturalSize = videoAssetTrack.naturalSize;
    }
    
    float renderWidth, renderHeight;
    renderWidth = naturalSize.width;
    renderHeight = naturalSize.height;
    mainCompositionInst.renderSize = CGSizeMake(renderWidth, renderHeight);
    mainCompositionInst.instructions = [NSArray arrayWithObject:mainInstruction];
    mainCompositionInst.frameDuration = CMTimeMake(1, 30);
    
    [self applyVideoEffectsToComposition:mainCompositionInst size:naturalSize];
    
    // 4 - Get path
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"FinalVideo-%d.mov",arc4random() % 1000]];
    NSURL *url = [NSURL fileURLWithPath:myPathDocs];
    
    // 5 - Create exporter
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition
                                                                      presetName:AVAssetExportPresetHighestQuality];
    exporter.outputURL=url;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    exporter.shouldOptimizeForNetworkUse = YES;
    exporter.videoComposition = mainCompositionInst;
    //exporter.audioMix = mainAudioInst;
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self exportDidFinish:exporter];
            
            NSError *error = exporter.error;
            NSLog(@"%@",error);
            self.avPlayer = nil;
            _videoURL = exporter.outputURL;
            [[PPLoader sharedLoader] HideHudLoader];
        });
    }];
}

- (void)exportDidFinish:(AVAssetExportSession*)session {
    if (session.status == AVAssetExportSessionStatusCompleted) {
        NSURL *outputURL = session.outputURL;
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
            [library writeVideoAtPathToSavedPhotosAlbum:outputURL completionBlock:^(NSURL *assetURL, NSError *error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (error) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Saving Failed"
                                                                       delegate:nil cancelButtonTitle:COK otherButtonTitles:nil];
                        [alert show];
                    } else {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Saved" message:@"Saved To Photo Album"
                                                                       delegate:self cancelButtonTitle:COK otherButtonTitles:nil];
                        [alert show];
                    }
                });
            }];
        }
    }
}


- (void)applyVideoEffectsToComposition:(AVMutableVideoComposition *)composition size:(CGSize)size
{
    CALayer *overlayLayer = [CALayer layer];
    UIImage *overlayImage = nil;
    
    overlayImage = drawingImage;//[UIImage imageNamed:@"filter_screen.png"]; // [self imageFromColor:nil]; //[UIImage imageNamed:@"1.png"];
   
    [overlayLayer setContents:(id)[overlayImage CGImage]];
    overlayLayer.frame = CGRectMake(0, 0, size.width, size.height);
    [overlayLayer setMasksToBounds:YES];
    
    // 2 - set up the parent layer
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, size.width, size.height);
    videoLayer.frame = CGRectMake(0, 0, size.width, size.height);
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:overlayLayer];
    
    // 3 - apply magic
    composition.animationTool = [AVVideoCompositionCoreAnimationTool
                                 videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    
}


-(void) video: (NSString *) videoPath didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    if(error)
        NSLog(@"Finished saving video with error: %@", error);
}

- (UIImage *)imageFromColor:(UIColor *)color {
    if(!color)
    {
        NSInteger CurrentRow =  [[colVImgLayer indexPathsForVisibleItems]firstObject].row;
        color = [UIColor colorWithRed:(CurrentRow * 25.0/255.0) green:(CurrentRow / 25.0) blue:1.0/(CurrentRow+1.0) alpha:0.4];
        if(CurrentRow == 0)
            color = [UIColor clearColor];
    }
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark - Notification fire
- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}

#pragma mark - Helper Method Image

#pragma mark - collection View

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 2;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return  1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"imageLayerCollectionViewCell";
    imageLayerCollectionViewCell *cell = (imageLayerCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    if (cell == nil)
        cell = [[imageLayerCollectionViewCell alloc] init];
    
    cell.imgVFilter.image = [UIImage imageNamed:@"filter_screen.png"];
    
    if(indexPath.row == 0)
    {
        cell.imgVFilter.image = nil;
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return collectionView.bounds.size;
}

#pragma mark - TextLable 
-(void)setLableProperty
{
    layoutTxtY.constant = CScreenHeight/2;
    txtLabel.delegate = self;
//    txtLabel.placeholder = @"Type your message above";
    txtLabel.font = CFontGilroyMedium(17);
    txtLabel.textColor = [UIColor whiteColor]; //CRGB(133, 133, 133);
    txtLabel.maxNumberOfLines = 100;
    txtLabel.minNumberOfLines = 1;
    txtLabel.animateHeightChange = YES;
    txtLabel.textAlignment = NSTextAlignmentCenter;
    txtLabel.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.3];
    UIPanGestureRecognizer *panGes = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragging:)];
    [txtLabel addGestureRecognizer:panGes];
    
    [self hideTextlabel];
}
-(void)hideTextlabel
{
    txtLabel.hidden = true;
}
-(void)showLabel
{
    txtLabel.hidden = false;
    [txtLabel becomeFirstResponder];
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    NSLog(@"%f",height);
    layoutTxtHeight.constant = height;
}
- (BOOL)growingTextViewShouldBeginEditing:(HPGrowingTextView *)growingTextView;
{
    return true;
}
- (BOOL)growingTextViewShouldEndEditing:(HPGrowingTextView *)growingTextView;
{
    if (growingTextView.text.length == 0) {
        [self hideTextlabel];
    }
    return true;
}

-(void)dragging:(UIPanGestureRecognizer *)gesture
{
    if(gesture.state == UIGestureRecognizerStateBegan)
    {
        panCoord = [gesture locationInView:gesture.view];
    }
    CGPoint newCoord = [gesture locationInView:gesture.view];
//    float dX = newCoord.x-panCoord.x;
    float dY = newCoord.y-panCoord.y;
    
    layoutTxtY.constant = layoutTxtY.constant - dY;
    
    if(layoutTxtY.constant < 0)
    {
        layoutTxtY.constant = 0;
    }
    if(layoutTxtY.constant > CScreenHeight - layoutTxtHeight.constant)
    {
        layoutTxtY.constant = CScreenHeight - layoutTxtHeight.constant;
    }
//    gesture.view.frame = CGRectMake(gesture.view.frame.origin.x+dX, gesture.view.frame.origin.y+dY, gesture.view.frame.size.width, gesture.view.frame.size.height);
}

- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, [[UIScreen mainScreen] scale]);
    
    
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
@end
