//
//  EventActivityTableViewCell.h
//  Snaports
//
//  Created by mac-0004 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventActivityTableViewCell : UITableViewCell

@property IBOutlet UIImageView *imgProfile;
@property IBOutlet UIView *viewImgOuter,*shadowView;
@property IBOutlet UILabel *lblDescription, *lblTime;
@property IBOutlet UIImageView *imgIcon,*imgPlayIcon;
@property IBOutlet UIButton *btnPlay;
@end
