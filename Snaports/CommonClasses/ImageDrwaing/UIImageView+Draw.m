//
//  UIView+Draw.m
//  GTImageViewDraw
//
//  Created by Gianluca Tursi on 25/03/14.
//  Copyright (c) 2014 Gianluca Tursi. All rights reserved.
//

#import "UIImageView+Draw.h"


@implementation UIImageView (Draw)

UIImage *mainImg;
UIImageView *mainImageView;

BOOL _isErasing = NO;
BOOL activate = NO;
CGPoint lastPoint;
CGFloat red = 0.0/255.0;
CGFloat green = 0.0/255.0;
CGFloat blue = 0.0/255.0;
CGFloat brush = 7.0;
CGFloat RubberSize = 7.0;
CGFloat opacity = 1.0;
BOOL mouseSwiped;
NSMutableArray *arrStorage;
NSMutableArray *arrImages;
int intValue;

-(void)startDrawing{
    
    activate = YES;
    self.userInteractionEnabled = YES;
    mainImg = self.image;
    
//    CGRect frame = [[UIScreen mainScreen] bounds];
//......Scale to fit the screen.....
//    CGFloat oriWidth = frame.size.width;
//    CGFloat oriHeight = (mainImg.size.height) * (oriWidth / mainImg.size.width);
//    CGFloat oriX = (frame.size.width - oriWidth) / 2;
//    CGFloat oriY = (frame.size.height - oriHeight) / 2;
//    
//    mainImageView = [[UIImageView alloc] initWithFrame:CGRectMake(oriX, oriY, oriWidth, oriHeight)];
    mainImageView = [[UIImageView alloc] initWithFrame:CScreenRect];
    [self addSubview:mainImageView];
    
    arrStorage = [[NSMutableArray alloc] init];
    arrImages = [[NSMutableArray alloc] init];
    
}

-(void)stopDrawing{
    
    activate = NO;
    
}

-(void)MakePause:(BOOL)isPause{
    
    if (isPause)
        activate = NO;
    else
        activate = YES;
}

-(void)resetImage{
    
    _isErasing = NO;
    [mainImageView removeFromSuperview];
    mainImageView = nil;
    mainImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    [self addSubview:mainImageView];
    
    
    
}

-(void)setLineBroadWidth:(CGFloat)size {
    brush = size;
}

-(void)setRubberSize:(CGFloat)size {
    RubberSize = size;
}

-(void) setColor:(UIColor *) color{
    
    _isErasing = NO;
    [color getRed:&red green:&green blue:&blue alpha:&opacity];
    
    
}

- (UIImage*)imageByCombiningImage:(UIImage*)firstImage withImage:(UIImage*)secondImage
{
    CGSize newImageSize = mainImageView.frame.size;
    
    UIGraphicsBeginImageContext(newImageSize);
    
    UIGraphicsBeginImageContextWithOptions(newImageSize, NO, 0.0);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    [mainImageView.superview.layer renderInContext:context];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //    if (UIGraphicsBeginImageContextWithOptions != NULL) {
    //        UIGraphicsBeginImageContextWithOptions(newImageSize, NO, [[UIScreen mainScreen] scale]);
    //    } else {
    //        UIGraphicsBeginImageContext(newImageSize);
    //    }
    //    [firstImage drawAtPoint:CGPointMake(roundf((newImageSize.width-firstImage.size.width)/2),
    //                                        roundf((newImageSize.height-firstImage.size.height)/2))];
    //    [secondImage drawAtPoint:CGPointMake(roundf((newImageSize.width-secondImage.size.width)/2),
    //                                         roundf((newImageSize.height-secondImage.size.height)/2))];
    //
    //    image = UIGraphicsGetImageFromCurrentImageContext();
    //    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage*) maskImage:(UIImage *)backgroundImage withMask:(UIImage *)watermarkImage {
    
    CGSize ImageSize = backgroundImage.size;
    if(backgroundImage.size.height < watermarkImage.size.height)
    {
        ImageSize = watermarkImage.size;
    }
    UIGraphicsBeginImageContext(ImageSize);
    [backgroundImage drawInRect:CGRectMake(0, 0, ImageSize.width, ImageSize.height)];
    [watermarkImage drawInRect:CGRectMake(0, 0, ImageSize.width, ImageSize.height)];
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}


-(UIImage *)save:(bool)isTosave withFilterImage:(UIImage *)FilterImage
{
    
    UIImage *imageToSave = [self maskImage:mainImg withMask:mainImageView.image];
    if (!imageToSave) {
        imageToSave = self.image;
    }
    
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO,0.0);
    [imageToSave drawInRect:CScreenRect];
    UIImage *SaveImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    if (FilterImage) {
        SaveImage = [self maskImage:SaveImage withMask:FilterImage];
    }
    if (isTosave) {
        UIImageWriteToSavedPhotosAlbum(SaveImage, self,@selector(image:didFinishSavingWithError:contextInfo:), nil);
    }
    return SaveImage;
    
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    
    if (error != NULL)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Image could not be saved.Please try again"  delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Close", nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Image is successfully saved in your photo album"  delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Okay", nil];
        [alert show];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if(activate){
        NSLog(@"touchbegan");
        mouseSwiped = NO;
        UITouch *touch = [touches anyObject];
        lastPoint = [touch locationInView:self];
    }
}

-(void)selectRubber{
    
    _isErasing = YES;
    
}

-(void)DeselectRubber{
    
    _isErasing = NO;
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if(activate){
        NSLog(@"touchmoved");
        mouseSwiped = YES;
        UITouch *touch = [touches anyObject];
        CGPoint currentPoint = [touch locationInView:self];
        
        UIGraphicsBeginImageContext(self.frame.size);
        [mainImageView.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
        CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
        CGContextSetBlendMode(UIGraphicsGetCurrentContext(),kCGBlendModeNormal);
        
        if (_isErasing) {
            CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeClear);
            CGContextSetLineWidth(UIGraphicsGetCurrentContext(), RubberSize);
        }
        else {
            CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, 1.0);
            CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush );
        }
        
        CGContextStrokePath(UIGraphicsGetCurrentContext());
        mainImageView.image = UIGraphicsGetImageFromCurrentImageContext();
        [mainImageView setAlpha:opacity];
        UIGraphicsEndImageContext();
        
        lastPoint = currentPoint;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if ([arrStorage count] < 1000) {
        if(!mouseSwiped) {
            UIGraphicsBeginImageContext(self.frame.size);
            [mainImageView.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
            CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
            CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush);
            CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, 1.0);
            CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
            CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
            CGContextStrokePath(UIGraphicsGetCurrentContext());
            CGContextFlush(UIGraphicsGetCurrentContext());
            mainImageView.image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            mainImageView.backgroundColor = [UIColor clearColor];
        }
        
        NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%lu_drawslide__.png", (unsigned long)[arrStorage count]]];
        
        [arrStorage addObject:path];
        [arrImages addObject:path];
        
        intValue = (int) arrStorage.count;
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:mainImageView.image, path, nil];
        [NSThread detachNewThreadSelector:@selector(backuppmage:) toTarget:self withObject:params];
    }
}

-(void) backuppmage:(NSMutableDictionary *) params
{
    for (NSString *key in params) {
        UIImage *image = (UIImage *)[params objectForKey:key];
        [UIImagePNGRepresentation(image) writeToFile:key atomically:YES];
    }
}

// add this method to button click
-(void)DoUndo
{
    if (arrStorage.count == 0)
        return;
    
    if ([arrStorage count] > 0) {
        [arrStorage removeObject:[arrStorage lastObject]];
    }
    
    if ([arrStorage count] > 0 && [arrStorage lastObject] != nil) {
        mainImageView.image = [self getImageWithURL:(NSString *)[arrStorage lastObject]];
    } else {
        mainImageView.image = nil;
    }
    intValue--;
}

-(void)DoRedo
{
    if ([arrStorage count] == [arrImages count])
        return;
    
    if (intValue >= 0)
    {
        [arrStorage addObject:[arrImages objectAtIndex:intValue]];
        
        if ([arrStorage count] > 0) {
            mainImageView.image = [self getImageWithURL:(NSString *)[arrStorage objectAtIndex:intValue]];
        } else {
            mainImageView.image = nil;
        }
        intValue++;
    }
}


-(UIImage *) getImageWithURL:(NSString *)url
{
    return [UIImage imageWithContentsOfFile:url];
}


@end
