//
//  BeaconManager.h
//  Master
//
//  Created by mac-0001 on 23/01/15.
//  Copyright (c) 2015 mac-0001. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void (^EnteredIntoBeaconRegion)(CLBeaconRegion *region);
typedef void (^BeaconRangeUpdated)(CLBeaconRegion *region, CLBeacon *beacon);
typedef void (^ExitedFromBeaconRegion)(CLBeaconRegion *region);


@interface BeaconManager : NSObject

@property (nonatomic,strong) CLLocationManager *locationManager;

+ (instancetype)sharedInstance;

- (void)startRangingWithProximityUUID:(NSUUID *)proximityUUID identifier:(NSString *)identifier;
- (void)startRangingProximityUUID:(NSUUID *)proximityUUID major:(CLBeaconMajorValue)major identifier:(NSString *)identifier;
- (void)startRangingProximityUUID:(NSUUID *)proximityUUID major:(CLBeaconMajorValue)major minor:(CLBeaconMinorValue)minor identifier:(NSString *)identifier;
-(void)resetRangingBeacons;
-(void)removePreviousBeacons;
-(void)lookingForBeacons:(EnteredIntoBeaconRegion)enter ranging:(BeaconRangeUpdated)ranging exit:(ExitedFromBeaconRegion)exit;

@end
