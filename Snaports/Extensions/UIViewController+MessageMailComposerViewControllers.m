//
//  UIViewController+MessageMailComposerViewControllers.m
//  MI API Example
//
//  Created by mac-0001 on 11/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import "UIViewController+MessageMailComposerViewControllers.h"

#import "NSObject+NewProperty.h"


static NSString *const MFMailComposeViewControllerHandler = @"MFMailComposeViewControllerHandler";
static NSString *const MFMessageComposeViewControllerHandler = @"MFMessageComposeViewControllerHandler";

@implementation UIViewController (MessageMailComposerViewControllers)

#pragma mark - Mail Composer

-(void)openMailComposer:(NSString *)subject recepients:(NSArray *)recepients body:(NSString *)body isHTML:(BOOL)isHtml completion:(MailCompletionHandler)completion
{
    [self setObject:completion forKey:MFMailComposeViewControllerHandler];
    [self openMailComposer:subject recepients:recepients body:body isHTML:isHtml];
}

-(void)openMailComposer:(NSString *)subject recepients:(NSArray *)recepients body:(NSString *)body isHTML:(BOOL)isHtml
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] init];
        composeViewController.mailComposeDelegate = self;
        
        [composeViewController setSubject:subject];
        [composeViewController setMessageBody:body isHTML:isHtml];
        [composeViewController setToRecipients:recepients];
        
        [self presentViewController:composeViewController animated:YES completion:nil];
    }
    else
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:MILocalizedString(@"MFMailComposeViewControllerCanNotSendMail", @"Can not send Mail")];
    }

}
    
#pragma mark - MFMailComposeViewControllerDelegate
    
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (error)
        NSLog(@"MFMailComposeViewController Error === %@", error);
    else
        NSLog(@"MFMailComposeViewController Result === %u", result);
    
    
    [self dismissViewControllerAnimated:YES completion:^{

        MailCompletionHandler handler = [self objectForKey:MFMailComposeViewControllerHandler];
        if (handler)
            handler(result);
    }];
}

#pragma mark - Message Composer

-(void)openMessageComposer:(NSString *)subject recepients:(NSArray *)recepients body:(NSString *)body completion:(MessageCompletionHandler)completion
{
    [self setObject:completion forKey:MFMessageComposeViewControllerHandler];
    [self openMessageComposer:subject recepients:recepients body:body];
}

-(void)openMessageComposer:(NSString *)subject recepients:(NSArray *)recepients body:(NSString *)body
{
    if ([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *composeViewController = [[MFMessageComposeViewController alloc] init];
        composeViewController.messageComposeDelegate = self;
        
        [composeViewController setSubject:subject];
        [composeViewController setBody:body];
        [composeViewController setRecipients:recepients];
        
        [self presentViewController:composeViewController animated:YES completion:nil];
    }
    else
    {
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:MILocalizedString(@"MFMessageComposeViewControllerCanNotSendMail",@"Can not send Message")];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    NSLog(@"MFMessageComposeViewController Result === %u", result);
    
    [self dismissViewControllerAnimated:YES completion:^{

        MessageCompletionHandler handler = [self objectForKey:MFMessageComposeViewControllerHandler];
        if (handler)
            handler(result);
    }];
}


#pragma mark - Send Message

+(void)sendEmailToDeveloper:(NSString *)subject body:(NSString *)body completion:(MIBooleanResultBlock)completion
{
    NSString *subjectString = [[CApplicationName stringByAppendingFormat:@": %@: %@",CVersionNumber,CBuildNumber] stringByAppendingString:subject];
    NSString *receipientString = [UIApplication developerEmail]?:@"michalgreg2013@gmail.com";
    NSString *bodyString = [[NSString stringWithFormat:@"%@ : %@\n\n\n",[UIDevice platformString],[[UIDevice currentDevice] systemVersion]] stringByAppendingString:body];
    [self sendEmail:subjectString recepients:receipientString body:bodyString completion:completion];
}

+(void)sendEmail:(NSString *)subject recepients:(NSString *)recepient body:(NSString *)body completion:(MIBooleanResultBlock)completion
{
    return;
    
    if (IS_IPHONE_SIMULATOR)
    {
        NSLog(@"##########################");

        NSLog(@"Sending mail to %@",recepient);
        NSLog(@"Subject: %@",subject);
        NSLog(@"Body: %@",body);
        
        NSLog(@"##########################");
        return;
    }

    @try {
        SKPSMTPMessage *emailMessage = [[SKPSMTPMessage alloc] init];
        emailMessage.fromEmail = @"michalgreg2013@gmail.com"; //sender email address
        emailMessage.toEmail = recepient;  //receiver email address
        
        emailMessage.relayHost = @"smtp.gmail.com";
        //    emailMessage.relayHost = @"smtp.live.com";
        //    emailMessage.relayHost = @"smtp.yahoo.com";
        
        emailMessage.requiresAuth = YES;
        emailMessage.login = @"michalgreg2013@gmail.com";
        emailMessage.pass = @"mind@123";
        
        emailMessage.subject = subject;
        emailMessage.wantsSecure = YES;
        emailMessage.delegate = self;
        NSString *messageBody = body;
        NSDictionary *plainMsg = [NSDictionary
                                  dictionaryWithObjectsAndKeys:@"text/plain",kSKPSMTPPartContentTypeKey,
                                  messageBody,kSKPSMTPPartMessageKey,@"8bit",kSKPSMTPPartContentTransferEncodingKey,nil];
        
        emailMessage.parts = @[plainMsg];
        [emailMessage send];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

-(void)messageSent:(SKPSMTPMessage *)message{
    NSLog(@"SKPSMTPMessage - message sent");
}

-(void)messageFailed:(SKPSMTPMessage *)message error:(NSError *)error{
    NSLog(@"SKPSMTPMessage - error(%ld): %@", (long)[error code], error);
}


@end
