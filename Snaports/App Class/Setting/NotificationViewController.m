//
//  NotificationViewController.m
//  Snaports
//
//  Created by mac-00018 on 28/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "NotificationViewController.h"
#import "NotificationTableViewCell.h"
#import "SeasonListViewController.h"

@interface NotificationViewController ()
{
    NSArray *arrNotification;
}

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:CNotifications];
    [self.tblNotfication setEstimatedRowHeight:81];
    [self.tblNotfication setRowHeight:UITableViewAutomaticDimension];
    [self setNotificationDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- TableView Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrNotification.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *strCellId = @"Cell";
    NotificationTableViewCell *cell = (NotificationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:strCellId];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"NotificationTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSDictionary *dictAtIndexPath = [arrNotification objectAtIndex:indexPath.row];
    //Set notificaiton
    cell.lblNotificationMessage.text = [dictAtIndexPath valueForKey:@"Notification"];
    
    //Set notification time
    cell.lblNotificationTime.text = [dictAtIndexPath valueForKey:@"Time"];
    
    //Set the color on the leading portion based on notification status
    cell.viwNotificationStatus.backgroundColor = [UIColor clearColor];
    
    if ([[dictAtIndexPath valueForKey:@"Status"] isEqualToString:@"Incorrect"]) {
        cell.viwNotificationStatus.backgroundColor = CRGB(255, 42, 69);
    }
    else if ([[dictAtIndexPath valueForKey:@"Status"] isEqualToString:@"Correct"])
    {
        cell.viwNotificationStatus.backgroundColor = CRGB(0, 181, 70);

    }
    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SeasonListViewController *vcSeasonList = [[SeasonListViewController alloc] initWithNibName:@"SeasonListViewController" bundle:nil];
    [self.navigationController pushViewController:vcSeasonList animated:YES];
}

- (void)setNotificationDetails
{
    arrNotification = @[
                                 @{
                                     @"Notification" : @"Oh no! Your prediction for spain to beat italy was incorrect!",
                                     @"Time" : @"2 hours ago",
                                     @"Status" : @"Incorrect"
                                     },
                                 @{
                                     @"Notification" : @"10 incorrect in a row! Almost like winning but not #redscreen",
                                     @"Time" : @"2 hours ago",
                                     @"Status" : @""
                                     },
                                 @{
                                     @"Notification" : @"Oh no! Your prediction for Queen's Park to beat Alnion Rovers was incorrect!",
                                     @"Time" : @"3 hours ago",
                                     @"Status" : @"Incorrect"
                                     },
                                 @{
                                     @"Notification" : @"Great Great!Your prediction of Bury to beat Scunthorpe United was correct!",
                                     @"Time" : @"5 hours ago",
                                     @"Status" : @"Correct"
                                     },
                                 @{
                                     @"Notification" : @"Oh no! Your prediction for spain to beat italy was incorrect!",
                                     @"Time" : @"6 hours ago",
                                     @"Status" : @"Incorrect"
                                     },
                                 @{
                                     @"Notification" : @"10 incorrect in a row! Almost like winning but not #redscreen",
                                     @"Time" : @"12 hours ago",
                                     @"Status" : @""
                                     },
                                 @{
                                     @"Notification" : @"Oh no! Your prediction for Queen's Park to beat Alnion Rovers was incorrect!",
                                     @"Time" : @"15 hours ago",
                                     @"Status" : @"Incorrect"
                                     },
                                 @{
                                     @"Notification" : @"Great Great!Your prediction of Bury to beat Scunthorpe United was correct!",
                                     @"Time" : @"18 hours ago",
                                     @"Status" : @"Correct"
                                     },
                                 @{
                                     @"Notification" : @"10 incorrect in a row! Almost like winning but not #redscreen",
                                     @"Time" : @"20 hours ago",
                                     @"Status" : @""
                                     },
                                 
                                 
                                 ];
    [self.tblNotfication reloadData];
}
@end
