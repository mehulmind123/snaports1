//
//  SendToTableViewCell.m
//  Snaports
//
//  Created by mac-0004 on 23/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SendToTableViewCell.h"

@implementation SendToTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    if (appDelegate.configureFlipToMirror) {
        appDelegate.configureFlipToMirror(self.viwContent, true);
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
