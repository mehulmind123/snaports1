//
//  SelectLanguageTableViewCell.h
//  Snaports
//
//  Created by mac-00018 on 23/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectLanguageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viwContent;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imgFlag;
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;

@end
