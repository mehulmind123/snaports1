//
//  ResetPasswordViewController.h
//  Snaports
//
//  Created by mac-00018 on 23/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SuperViewController.h"

@interface ResetPasswordViewController : SuperViewController
@property (weak, nonatomic) IBOutlet CommonTextField *txtOTPCode;
@property (weak, nonatomic) IBOutlet CommonTextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet CommonTextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UIView *viwContent;

@end
