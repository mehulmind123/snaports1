//
//  AppDelegate.h
//  Snaports
//
//  Created by mac-00018 on 22/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#define CCureentLanguageStore @"currentStoredLanguage"
#import "YZSwipeBetweenViewController.h"
#import <UIKit/UIKit.h>
#import "YZSwipeBetweenViewController.h"
#import "CameraViewController.h"
#import "EventsLiveViewController.h"
#import "PredicationViewController.h"
#import "EventDetailsViewController.h"

typedef void(^HandleSwipeGesture)(UIViewController *vc,
UISwipeGestureRecognizerDirection *direction);
typedef void(^flipToMirror)(UIView *viewOriginal, BOOL IsRotateMain);
typedef void(^flipSingleview)(UIView *view);
typedef void(^filpToMirrorSubViews)(UIView *view);

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    SuperNavigationController *nav;
}

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, copy) HandleSwipeGesture configureSwipeGesture;
@property (nonatomic, copy) flipToMirror configureFlipToMirror;
@property (nonatomic, copy) flipSingleview configureFlipSingleView;

@property (strong) YZSwipeBetweenViewController *swipeBetweenVC;

@property BOOL isArabic;
- (void)changeLanguage;
- (NSString *)getLanguage;
- (void)setupRootViewControllerForWindow;

-(void)UpdateRootViewControllerForWindow;



@end

