//
//  SeasonListTableViewCell.h
//  Snaports
//
//  Created by mac-00018 on 29/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeasonListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePicture;
@property (weak, nonatomic) IBOutlet UILabel *lblRank;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIView *viwPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblPoints;

@end
