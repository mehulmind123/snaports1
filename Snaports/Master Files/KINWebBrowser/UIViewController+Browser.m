//
//  UIViewController+Browser.m
//  Master
//
//  Created by mac-0001 on 19/12/14.
//  Copyright (c) 2014 mac-0001. All rights reserved.
//

#import "UIViewController+Browser.h"

#import "Constants.h"

@implementation UIViewController (Browser)

-(KINWebBrowserViewController *)openURLWithinApplication:(NSString *)url
{
    UIViewController *controller = self;
    
    if ([controller isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *navigationController = (UINavigationController *)controller;
        
        KINWebBrowserViewController *webBrowser = [KINWebBrowserViewController webBrowser];
        [webBrowser setDelegate:nil];
        [navigationController pushViewController:webBrowser animated:YES];
        [webBrowser loadURLString:url];
        return webBrowser;
    }
    else
    {
        UINavigationController *webBrowserNavigationController = [KINWebBrowserViewController navigationControllerWithWebBrowser];
        KINWebBrowserViewController *webBrowser = [webBrowserNavigationController rootWebBrowser];
        [webBrowser setDelegate:nil];
        webBrowser.showsURLInNavigationBar = YES;
        webBrowser.tintColor =self.navigationController.navigationBar.tintColor;
        webBrowser.barTintColor = self.navigationController.navigationBar.barTintColor;
        webBrowser.showsPageTitleInNavigationBar = NO;
        webBrowser.showsURLInNavigationBar = NO;
        [controller presentViewController:webBrowserNavigationController animated:YES completion:nil];
        [webBrowser loadURLString:url];
        return webBrowser;
    }
}

@end
