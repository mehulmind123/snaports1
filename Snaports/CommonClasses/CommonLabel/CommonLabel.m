//
//  CommonLabel.m
//  Snaports
//
//  Created by mac-00018 on 05/01/17.
//  Copyright © 2017 mac-00018. All rights reserved.
//

#import "CommonLabel.h"

@implementation CommonLabel

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.text = CLocalize(self.text);
    
}

@end
