//
//  UIApplication+Browser.m
//  Master
//
//  Created by mac-0001 on 19/12/14.
//  Copyright (c) 2014 mac-0001. All rights reserved.
//

#import "UIApplication+Browser.h"

#import "Constants.h"
#import "UIApplication+Extension.h"

@implementation UIApplication (Browser)

+(KINWebBrowserViewController *)openURLWithinApplication:(NSString *)url
{
    UIViewController *controller = [UIApplication topMostController];
    
    if ([controller isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *navigationController = (UINavigationController *)controller;
        
        KINWebBrowserViewController *webBrowser = [KINWebBrowserViewController webBrowser];
        [webBrowser setDelegate:nil];
        [navigationController pushViewController:webBrowser animated:YES];
        [webBrowser loadURLString:url];
        return webBrowser;
    }
    else
    {
        UINavigationController *webBrowserNavigationController = [KINWebBrowserViewController navigationControllerWithWebBrowser];
        KINWebBrowserViewController *webBrowser = [webBrowserNavigationController rootWebBrowser];
        [webBrowser setDelegate:nil];
        webBrowser.showsURLInNavigationBar = YES;
        webBrowser.tintColor = controller.navigationController.navigationBar.tintColor;
        webBrowser.barTintColor = controller.navigationController.navigationBar.barTintColor;
        webBrowser.showsPageTitleInNavigationBar = NO;
        webBrowser.showsURLInNavigationBar = NO;
        [controller presentViewController:webBrowserNavigationController animated:YES completion:nil];
        [webBrowser loadURLString:url];
        return webBrowser;
    }
}

-(KINWebBrowserViewController *)openURLWithinApplication:(NSString *)url
{
    return [[self class] openURLWithinApplication:url];
}


@end
