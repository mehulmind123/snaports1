//
//  BeaconManager.m
//  Master
//
//  Created by mac-0001 on 23/01/15.
//  Copyright (c) 2015 mac-0001. All rights reserved.
//

#import "BeaconManager.h"

#import "Master.h"


static NSString *const ACTIVEBEACONS = @"arrActiveBeacons";



@interface BeaconManager () <CLLocationManagerDelegate>

@property NSMutableArray *arrBeaconEntryHandler;
@property NSMutableArray *arrBeaconRangingHandler;
@property NSMutableArray *arrBeaconExitHandler;

@property NSMutableArray *arrBeaconsStartedRanging;


@end

@implementation BeaconManager

@synthesize arrBeaconEntryHandler,arrBeaconExitHandler,arrBeaconRangingHandler;
@synthesize arrBeaconsStartedRanging;
@synthesize locationManager;

// To-Do Check logic for firing and recieving UILocalNotifications
// To-Do Check Testing when Outside state fetched, didExitRegion and didRangeBeacons with beacons count 0 is fired or not
// To-Do Add code to check bluetooth is enabled or not

+ (instancetype)sharedInstance
{
    static BeaconManager *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[BeaconManager alloc] init];
    });
    
    return _sharedInstance;
}

- (void)startRangingWithProximityUUID:(NSUUID *)proximityUUID identifier:(NSString *)identifier
{
    if (!arrBeaconEntryHandler && !arrBeaconExitHandler && !arrBeaconRangingHandler)
    {
        NSLog(@"Implement lookingForBeacons:ranging:exit: method first. Otherwise beacons you are allready in will not fired entry notification.");
    }
    
    CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:proximityUUID identifier:identifier];
    
    [self startMonitoring:region];

    [arrBeaconsStartedRanging addObject:region];
}

- (void)startRangingProximityUUID:(NSUUID *)proximityUUID major:(CLBeaconMajorValue)major identifier:(NSString *)identifier
{
    if (!arrBeaconEntryHandler && !arrBeaconExitHandler && !arrBeaconRangingHandler)
    {
        NSLog(@"Implement lookingForBeacons:ranging:exit: method first. Otherwise beacons you are allready in will not fired entry notification.");
    }

    CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:proximityUUID major:major identifier:identifier];
    
    [self startMonitoring:region];

    [arrBeaconsStartedRanging addObject:region];
}

- (void)startRangingProximityUUID:(NSUUID *)proximityUUID major:(CLBeaconMajorValue)major minor:(CLBeaconMinorValue)minor identifier:(NSString *)identifier
{
    if (!arrBeaconEntryHandler && !arrBeaconExitHandler && !arrBeaconRangingHandler)
    {
        NSLog(@"Implement lookingForBeacons:ranging:exit: method first. Otherwise beacons you are allready in will not fired entry notification.");
    }

    CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:proximityUUID major:major minor:minor identifier:identifier];

    [self startMonitoring:region];

    [arrBeaconsStartedRanging addObject:region];
}

-(void)startMonitoring:(CLBeaconRegion *)region
{
    if (![[self.locationManager monitoredRegions] containsObject:region])
    {
        region.notifyEntryStateOnDisplay = YES;
        [self.locationManager startMonitoringForRegion:region];
        [self.locationManager startRangingBeaconsInRegion:region];
    }
}

-(void)resetRangingBeacons
{
    [arrBeaconsStartedRanging removeAllObjects];
}

-(void)removePreviousBeacons
{
    NSArray *removedBeacons = [[self.locationManager.monitoredRegions allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT SELF IN %@",arrBeaconsStartedRanging]];

    
    for (CLBeaconRegion *region in removedBeacons)
    {
        if ([region isKindOfClass:[CLBeaconRegion class]])
        {
            [self.locationManager stopMonitoringForRegion:region];
            [self.locationManager stopRangingBeaconsInRegion:region];
            [self removeBeacon:region];
        }
    }
}

-(void)lookingForBeacons:(EnteredIntoBeaconRegion)enter ranging:(BeaconRangeUpdated)ranging exit:(ExitedFromBeaconRegion)exit
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        arrBeaconEntryHandler = [[NSMutableArray alloc] init];
        arrBeaconRangingHandler = [[NSMutableArray alloc] init];
        arrBeaconExitHandler = [[NSMutableArray alloc] init];
        arrBeaconsStartedRanging = [[NSMutableArray alloc] init];
    
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate =self;
        
        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        {
            if ([CBundleDictionary objectForKey:NSLocationAlwaysUsageDescription])
                [self.locationManager performSelector:@selector(requestAlwaysAuthorization)];
            else if ([CBundleDictionary objectForKey:NSLocationWhenInUseUsageDescription])
                [self.locationManager performSelector:@selector(requestWhenInUseAuthorization)];
            else
                NSAssert(nil, @"Please add Location Usage Key (NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription) on Info.plist");
        }

        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
        {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil]];
        }

    });

    
    if (![arrBeaconEntryHandler containsObject:enter] && enter)
        [arrBeaconEntryHandler addObject:enter];

    if (![arrBeaconRangingHandler containsObject:ranging] && ranging)
        [arrBeaconRangingHandler addObject:ranging];

    if (![arrBeaconExitHandler containsObject:exit] && exit)
        [arrBeaconExitHandler addObject:exit];
}


#pragma mark - LocationManagerDelegate

-(BOOL)addBeacon:(CLBeaconRegion *)region
{
    NSString *uuid = region.proximityUUID.UUIDString;
    NSString *major = [NSString stringWithFormat:@"%@",region.major];
    NSString *minor = [NSString stringWithFormat:@"%@",region.minor];

    NSMutableArray *arrUserDefaults = [[NSMutableArray alloc] init];
    [arrUserDefaults addObjectsFromArray:[CUserDefaults objectForKey:ACTIVEBEACONS]];
    
    NSArray *arrTemp = [arrUserDefaults filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"uuid == %@ && major == %@ && minor == %@",uuid,major,minor]];
    
    if (arrTemp && arrTemp.count>0)
        return NO;
    
    [arrUserDefaults addObject:@{@"uuid":uuid,@"major":major,@"minor":minor}];
    [CUserDefaults setObject:arrUserDefaults forKey:ACTIVEBEACONS];
    [CUserDefaults synchronize];
    
    return YES;
}

- (BOOL)removeBeacon:(CLBeaconRegion *)region
{
    NSString *uuid = region.proximityUUID.UUIDString;
    NSString *major = [NSString stringWithFormat:@"%@",region.major];
    NSString *minor = [NSString stringWithFormat:@"%@",region.minor];

    NSMutableArray *arrUserDefaults = [[NSMutableArray alloc] init];
    [arrUserDefaults addObjectsFromArray:[CUserDefaults objectForKey:ACTIVEBEACONS]];
    
    NSMutableArray *arrPredicates = [[NSMutableArray alloc] init];
    [arrPredicates addObject:[NSPredicate predicateWithFormat:@"uuid == %@",uuid]];
    [arrPredicates addObject:[NSPredicate predicateWithFormat:@"major == %@",major]];
    [arrPredicates addObject:[NSPredicate predicateWithFormat:@"minor == %@",minor]];
    
    NSArray *arrTemp = [arrUserDefaults filteredArrayUsingPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:arrPredicates]];
    
    if (!arrTemp || arrTemp.count<=0)
        return NO;
    
    [arrUserDefaults removeObjectsInArray:arrTemp];
    [CUserDefaults setObject:arrUserDefaults forKey:ACTIVEBEACONS];
    [CUserDefaults synchronize];
    
    return YES;
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLBeaconRegion *)region
{
    if (![region isKindOfClass:[CLBeaconRegion class]])
        return;
    if(state == CLRegionStateInside)
    {
        NSLog(@"CLRegionStateInside = %@",region.identifier);
        
        if ([self addBeacon:region])
        {
            if (arrBeaconRangingHandler.count>0)
                [self.locationManager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
            
            for (EnteredIntoBeaconRegion block in arrBeaconEntryHandler)
            {
                block((CLBeaconRegion *)region);
            }
        }
    }
    else if(state == CLRegionStateOutside)
    {
        NSLog(@"CLRegionStateOutside = %@",region.identifier);

        if ([self removeBeacon:region])
        {
            [self.locationManager stopRangingBeaconsInRegion:(CLBeaconRegion *)region];
            
            
            for (ExitedFromBeaconRegion block in arrBeaconExitHandler)
            {
                block((CLBeaconRegion *)region);
            }
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    if (beacons.count==0)
    {
        [self.locationManager requestStateForRegion:region];
        // To-Do Check this method for 2-3 seconds, if still not ranging than fire exit event
    }
    else
    {
        for (CLBeacon *beacon in beacons)
        {
            for (BeaconRangeUpdated block in arrBeaconRangingHandler)
            {
                block((CLBeaconRegion *)region,beacon);
            }
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    if (![region isKindOfClass:[CLBeaconRegion class]])
        return;

    [self.locationManager requestStateForRegion:region];
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    if (![region isKindOfClass:[CLBeaconRegion class]])
        return;

    [self.locationManager requestStateForRegion:region];
}

- (void)locationManager:(CLLocationManager *)manager rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error
{
    NSLog(@"locationManager:rangingBeaconsDidFailForRegion:withError: %@",error);
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
    NSLog(@"locationManager:monitoringDidFailForRegion:withError: %@",error);
}

@end
