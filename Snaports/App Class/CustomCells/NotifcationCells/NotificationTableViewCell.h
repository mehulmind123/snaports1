//
//  NotificationTableViewCell.h
//  Snaports
//
//  Created by mac-00018 on 28/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblNotificationMessage;
@property (weak, nonatomic) IBOutlet UIView *viwNotificationStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblNotificationTime;

@end
