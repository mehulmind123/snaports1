//
//  MKMapView+MapExtension.m
//  Only Appointment
//
//  Created by mac-00015 on 8/7/15.
//  Copyright (c) 2015 mac-0009. All rights reserved.
//

#import "MKMapView+MapExtension.h"

@implementation MKMapView (MapExtension)

- (void)setZoomViewByMile:(MKMapView*)mapView mile:(double)mile location:(CLLocationCoordinate2D)location
{
    double scalingFactor = ABS( (cos(2 * M_PI * location.latitude / 360.0)));
    MKCoordinateSpan span;
    span.latitudeDelta = mile/69.0;
    span.longitudeDelta = mile/(scalingFactor * 69.0);
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location, mile, mile);
    region.span = span;
    region.center = location;
    
    if ((region.center.latitude >= -90) && (region.center.latitude <= 90) && (region.center.longitude >= -180) && (region.center.longitude <= 180))
        [mapView setRegion:[mapView regionThatFits:region]];
    else
        NSLog(@"Invalid Region . . .");
}

- (MKCoordinateRegion)regionForAnnotations:(NSArray *)annotations mapView:(MKMapView*)mapView fromLat:(double)fromLat fromLong:(double)fromLong
{
    MKCoordinateRegion region;
    if ([annotations count] == 0)
        region = MKCoordinateRegionMakeWithDistance(mapView.userLocation.coordinate, 1000, 1000);
    else if ([annotations count] == 1)
    {
        id <MKAnnotation> annotation = [annotations lastObject];
        region = MKCoordinateRegionMakeWithDistance(annotation.coordinate, 1000, 1000);
    }
    else
    {
        CLLocationCoordinate2D topLeftCoord;
        topLeftCoord.latitude = -90;
        topLeftCoord.longitude = 180;
        
        CLLocationCoordinate2D bottomRightCoord;
        bottomRightCoord.latitude = 90;
        bottomRightCoord.longitude = -180;
        
        for (id <MKAnnotation> annotation in annotations)
        {
            topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
            topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
            bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
            bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        }
        
        const double extraSpace = 1.1;
        region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) / 2.0;
        region.center.longitude = topLeftCoord.longitude - (topLeftCoord.longitude - bottomRightCoord.longitude) / 2.0;
        region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * extraSpace;
        region.span.longitudeDelta = fabs(topLeftCoord.longitude - bottomRightCoord.longitude) * extraSpace;
    }
    
    if ((region.center.latitude >= -90) && (region.center.latitude <= 90) && (region.center.longitude >= -180) && (region.center.longitude <= 180))
        return [mapView regionThatFits:region];
    
    return MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(fromLat, fromLong), 400, 400);
}

@end
