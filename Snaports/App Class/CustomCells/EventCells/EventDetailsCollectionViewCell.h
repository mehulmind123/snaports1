//
//  EventDetailsCollectionViewCell.h
//  Snaports
//
//  Created by mac-00018 on 27/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDetailsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnShowDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *viwTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgShowMore;
@property (weak, nonatomic) IBOutlet UIView *viwContent;
@property (weak, nonatomic) IBOutlet UIView *viwForRotation;

@end
