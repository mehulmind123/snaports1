//
//  EventsLiveViewController.m
//  Snaports
//
//  Created by mac-00018 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "EventsLiveViewController.h"
#import "EventsLiveTableViewCell.h"
#import "EventsCollectionViewCell.h"
#import "EventDetailsViewController.h"
#import "PredicationViewController.h"
#import "CameraViewController.h"

@interface EventsLiveViewController ()
{
    NSDictionary *dictData;
}

@end

@implementation EventsLiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//   [self.tblEvents registerNib:[UINib nibWithNibName:@"EventsLiveTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];

    
    //Right swipe
    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(navigateToCameraScreen)];
    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizerRight];

    //Left swipe
    UISwipeGestureRecognizer *gestureRecognizerLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(navigateToPredictionScreen)];
    [gestureRecognizerLeft setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:gestureRecognizerLeft];
    
    

    [self setNavigationBarbuttons];
    [self setDataforChannelsAndEvents];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Set Navigaiton bar buttons
- (void)setNavigationBarbuttons
{
    
    [self setTitle:CEventsLive];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Home_prediction"]
                                                   style:UIBarButtonItemStylePlain
                                                  target:self
                                                  action:@selector(navigateToPredictionScreen)];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Home_camera"]
                                                  style:UIBarButtonItemStylePlain
                                                 target:self
                                                action:@selector(navigateToCameraScreen)];
    
    self.navigationItem.leftBarButtonItem = leftButton;
    self.navigationItem.rightBarButtonItem = rightButton;

}

- (void)navigateToCameraScreen
{
    [appDelegate.swipeBetweenVC setCurrentIndex:0 animated:true];
//    appDelegate.swipeBetweenVC.currentIndex = 0;
////    CameraViewController *vcCamera = [[CameraViewController alloc] initWithNibName:@"CameraViewController" bundle:nil];
//
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.30;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromLeft;
//    self.view.window.backgroundColor = [UIColor whiteColor];
//    self.view.window.tintColor = [UIColor whiteColor];
//
//    [self.navigationController.view.layer addAnimation:transition forKey:nil];
//    [self.navigationController popViewControllerAnimated:true];
//    [self.navigationController pushViewController:vcCamera animated:NO];

}

- (void)navigateToPredictionScreen
{
     [appDelegate.swipeBetweenVC setCurrentIndex:2 animated:true];
    
//    PredicationViewController *vcPrediction = [[PredicationViewController alloc] initWithNibName:@"PredicationViewController" bundle:nil];
//    [self.navigationController pushViewController:vcPrediction animated:YES];
}


#pragma mark - TableView Delegate and Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    return [[dictData valueForKey:@"RECENT LIVE"] count];

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 180.0;

    }
    else
    {
        return 154.0;
        
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"section is %ld", (long)indexPath.section);
    static NSString *strCellId = @"Cell";

    EventsLiveTableViewCell *cell = (EventsLiveTableViewCell*)[tableView dequeueReusableCellWithIdentifier:strCellId];

    if (cell == nil) {
        
        NSArray *nib =[[NSBundle mainBundle] loadNibNamed:@"EventsLiveTableViewCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.section == 0) {
        
        cell.viwChannels.hidden = NO;
        cell.viwRecentLive.hidden = YES;
        
        [cell.collectionviwChannels registerNib:[UINib nibWithNibName:@"EventsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:CollectionViewCellIdentifier];
        
        
        //Set flow layout for collectionview
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(100.0, cell.collectionviwChannels.frame.size.height);
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [flowLayout setSectionInset:UIEdgeInsetsMake(0, 5, 0, 0)];

        cell.collectionviwChannels.showsHorizontalScrollIndicator = NO;

        [flowLayout setMinimumInteritemSpacing:5.0];
        [flowLayout setMinimumLineSpacing:5.0];
        [cell.collectionviwChannels setCollectionViewLayout:flowLayout];

        cell.collectionviwChannels.delegate = self;
        cell.collectionviwChannels.dataSource = self;
        [cell.collectionviwChannels reloadData];

    }
    else
    {
        cell.viwChannels.hidden = YES;
        cell.viwRecentLive.hidden = NO;
        [self setRoundedCorners:cell.viwLIVE];

    }
    


    /*
     static NSString *strCellId = @"Cell";
     
     EventsLiveTableViewCell *cell = [[EventsLiveTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strCellId];
     
     if (cell == nil) {
     NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"EventsLiveTableViewCell" owner:self options:nil];
     cell = [nib objectAtIndex:0];
     }
     
     [cell.collectionviwChannels registerNib:[UINib nibWithNibName:@"EventsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:CollectionViewCellIdentifier];
     
     UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
     //        layout.sectionInset = UIEdgeInsetsMake(10, 10, 9, 10);
     //        layout.itemSize = CGSizeMake(44, 44);
     layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
     
     UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
     //        [flowLayout setItemSize:CGSizeMake((self.collectionviwChannels.frame.size.height-2)/4, (self.collectionviwChannels.frame.size.height - 2)/4)];
     [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
     [cell.collectionviwChannels setCollectionViewLayout:flowLayout];
     
     //        [self.collectionviwChannels registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:CollectionViewCellIdentifier];
     cell.collectionviwChannels.backgroundColor = [UIColor purpleColor];
     cell.collectionviwChannels.showsHorizontalScrollIndicator = NO;
     
         cell.collectionviwChannels.delegate = self;
         cell.collectionviwChannels.dataSource = self;
         [cell.collectionviwChannels reloadData];

     */

    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return nil;
    }

    CGFloat tableHeight = [self tableView:tableView heightForHeaderInSection:section];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CViewWidth(tableView), tableHeight)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, CViewHeight(view)/2.0 -9.0, CViewWidth(tableView) -10.0, 18)];
    label.font = CFontSolidoMedium(15);
//    [label setFont:[UIFont boldSystemFontOfSize:12]];
    [label setText:CLocalize(@"RECENT LIVE")];
    label.textColor = CRGB(95, 95, 95);
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    [view setBackgroundColor:CRGB(249, 249, 249)];
    return view;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section!=0) {

        NSDictionary *dictAtIndex = [[dictData valueForKey:@"RECENT LIVE"] objectAtIndex:indexPath.row];
        if (![[dictAtIndex valueForKey:@"video"] isEqualToString:@""]) {
            //Play default video
            NSBundle *bundle = [NSBundle mainBundle];
            NSString *moviePath = [bundle pathForResource:@"temp" ofType:@"mp4"];
            NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
            
            // create an AVPlayer
            AVPlayer *player = [AVPlayer playerWithURL:movieURL];
            
            AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
            [self presentViewController:controller animated:YES completion:nil];
            controller.player = player;
            [player play];

        }
        else
        {
            EventDetailsViewController *vcEventDetails = [[EventDetailsViewController alloc] initWithNibName:@"EventDetailsViewController" bundle:nil];
            vcEventDetails.arrData = [NSArray arrayWithObject:dictAtIndex];
            [self.navigationController pushViewController:vcEventDetails animated:YES];

        }

    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0.0;
    }

    return 37.0;
}





#pragma mark - Collection View Delegate and DataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[dictData valueForKey:@"CHANNELS"] count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    EventsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier forIndexPath:indexPath];
    
    [self setRoundedCorners:cell.viwDesciption];
    
    NSDictionary *dictAtIndex = [[dictData valueForKey:@"CHANNELS"] objectAtIndex:indexPath.row];
    cell.lblDescriptions.text = [dictAtIndex valueForKey:@"Description"];
    cell.layer.cornerRadius = 4.0; //CViewWidth(cell)/7.0;
    cell.layer.masksToBounds = YES;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    EventDetailsViewController *vcEventDetails = [[EventDetailsViewController alloc] initWithNibName:@"EventDetailsViewController" bundle:nil];
    vcEventDetails.arrData = @[
                               @{
                                   @"imgPath":@"",
                                   @"ImgTitle" : @"BALLON D'OR SHORTLIST:FIRST10NAMES NSNNNSSDD",
                                   @"video":@"",
                                   @"text":@"fdfgdfgdfgdfgdfgdfgdfgdfgdfg"
                                   
                                   },
                               @{
                                   @"imgPath":@"",
                                   @"ImgTitle" : @"MOU SLAMS COUNTE:IT'S HUMILIATING FOR US",
                                   @"video":@"gfdgdfgdfgdfgdfgdfgdfgdfgdgdfgdfgdf",
                                   @"text":@""
                                   
                                   },
                               @{
                                   @"imgPath":@"",
                                   @"ImgTitle" : @"",
                                   @"video":@"",
                                   @"text":@""
                                   
                                   }
                               ];

    [self.navigationController pushViewController:vcEventDetails animated:YES];
}

#pragma mark - Helper Fumctions
- (void)setRoundedCorners:(UIView *)viw
{
    viw.layer.cornerRadius = CViewHeight(viw)/2;
    viw.layer.masksToBounds = YES;
}

- (void)setDataforChannelsAndEvents
{
    dictData = @{@"CHANNELS" : @[
                             @{
                                 @"imgName" : @"",
                                 @"Description" : @"BALLON D'OR SHORTLIST:FIRST10NAMES"
                                 },
                             @{
                                 @"imgName" : @"",
                                 @"Description" : @"MOU SLAMS CONTE:IT'S HUMILIATING FOR US"
                                 },
                             
                             @{
                                 @"imgName" : @"",
                                 @"Description" : @"ZLATON IS PLAYING LIKE AN OLD MAN"
                                 },
                             @{
                                 @"imgName" : @"",
                                 @"Description" : @"ZLATON IS PLAYING LIKE AN OLD MAN"
                                 },

                             ],
            @"RECENT LIVE" :
                             @[
                             @{
                                 @"TeamA" : @"REAL MADRID",
                                 @"TeamB" : @"",
                                 @"TeamAScrore" : @"0",
                                 @"TeamBScrore" : @"0",
                                 @"MatchStatus" :@"FULL TIME",
                                 @"MatchDescription" : @"LA LIGA",
                                 @"video":@"gfdgdfgdfgdfgdfgdfgdfgdfgdgdfgdfgdf",
                                 @"imgPath":@"",
                                 @"text":@"",
                                 @"ImgTitle":@"",

                                 },
                             @{
                                 @"TeamA" : @"REAL MADRID",
                                 @"TeamB" : @"",
                                 @"TeamAScrore" : @"0",
                                 @"TeamBScrore" : @"0",
                                 @"MatchStatus" :@"FULL TIME",
                                 @"MatchDescription" : @"LA LIGA",
                                 @"imgPath":@"",
                                 @"video":@"",
                                 @"text":@"",
                                 @"ImgTitle":@"",



                                 },
                             
                             @{
                                 @"TeamA" : @"REAL MADRID",
                                 @"TeamB" : @"",
                                 @"TeamAScrore" : @"0",
                                 @"TeamBScrore" : @"0",
                                 @"MatchStatus" :@"FULL TIME",
                                 @"MatchDescription" : @"LA LIGA",
                                 @"video":@"gfdgdfgdfgdfgdfgdfgdfgdfgdgdfgdfgdf",
                                 @"imgPath":@"",
                                 @"video":@"",
                                 @"text":@"",
                                 @"ImgTitle":@"",


                                 },
                             
                             @{
                                 @"TeamA" : @"REAL MADRID",
                                 @"TeamB" : @"",
                                 @"TeamAScrore" : @"0",
                                 @"TeamBScrore" : @"0",
                                 @"MatchStatus" :@"FULL TIME",
                                 @"MatchDescription" : @"LA LIGA",
                                 @"ImgTitle" : @"MOU SLAMS COUNTE:IT'S HUMILIATING FOR US",
                                 @"video":@"",
                                 @"text":@"",
                                 @"ImgTitle":@"",

                                 },
                             
                             ]
                         
                };
}
@end
