//
//  NSArray+MIExtension.h
//  MI API Example
//
//  Created by mac-0001 on 8/20/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (MIExtension)

-(NSArray *)sortByTimestampAscending;
-(NSArray *)sortByTimestampDescending;

-(NSArray *)distinctValues;

@end
