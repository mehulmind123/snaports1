//
//  PredicationViewController.m
//  Snaports
//
//  Created by mac-0004 on 27/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "PredicationViewController.h"
#import "EventPredicationTableViewCell.h"
#import "SeasonListViewController.h"
@interface PredicationViewController ()
{
    NSArray *arrayDataSection,*arryDataRow;
    NSUInteger selectedSection;
    NSInteger SwippedRow;
    
}
@end

@implementation PredicationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedSection = -1;
    SwippedRow = -1;
    arrayDataSection = @[@"LaLiga Final",@"India Vs Pakistan",@"Euro Cup",@"Saudi Professional League",@"Eufa Cup"];
    arryDataRow = @[@{@"Type":@"Live",
                      @"Team1":@"Spain",
                      @"Team2":@"Portugal",
                      },
                    @{@"Type":@"FullTime",
                      @"Team1":@"Argentina",
                      @"Team2":@"Germany",

                      },
                    @{@"Type":@"Time",
                      @"Team1":@"Poland",
                      @"Team2":@"Chile",

                      }
                    ];
    
    
    [tblPredicatoin registerNib:[UINib nibWithNibName:@"EventPredicationTableViewCell" bundle:nil] forCellReuseIdentifier:@"EventPredicationTableViewCell"];
    
    [self setNavigationBarbuttons];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setNavigationBarbuttons
{
    
    [self setTitle:CFootballMatches];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Prediction_menu"]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(seasonList)];

    self.navigationItem.rightBarButtonItem = rightButton;
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backArraow"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(backButtonClk)];
    
    self.navigationItem.leftBarButtonItem = leftButton;
    
}



#pragma mark - Action event
-(void)seasonList
{
    SeasonListViewController *objSeason = [[SeasonListViewController alloc] initWithNibName:@"SeasonListViewController" bundle:nil];
    [self.navigationController pushViewController:objSeason animated:true];
}
-(void)backButtonClk
{
    [appDelegate.swipeBetweenVC setCurrentIndex:1 animated:true];
}

#pragma mark - TableView Delegate and DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(selectedSection == indexPath.section )
        return 65;
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 0.001;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return arrayDataSection.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arryDataRow.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier =@"EventPredicationTableViewCell";
    EventPredicationTableViewCell *cell = (EventPredicationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
        cell = [[EventPredicationTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    
    //------------- Swip Cell -----------------
    cell.layoutMainViewTrailing.constant = 0;
    [UIView animateWithDuration:0.2
                     animations:^{
                         [cell.contentView layoutIfNeeded]; // Called on parent view
                     }];
    [cell.btnSwipe touchUpInsideClicked:^{
    
        if (indexPath.row == SwippedRow) {
            // to close
            SwippedRow = -1;
            cell.layoutMainViewTrailing.constant = 0;
        }
        else
        {
            //TO OPEN
            if (SwippedRow != -1) {
                [tblPredicatoin reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:SwippedRow inSection:indexPath.section]] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            SwippedRow = indexPath.row;
            cell.layoutMainViewTrailing.constant = cell.viewSideButtons.frame.size.width;
        }
        
        
        [UIView animateWithDuration:0.2
                         animations:^{
                             [cell.contentView layoutIfNeeded]; // Called on parent view
                         }];
    }];
    
    UISwipeGestureRecognizer *right = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
    right.direction = UISwipeGestureRecognizerDirectionRight;
    right.delegate = self;
    [cell.contentView addGestureRecognizer:right];

    UISwipeGestureRecognizer* left = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    left.delegate = self;
    left.direction = UISwipeGestureRecognizerDirectionLeft;
    [cell.contentView addGestureRecognizer:left];
    
    
    NSMutableDictionary *Item =  [arryDataRow[indexPath.row] mutableCopy];
    cell.viewTime.hidden = true;
    //----------------- Match Type -----------------
    if ([[Item valueForKey:@"Type"] isEqualToString:@"Live"]) {
        cell.viewLnFT.backgroundColor = CRGB(0, 178, 66);
        
    }
    else if ([[Item valueForKey:@"Type"] isEqualToString:@"FullTime"])
    {
         cell.viewLnFT.backgroundColor = CRGB(255, 58, 67);
    }
    else
    {
        cell.viewTime.hidden = false;
    }
    
    //----------------- Assign Data -----------------
    
    cell.lblTeam1.text = [Item valueForKey:@"Team1"];
    cell.lblTeam2.text = [Item valueForKey:@"Team2"];
    
    [cell.btnTeam1 setTitle:[Item valueForKey:@"Team1"] forState:UIControlStateNormal];
    [cell.btnTeam2 setTitle:[Item valueForKey:@"Team2"] forState:UIControlStateNormal];
    
    
    [cell.btnTeam1 touchUpInsideClicked:^{
        cell.btnTeam2.selected = false;
        cell.btnDraw.selected = false;
        cell.btnTeam1.selected = true;
        [self showPredictionMessage];
    }];
    [cell.btnTeam2 touchUpInsideClicked:^{
        cell.btnTeam2.selected = true;
        cell.btnDraw.selected = false;
        cell.btnTeam1.selected = false;
        [self showPredictionMessage];
    }];
    [cell.btnDraw touchUpInsideClicked:^{
        cell.btnTeam2.selected = false;
        cell.btnDraw.selected = true;
        cell.btnTeam1.selected = false;
        [self showPredictionMessage];
    }];

    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    return 43;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CScreenWidth, 43)];
    UIView *viewTopLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CScreenWidth, 1)];
    UIView *viewBottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, 42, CScreenWidth, 1)];
    UILabel *lblSection  = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, CScreenWidth-30, 43)];
    lblSection.textAlignment = NSTextAlignmentLeft;
    lblSection.text = arrayDataSection[section];
    lblSection.font = CFontSolidoMedium(17);
    lblSection.textColor =[UIColor blackColor];
    view.backgroundColor = CRGB(245, 245, 245);
    viewTopLine.backgroundColor = CRGB(231, 231, 231);
    viewBottomLine.backgroundColor = CRGB(231, 231, 231);
    [view addSubview:lblSection];
    [view addSubview:viewTopLine];
    [view addSubview:viewBottomLine];
    
    UIButton *btnExpandCollapse  = [[UIButton alloc] initWithFrame:CGRectMake(15, 0, CScreenWidth -30, 42)];

    [btnExpandCollapse setImage:[UIImage imageNamed:(appDelegate.isArabic ? @"Setting_Expands_Rotate": @"Setting_Expands")] forState:UIControlStateNormal];
    [btnExpandCollapse setImage:[UIImage imageNamed:@"EditProfile_Expand"] forState:UIControlStateSelected];
    [btnExpandCollapse setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    
    if (appDelegate.isArabic) {
        
        lblSection.textAlignment = NSTextAlignmentRight;
        [btnExpandCollapse setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];

    }

    [view addSubview:btnExpandCollapse];
    btnExpandCollapse.selected = (selectedSection == section);
    
    [btnExpandCollapse touchUpInsideClicked:^{
        
        if (selectedSection == section)
        {
            selectedSection = -1;
            NSIndexSet *sections2 = [[NSIndexSet alloc] initWithIndex:section];
            [tblPredicatoin reloadSections:sections2 withRowAnimation:UITableViewRowAnimationAutomatic];
            return;
        }
        if (selectedSection != -1) {
            NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:selectedSection];
            selectedSection = section;
            [tblPredicatoin reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        
        selectedSection = section;
        NSIndexSet *sections2 = [[NSIndexSet alloc] initWithIndex:selectedSection];
        [tblPredicatoin reloadSections:sections2 withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }];
    
    return view;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - Show/Hide Message
- (void)showPredictionMessage
{
        layoutTblTopSpacing.constant = lblPredictionMessage.frame.size.height;

        [UIView animateWithDuration:0.2
                         animations:^{
                             
                             lblPredictionMessage.hidden = NO;
                             [self.view layoutIfNeeded];
                         }
         ];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self hidePredictionMessage];
        });


}

- (void)hidePredictionMessage
{
    layoutTblTopSpacing.constant = 0;
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         lblPredictionMessage.hidden = NO;
                         [self.view layoutIfNeeded];
                     }
    ];
}
#pragma mark - Gesture

-(void)swipeRight:(UITapGestureRecognizer *)rec
{
    // to close
    
    CGPoint swipeLocation = [rec locationInView:tblPredicatoin];
    NSIndexPath *swipedIndexPath = [tblPredicatoin indexPathForRowAtPoint:swipeLocation];
    EventPredicationTableViewCell *cell = (EventPredicationTableViewCell *)[tblPredicatoin cellForRowAtIndexPath:swipedIndexPath];
    if (SwippedRow == swipedIndexPath.row) {
        [cell.btnSwipe sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
}
-(void)swipeLeft:(UISwipeGestureRecognizer *)rec
{
    // to open
    
    CGPoint swipeLocation = [rec locationInView:tblPredicatoin];
    NSIndexPath *swipedIndexPath = [tblPredicatoin indexPathForRowAtPoint:swipeLocation];
    
    if(swipedIndexPath.row == SwippedRow)
        return;
    
    EventPredicationTableViewCell *cell = (EventPredicationTableViewCell *)[tblPredicatoin cellForRowAtIndexPath:swipedIndexPath];
    
    [cell.btnSwipe sendActionsForControlEvents:UIControlEventTouchUpInside];

}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

@end
