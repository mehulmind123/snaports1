//
//  IntrouctionViewController.m
//  Snaports
//
//  Created by mac-00018 on 30/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "IntrouctionViewController.h"
#import "SelectLanguageViewController.h"

@interface IntrouctionViewController ()
{
    NSArray *arrInstruction;
    bool isNavigatedAlready;
}

@end

@implementation IntrouctionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.btnPrevious setHidden: YES];
    [self setInstructionData];
    
    //Register class for default collection view cell
    [self.collectionviwIntroduction registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"CellIdentifier"];
    
    //Set flow layout for collectionview
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(CMainScreen.bounds.size.width , CMainScreen.bounds.size.height);
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    
    [self.collectionviwIntroduction setCollectionViewLayout:flowLayout];

    
}

- (void)viewWillAppear:(BOOL)animated
{
    isNavigatedAlready = false;
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self setNeedsStatusBarAppearanceUpdate];


}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CollectionView Datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrInstruction.count;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellIdentifier" forIndexPath:indexPath];
   
    //Add image view for instruction screen
    UIImageView *imgInstruction = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width , cell.frame.size.height )];
    imgInstruction.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [arrInstruction objectAtIndex:indexPath.row]]];
    [cell.contentView addSubview:imgInstruction];

    cell.backgroundColor = [UIColor greenColor];
    return cell;

}



- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (sender.contentOffset.x + CScreenWidth > self.collectionviwIntroduction.contentSize.width + 70.0) {
        
        if (!isNavigatedAlready)
        {
        SelectLanguageViewController *vcSelectLanguage = [[SelectLanguageViewController alloc] initWithNibName:@"SelectLanguageViewController" bundle:nil];
        [self.navigationController pushViewController:vcSelectLanguage animated:YES];
        }
    }
    
    else
    {
//        [self ShowHideButton];
    }


}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self ShowHideButton];

}


- (void)ShowHideButton
{
    float currentPage = [self GetCurrentPage];
    [self.btnPrevious setHidden:NO];
    [self.btnNext setHidden:NO];
    
    if (currentPage == 0) {
        [self.btnPrevious setHidden:YES];
    }
    else if (currentPage == arrInstruction.count-1)
    {
        [self.btnNext setHidden:YES];
        
    }

}

- (float)GetCurrentPage
{
    CGFloat pageWidth = _collectionviwIntroduction.frame.size.width;
    float currentPage = _collectionviwIntroduction.contentOffset.x / pageWidth;
    
    if (0.0f != fmodf(currentPage, 1.0f))
    {
        currentPage = currentPage + 1;
    }
    else
    {
        currentPage = currentPage;
    }
    
    return currentPage;

}

#pragma mark - Set Data

- (void)setInstructionData
{
    arrInstruction = @[
                       @"instruction_1",
                       @"instruction_2",
                       @"instruction_3"
                       ];
    
}

#pragma mark - Button CLick Event

- (IBAction)btnPreviousClicked:(id)sender {
    
    CGFloat currentPage = [self GetCurrentPage];
    NSIndexPath *index = [NSIndexPath indexPathForRow:currentPage-1 inSection:0];
    
    [self.collectionviwIntroduction scrollToItemAtIndexPath:index atScrollPosition:UICollectionViewScrollPositionNone animated:YES];

}
- (IBAction)btnSkipClicked:(id)sender {
//    SelectLanguageViewController *vcSelectLanguage = [[SelectLanguageViewController alloc] initWithNibName:@"SelectLanguageViewController" bundle:nil];
//    [self.navigationController pushViewController:vcSelectLanguage animated:YES];
    
    [appDelegate setupRootViewControllerForWindow];

}
- (IBAction)btnNextClicked:(id)sender {
    
    CGFloat currentPage = [self GetCurrentPage];
    NSIndexPath *index = [NSIndexPath indexPathForRow:currentPage+1 inSection:0];

    [self.collectionviwIntroduction scrollToItemAtIndexPath:index atScrollPosition:UICollectionViewScrollPositionNone animated:YES];

}



/*
 //- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
 //{
 //
 //    currentPage = indexPath.row;
 //
 //}

 
 - (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
 {
 //    [self ShowHideButton];
 }
 
 - (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
 {
 //    [self ShowHideButton];
 }

 - (void)scrollViewDidScroll:(UIScrollView *)sender
 {
 CGFloat pageWidth = self.collectionviwIntroduction.frame.size.width;
 float currentPage = self.collectionviwIntroduction.contentOffset.x / pageWidth;
 
 }
 
 - (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
 {
 NSLog(@"decelerate %hhd", decelerate);
 }
 - (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
 {
 
 float contentOffsetWhenFullyScrolledRight = self.collectionviwIntroduction.frame.size.width * ([arrInstruction count] -1);
 
 if (scrollView.contentOffset.x == contentOffsetWhenFullyScrolledRight) {
 NSLog(@"Scroll from last item");
 NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:1 inSection:0];
 
 [self.collectionviwIntroduction scrollToItemAtIndexPath:newIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
 
 
 }
 
 //    NSLog(@"scrollViewDidEndDecelerating");
 //    CGFloat pageWidth = self.collectionviwIntroduction.frame.size.width;
 //    float currentPage = self.collectionviwIntroduction.contentOffset.x / pageWidth;
 ////    NSLog(@"Current Page  %f %f", currentPage, scrollView.contentOffset.x);
 //
 //    for (UICollectionViewCell *cell in [self.collectionviwIntroduction visibleCells]) {
 //        NSIndexPath *indexPath = [self.collectionviwIntroduction indexPathForCell:cell];
 //        NSUInteger lastIndex = [indexPath indexAtPosition:[indexPath length] - 1];
 //        NSLog(@"lastIndex  %d", lastIndex);
 //
 //    }
 //
 //
 //
 //    if (0.0f != fmodf(currentPage, 1.0f))
 //    {
 //        NSLog(@"Current Page 1 - %f",currentPage + 1);
 //    }
 //    else
 //    {
 //
 //        NSLog(@"Current Page 2 - %f",currentPage);
 //
 //    }
 
 
 
 }
 

 */
@end
