//
//  NSString+Extension.h
//  Master
//
//  Created by mac-0001 on 12/12/14.
//  Copyright (c) 2014 mac-0001. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UIApplication+Extension.h"

@interface NSString (Extension)

-(void)setApplicationLifecycleComplete;
-(void)setInstallationLifecycleComplete;



-(void)performBlockForApplicaitonLifecycle:(Block)block;
-(void)performBlockForInstallationLifecycle:(Block)block;

-(void)performBlockForApplicaitonLifecycleWithoutAutoComplete:(Block)block;
-(void)performBlockForInstallationLifecycleWithoutAutoComplete:(Block)block;

@end
