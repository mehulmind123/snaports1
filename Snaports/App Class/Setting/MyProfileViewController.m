//
//  MyProfileViewController.m
//  Snaports
//
//  Created by mac-0004 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "MyProfileViewController.h"
#import "PredicationTableViewCell.h"
#import "EventActivityTableViewCell.h"
#import "AllActivitiewViewController.h"
#import "EditViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>

@interface MyProfileViewController ()

@end

@implementation MyProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:CMyProfile];
    
    viewMyScore.layer.borderColor = CRGB(224, 224, 224).CGColor;
    viewMyScore.layer.borderWidth = 1.0;
    
    viwImage.layer.borderColor = CRGB(224, 224, 224).CGColor;
    viwImage.layer.borderWidth = 1.0;
    
    viwImage.layer.cornerRadius = (CScreenWidth * layOutImagePrpsnl.multiplier/2.0) + 8.0;
    imgProfile.layer.cornerRadius = CScreenWidth * layOutImagePrpsnl.multiplier/2.0;// imgProfile.frame.size.width/2;
    imgProfile.layer.masksToBounds  = YES;
    
    [tblActivities registerNib:[UINib nibWithNibName:@"PredicationTableViewCell" bundle:nil] forCellReuseIdentifier:@"PredicationTableViewCell"];
    [tblActivities registerNib:[UINib nibWithNibName:@"EventActivityTableViewCell" bundle:nil] forCellReuseIdentifier:@"EventActivityTableViewCell"];
    
    [tblActivities setEstimatedRowHeight:225];
    [tblActivities setRowHeight:UITableViewAutomaticDimension];
    
    ViewButton.layer.cornerRadius = ViewButton.frame.size.height/2;
    ViewButton.clipsToBounds = true;
    ViewButton.layer.borderWidth = 1;
    ViewButton.layer.borderColor = [UIColor blackColor].CGColor;
    
    appDelegate.configureFlipToMirror(viewMyScore,true);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - TableView Delegate and DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 0.001;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
        static NSString *simpleTableIdentifier =@"PredicationTableViewCell";
        PredicationTableViewCell *cell = (PredicationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
            cell = [[PredicationTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
        
        
        cell.ContWidthTeam1.constant = CViewWidth(cell.viewTeam1) * 0.1;
        cell.ContWidthDraw.constant = CViewWidth(cell.viewDraw) * 0.2;
        cell.ContWidthTeam2.constant = CViewWidth(cell.ViewTeam2) * 0.7;
        cell.imgProfile.image = [UIImage imageNamed:[NSString stringWithFormat:@"%ld.jpg",(long)indexPath.row]];
        [UIView animateWithDuration:0.5
                         animations:^{
                             [cell layoutIfNeeded]; // Called on parent view
                         }];
        
        [cell layoutIfNeeded];
        return cell;
    }
    else
    {
            static NSString *simpleTableIdentifier =@"EventActivityTableViewCell";
            EventActivityTableViewCell *cell = (EventActivityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            if (cell == nil)
                cell = [[EventActivityTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
        [cell.btnPlay touchUpInsideClicked:^{
            
            NSBundle *bundle = [NSBundle mainBundle];
            NSString *moviePath = [bundle pathForResource:@"temp" ofType:@"mp4"];
            NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
            
            AVPlayer *player = [AVPlayer playerWithURL:movieURL];
            
            AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
            [self presentViewController:controller animated:YES completion:nil];
            controller.player = player;
            [player play];
            
        }];
        cell.imgProfile.image = [UIImage imageNamed:[NSString stringWithFormat:@"%ld.jpg",(long)indexPath.row]];
        return cell;
       
    }
}

#pragma mark - Action Events 
-(IBAction)btnAllActivities:(id)sender
{
    AllActivitiewViewController *objAct = [[AllActivitiewViewController alloc] initWithNibName:@"AllActivitiewViewController" bundle:nil];
    [self.navigationController pushViewController:objAct animated:true];
}
-(IBAction)btnEditProfile:(id)sender
{
    EditViewController *objEdit = [[EditViewController alloc] initWithNibName:@"EditViewController" bundle:nil];
    [self.navigationController pushViewController:objEdit animated:true];
}
@end
