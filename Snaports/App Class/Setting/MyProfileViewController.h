//
//  MyProfileViewController.h
//  Snaports
//
//  Created by mac-0004 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SuperViewController.h"

@interface MyProfileViewController : SuperViewController
{
    IBOutlet UIButton *btnEdit_Profile;
    IBOutlet UILabel *lblName,*lblSubName;
    IBOutlet UILabel *lblPredication,*lblPoints,*lblAcurracy;
    IBOutlet UITableView *tblActivities;
    IBOutlet UIView *viewMyScore;
    IBOutlet UIImageView *imgProfile;
    IBOutlet UIView *viwImage;
    IBOutlet NSLayoutConstraint *layOutImagePrpsnl;
    IBOutlet UIView *ViewButton;
}
@end
