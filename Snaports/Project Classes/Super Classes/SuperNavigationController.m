//
//  SuperNavigationController.m
//  ChamRDV
//
//  Created by mac-0001 on 12/17/13.
//  Copyright (c) 2013 MIND INVENTORY. All rights reserved.
//

#import "SuperNavigationController.h"

@interface SuperNavigationController ()

@end

@implementation SuperNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, -5, 0); // or (0, 0, -10.0, 0)
    UIImage *alignedImage = [[UIImage imageNamed:@"ic_btn_back"] imageWithAlignmentRectInsets:insets];
    [[UINavigationBar appearance] setBackIndicatorImage:alignedImage];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"ic_btn_back"]];
    
//    [[UINavigationBar appearance] setBarTintColor:CNavBackgroundColor];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];

    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent]; //UIStatusBarStyleLightContent
    
    [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_navbarImage"] forBarMetrics:UIBarMetricsDefault];
    
    self.navigationBar.translucent = NO;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
