//
//  NSSet+MIExtension.m
//  MI API Example
//
//  Created by mac-0001 on 8/20/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import "NSSet+MIExtension.h"

@implementation NSSet (MIExtension)

-(NSArray *)sortByTimestampAscending
{
    return [self sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:YES]]];
}

-(NSArray *)sortByTimestampDescending
{
    return [self sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:NO]]];
}

@end
