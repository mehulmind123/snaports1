//
//  LocaleButton.m
//  Snaports
//
//  Created by mac-00018 on 05/01/17.
//  Copyright © 2017 mac-00018. All rights reserved.
//

#import "LocaleButton.h"

@implementation LocaleButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setTitle:CLocalize(self.titleLabel.text) forState:UIControlStateNormal];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
