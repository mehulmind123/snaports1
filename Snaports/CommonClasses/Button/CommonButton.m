//
//  CommonButton.m
//  Snaports
//
//  Created by mac-0004 on 23/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "CommonButton.h"

@implementation CommonButton
- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setTitle:CLocalize(self.titleLabel.text) forState:UIControlStateNormal];

}

- (void)drawRect:(CGRect)rect {
    
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    self.backgroundColor = [UIColor blackColor];
    self.layer.cornerRadius = 4;
    self.clipsToBounds = true;
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.titleLabel.font = CFontSolidoExtraBold(21);
    
}
@end
