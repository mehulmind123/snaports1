        //
//  main.m
//  Snaports
//
//  Created by mac-00018 on 22/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
