//
//  EditViewController.h
//  Snaports
//
//  Created by mac-0004 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SuperViewController.h"

@interface EditViewController : SuperViewController
{
    IBOutlet CommonTextField *txtFirstName,*txtLastName,*txtUserName,*txtEmailAddress,*txtBirthDay,*txtMobile,*txtContryCode,*txtCountry,*txtCity,*txtFavTeam;
    IBOutlet UIImageView *imgProfile;
    IBOutlet UIButton *btnProfilePic;
    IBOutlet CommonButton *btnSave;
}
@end
