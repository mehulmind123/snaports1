//
//  SeasonListViewController.h
//  Snaports
//
//  Created by mac-00018 on 29/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SuperViewController.h"

@interface SeasonListViewController : SuperViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblSeasonList;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutSearchTop;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBarSeasonDetails;

@end
