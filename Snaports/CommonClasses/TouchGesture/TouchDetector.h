//
//  TouchDetector.h
//  Snapports
//
//  Created by mac-0004 on 19/12/16.
//  Copyright © 2016 mindinventory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
@interface TouchDetector : UIGestureRecognizer

@property (nonatomic, strong) NSDictionary *dictDetails;
@end
