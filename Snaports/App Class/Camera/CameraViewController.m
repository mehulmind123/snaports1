//
//  CameraViewController.m
//  Snaports
//
//  Created by mac-0004 on 29/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "CameraViewController.h"
#import "PreviewViewController.h"
#import "SettingViewController.h"
#import "EventsLiveViewController.h"

@interface CameraViewController ()
{
    NSDate *RecoredstartTime;
    BOOL isTouchStart;
    BOOL isImageCaptauring;
    BOOL isKeepNavigationHidden;
    AVCaptureVideoDataOutput *output; // for image
    
}
@end

@implementation CameraViewController
@synthesize PreviewLayer,stillImageOutput;
#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Right swipe
    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(btnSettingclk:)];
    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionDown)];
    [self.view addGestureRecognizer:gestureRecognizerRight];

    
    //Left swipe
//    UISwipeGestureRecognizer *gestureRecognizerLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(navigateToEventsScreen)];
//    [gestureRecognizerLeft setDirection:(UISwipeGestureRecognizerDirectionLeft)];
//    [self.view addGestureRecognizer:gestureRecognizerLeft];

    
    //Show Alert
    self.configureShowAlert = ^(NSString *strTitle, NSString *strMsg, UIViewController *vc){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:strTitle message:strMsg preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:COK style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [vc presentViewController:alertController animated:YES completion:nil];
        
    };
    
    //Get
    self.configureGetCaptureDevice = ^(AVCaptureDevicePosition Position)
    {
        NSArray *Devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
        NSLog(@"Devices types %@", Devices);
        AVCaptureDevice *device;
        for (AVCaptureDevice *Device in Devices)
        {
            if ([Device position] == Position)
            {
                device = Device;
            }
        }
        return device;
        
    };
    
    [self getAuthorizationForCamera];
    [viewbtnRecord addGestureRecognizer:[[TouchDetector alloc] initWithTarget:self action:@selector(handleTouchDetected:)]];
    [viewbtnRecord setMaxValue:20];
    
    viewInside.layer.cornerRadius = CViewWidth(viewInside)/2;
    viewInside.clipsToBounds = true;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    [gradient setFrame:CGRectMake(0, 0, CScreenWidth, viewTopShadow.frame.size.height)];
    gradient.colors = [NSArray arrayWithObjects:(id)[CRGBA(0, 0, 0, 0.3) CGColor], (id)[[UIColor clearColor] CGColor],nil];
    [viewTopShadow.layer insertSublayer:gradient atIndex:0];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    viewbtnRecord.value = 0.0;
    isKeepNavigationHidden = false;
    self.navigationController.navigationBarHidden = true;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self setNeedsStatusBarAppearanceUpdate];

    WeAreRecording = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self manageTorch];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self turnTorchOn:false];
    self.navigationController.navigationBarHidden = isKeepNavigationHidden;
    
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if([timerProgress isValid])
    {
        [timerProgress invalidate];
        timerProgress = nil;
    }
    [self changeCaptureseetionOutput_IsImage:false];
}

#pragma mark - Show Event Screen
- (void)navigateToEventsScreen
{
//    EventsLiveViewController *vcEventsLive = [[EventsLiveViewController alloc] initWithNibName:@"EventsLiveViewController" bundle:nil];
//    
//    [self.navigationController pushViewController:vcEventsLive animated:YES];

}


#pragma mark - initializer

- (void)getAuthorizationForCamera
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        [self initializeCamera];
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted)
             {
                 NSLog(@"Granted access to %@", AVMediaTypeVideo);
                 [self initializeCamera];
             }
             else
             {
                 NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                 if (self.configureShowAlert) {
                     self.configureShowAlert(@"Error",@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access.", self);
                 }
             }
         }];
    }
    else if (authStatus == AVAuthorizationStatusRestricted)
    {
        // My own Helper class is used here to pop a dialog in one simple line.
        if (self.configureShowAlert) {
            self.configureShowAlert(@"Error",@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access.", self);
        }
    }
    else
    {
        if (self.configureShowAlert) {
            self.configureShowAlert(@"Error",@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access.", self);
        }
    }
}

-(void)initializeCamera
{
    
    CaptureSession = [[AVCaptureSession alloc] init];
    
    //ADD VIDEO INPUT
    AVCaptureDevice *VideoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (VideoDevice)
    {
        NSError *error;
        VideoInputDevice = [AVCaptureDeviceInput deviceInputWithDevice:VideoDevice error:&error];
        if (!error)
        {
            if ([CaptureSession canAddInput:VideoInputDevice])
                [CaptureSession addInput:VideoInputDevice];
            else
                NSLog(@"Couldn't add video input");
        }
        else
        {
            NSLog(@"Couldn't create video input");
        }
    }
    else
    {
        NSLog(@"Couldn't create video capture device");
    }
    
    //ADD AUDIO INPUT
    AVCaptureDevice *audioCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    NSError *error = nil;
    AVCaptureDeviceInput *audioInput = [AVCaptureDeviceInput deviceInputWithDevice:audioCaptureDevice error:&error];
    if (audioInput)
    {
        [CaptureSession addInput:audioInput];
    }
    
    
    
    //ADD VIDEO PREVIEW LAYER
//    [self setPreviewLayer:[[AVCaptureVideoPreviewLayer alloc] initWithSession:CaptureSession] ];
//    
    PreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:CaptureSession];
    
    [PreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [[PreviewLayer connection] setVideoOrientation:AVCaptureVideoOrientationPortrait];
    
    
    
    //ADD MOVIE FILE OUTPUT
    MovieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
    
    Float64 TotalSeconds = 60;			//Total seconds
    int32_t preferredTimeScale = 30;	//Frames per second
    CMTime maxDuration = CMTimeMakeWithSeconds(TotalSeconds, preferredTimeScale);	///<SET MAX DURATION
    MovieFileOutput.maxRecordedDuration = maxDuration;
    
    MovieFileOutput.minFreeDiskSpaceLimit = 1024 * 1024;						///<SET MIN FREE SPACE IN BYTES FOR RECORDING TO CONTINUE ON A VOLUME
    
    if ([CaptureSession canAddOutput:MovieFileOutput])
        [CaptureSession addOutput:MovieFileOutput];
    
    //SET THE CONNECTION PROPERTIES (output properties)
    [self CameraSetOutputProperties];               //(We call a method as it also has to be done after changing camera)
    
    [CaptureSession setSessionPreset:(IS_IPOD || IS_IPAD) ? AVCaptureSessionPresetMedium : AVCaptureSessionPresetHigh];
//    if ([CaptureSession canSetSessionPreset:AVCaptureSessionPreset640x480])		//Check size based configs are supported before setting them
//        [CaptureSession setSessionPreset:AVCaptureSessionPreset640x480];
    
    
    
    //Display it full screen under out view controller existing controls
    NSLog(@"Display the preview layer");
    CGRect layerRect = CScreenRect; // [[[self view] layer] bounds];
    [PreviewLayer setBounds:layerRect];
    [PreviewLayer setPosition:CGPointMake(CGRectGetMidX(layerRect),
                                          CGRectGetMidY(layerRect))];
    //[[[self view] layer] addSublayer:[[self CaptureManager] previewLayer]];
    //We use this instead so it goes on a layer behind our UI controls (avoids us having to manually bring each control to the front):
    dispatch_async(GCDMainThread, ^{
        
        UIView *CameraView = [[UIView alloc] init];
        [[self view] addSubview:CameraView];
        [self.view sendSubviewToBack:CameraView];
        
        [[CameraView layer] addSublayer:PreviewLayer];
        
    });
    
    //----- START THE CAPTURE SESSION RUNNING -----
    [CaptureSession startRunning];
}

#pragma mark -  camera Method
- (void) CameraSetOutputProperties
{
    //SET THE CONNECTION PROPERTIES (output properties)
    AVCaptureConnection *CaptureConnection = [MovieFileOutput connectionWithMediaType:AVMediaTypeVideo];
    
    //Set landscape (if required)
    if ([CaptureConnection isVideoOrientationSupported])
    {
        AVCaptureVideoOrientation orientation = AVCaptureVideoOrientationPortrait;		///<<<SET VIDEO ORIENTATION IF LANDSCAPE
        [CaptureConnection setVideoOrientation:orientation];
    }
    
    AVCaptureDevice *device = self.configureGetCaptureDevice(AVCaptureDevicePositionFront);
    CMTimeShow(device.activeVideoMinFrameDuration);
    CMTimeShow(device.activeVideoMaxFrameDuration);
    
  
    CMTimeShow(device.activeVideoMinFrameDuration);
    CMTimeShow(device.activeVideoMaxFrameDuration);
}


#pragma mark - ActionEvent

-(IBAction)btnFlashOnOff:(UIButton *)sender
{
    sender.selected = !sender.selected;
    [self manageTorch];
}
-(IBAction)btnSettingclk:(id)sender
{
    SettingViewController *objSettings = [[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    
    [self.navigationController.view.layer addAnimation:transition forKey:nil];

    [self.navigationController pushViewController:objSettings animated:false];
}

- (IBAction)CameraToggleButtonPressed:(UIButton *)sender
{
    if(CaptureSession)
    {
        [CaptureSession beginConfiguration];
        AVCaptureInput *currentCameraInput;
        
        
        
        for (AVCaptureInput *CameraInput in CaptureSession.inputs)
        {
            if (((AVCaptureDeviceInput*)CameraInput).device.position == AVCaptureDevicePositionBack || ((AVCaptureDeviceInput*)CameraInput).device.position == AVCaptureDevicePositionFront)
            {
                currentCameraInput = CameraInput;
            }
            
        }
        
        [CaptureSession removeInput:currentCameraInput];
        
        AVCaptureDevice *newCamera = nil;
        
        if(((AVCaptureDeviceInput*)currentCameraInput).device.position == AVCaptureDevicePositionBack)
        {
            newCamera = [self cameraWithPosition:AVCaptureDevicePositionFront];
            isFrontCamera = true;
            //            btnFleshLight.hidden = YES;
        }
        else
        {
            newCamera = [self cameraWithPosition:AVCaptureDevicePositionBack];
            isFrontCamera = false;
        }
        
        NSError *err = nil;
        
        AVCaptureDeviceInput *newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:newCamera error:&err];
        
        if(!newVideoInput || err)
        {
            NSLog(@"Error creating capture device input: %@", err.localizedDescription);
        }
        else
        {
            
            if ([CaptureSession canAddInput:newVideoInput])
                [CaptureSession addInput:newVideoInput];
        }
        
        [CaptureSession commitConfiguration];
        [self manageTorch];
    }
}

- (AVCaptureDevice *)cameraWithPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position)
            return device;
    }
    return nil;
}

#pragma mark - gesture
- (void)handleTouchDetected:(TouchDetector *)touchDetector {
    if (touchDetector.state == UIGestureRecognizerStateBegan) {
        isTouchStart = true;
         if(btnFlashOnOF.selected)
             [self turnTorchOn:true];
//        btnSwichCamera.hidden = true;
        [self VideoStart];
        RecoredstartTime = [NSDate date];
        self.view.userInteractionEnabled = false;
        
    }
    else if (touchDetector.state == UIGestureRecognizerStateEnded) {
        
        if (isTouchStart == true) {
            [self touchEndedForRecord];
        }
    }
}
-(void)touchEndedForRecord
{
//    btnSwichCamera.hidden = false;
    
    isTouchStart = false;
    isImageCaptauring = (fabs([RecoredstartTime timeIntervalSinceNow]) < 1);
    [self VideoStop];
    if (isImageCaptauring) {
        [self captureImage];
    }
    else
    {
        self.view.userInteractionEnabled = true;
    }
}
#pragma mark - Helper Function
#pragma mark - Tourch

-(void)manageTorch
{
    if(isFrontCamera == true || !btnFlashOnOF.selected)
    {
        [self turnTorchOn:false];
    }
    else
    {
//        [self turnTorchOn:true];
    }
}
- (void) turnTorchOn: (bool) on {
    
    // check if flashlight available
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil) {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasTorch] && [device hasFlash]){
            
            [device lockForConfiguration:nil];
            if (on) {
                if(btnFlashOnOF.selected)
                {
                [device setTorchMode:AVCaptureTorchModeOn];
                [device setFlashMode:AVCaptureFlashModeOn];
                }
                //torchIsOn = YES; //define as a variable/property if you need to know status
            } else {
                [device setTorchMode:AVCaptureTorchModeOff];
                [device setFlashMode:AVCaptureFlashModeOff];
                //torchIsOn = NO;
            }
            
            [device unlockForConfiguration];
        }
    } }

-(void)VideoStart
{
    if (!timerProgress) {
        timerProgress = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateRadius) userInfo:nil repeats:true];
    }
    if (!timerProgress.isValid) {
        
    }
    
    [self StartStopVideo];
}

-(void)VideoStop
{
    if([timerProgress isValid])
    {
        [timerProgress invalidate];
        timerProgress = nil;
    }
    
    [self StartStopVideo];
    
}

-(void)updateRadius
{
    viewbtnRecord.value = fabs([RecoredstartTime timeIntervalSinceNow]);
    
    if(viewbtnRecord.value < 0.5)
    {
        viewbtnRecord.value = 0;
    }
    if(fabs([RecoredstartTime timeIntervalSinceNow]) > 19.5)
    {
        [self touchEndedForRecord];
    }
}

#pragma mark - delegates


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    // This is the NSURL of the video object
    NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
    
    NSLog(@"VideoURL = %@", videoURL);
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - video
-(void)StartStopVideo
{
    if (!WeAreRecording)
    {
        
        WeAreRecording = YES;
        
        //Create temporary URL to record to
        NSString *outputPath = [[NSString alloc] initWithFormat:@"%@%@", NSTemporaryDirectory(), @"output.mov"];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:outputPath])
        {
            NSError *error;
            NSLog(@"File found -- and -- ");
            
            
            if ([[NSFileManager defaultManager] isDeletableFileAtPath:outputPath]) {
                BOOL success = [[NSFileManager defaultManager] removeItemAtPath:outputPath error:&error];
                if (!success) {
                    NSLog(@"Error removing file at path: %@", error.localizedDescription);
                }
                else                
                {
                    NSLog(@"Deleted Successfully");
                }
            }
//            
//            if ([fileManager removeItemAtPath:outputPath error:&error] == NO)
//            {
//                NSLog(@"%@ -- and -- %@",outputPath,error);
//                //Error - handle if requried
//            }
        }
        outputURL = [[NSURL alloc] initFileURLWithPath:outputPath];
        NSLog(@"%@",outputPath);
        [MovieFileOutput startRecordingToOutputFileURL:outputURL recordingDelegate:self];
        
    }
    else
    {
        WeAreRecording = NO;
        
        [MovieFileOutput stopRecording];
        if (!isImageCaptauring) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                
                PreviewViewController *objVdPly = [[PreviewViewController alloc] initWithNibName:@"PreviewViewController" bundle:nil];
                objVdPly.videoURL = outputURL;
                isKeepNavigationHidden = true;
                
                CATransition *transition = [CATransition animation];
                transition.duration = 0.30;
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromLeft;
                self.view.window.backgroundColor = [UIColor whiteColor];
                self.view.window.tintColor = [UIColor whiteColor];
                
                [self.navigationController pushViewController:objVdPly animated:false];
                
            });
//            [self presentViewController:objVdPly animated:true completion:nil];
        }
        else
        {
            
        }
    }
}

#pragma mark - Image

- (void)captureImage
{
    if (!stillImageOutput)
        stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    
    
    [self changeCaptureseetionOutput_IsImage:true];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self capturingImage];
        self.view.userInteractionEnabled = true;
    });
    self.view.userInteractionEnabled = false;
    
    
}
-(void)capturingImage
{
    self.view.userInteractionEnabled = true;
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in stillImageOutput.connections) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] ) {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) {
            NSLog(@"test demo ");
            break;
        }
    }
    
    NSLog(@"about to request a capture from: %@", stillImageOutput);
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
        
        NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
        UIImage *image = [[UIImage alloc] initWithData:imageData];
        
        PreviewViewController *objVdPly = [[PreviewViewController alloc] initWithNibName:@"PreviewViewController" bundle:nil];
        objVdPly.ClickedImage = image;
        isKeepNavigationHidden = true;
        [self.navigationController pushViewController:objVdPly animated:false];
    }];
}

-(void)changeCaptureseetionOutput_IsImage:(int)isForImage
{
    if (isForImage) {
        if(![CaptureSession.outputs containsObject:stillImageOutput])
        {
            [CaptureSession removeOutput:MovieFileOutput];
            [CaptureSession addOutput:stillImageOutput];
        }
    }
    else
    {
        if(![CaptureSession.outputs containsObject:MovieFileOutput])
        {
            [CaptureSession removeOutput:stillImageOutput];
            [CaptureSession addOutput:MovieFileOutput];
        }
    }
}
-(void)captureVideo
{
    
}
- (UIImage *) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer  {
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Get the number of bytes per row for the pixel buffer
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    
    // Get the number of bytes per row for the pixel buffer
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Create a bitmap graphics context with the sample buffer data
    CGContextRef context1 = CGBitmapContextCreate(baseAddress, width, height, 8,
                                                  bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context1);
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    // Free up the context and color space
    CGContextRelease(context1);
    CGColorSpaceRelease(colorSpace);
    
    // Create an image object from the Quartz image
    //I modified this line: [UIImage imageWithCGImage:quartzImage]; to the following to correct the orientation:
    UIImage *image =  [UIImage imageWithCGImage:quartzImage scale:1.0 orientation:UIImageOrientationRight];
    
    // Release the Quartz image
    CGImageRelease(quartzImage);
    
    return (image);
}



@end

