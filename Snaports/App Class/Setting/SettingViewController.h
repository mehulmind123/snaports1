//
//  SettingViewController.h
//  Snaports
//
//  Created by mac-0004 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SuperViewController.h"

@interface SettingViewController : SuperViewController<UIGestureRecognizerDelegate>
{
    IBOutlet UITableView *tblSetting;
    IBOutlet UIButton *btnLogout;
}
@end
