
#import "AppDelegate.h"

#define appDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#define CJsonStatus @"status"
#define CJsonMessage @"message"
#define CJsonTitle @"title"
#define CJsonData @"data"
#define CJsonResponse @"response"

#define CMessageWait @"Please Wait..."
#define CMessageSorry @"Sorry!"


#define CInternetNotAvailable @"Please connect to internet to proceed."
#define CUserId @"LoginUserId"

// Social Network API KEY
#define CGoogleApiKey @"AIzaSyAh4AngJiNeJviQtjAIyEOVazLj32UFM6M"
#define CGoogleClientId @"219945525881-uk4df785rb98libg439dsrbmlhu2b1jf.apps.googleusercontent.com"
#define CGoogleCallBackUrl @"com.mi.zoharii"

#define CFacebookApiId @"991727910891808 "
#define CFacebookCallBackUrl @"fb991727910891808"

#pragma mark - Colors
// --------------------------------------
#define CTextFieldBorderColor  CRGB(231, 231, 231)

#pragma mark - Device Constants
// --------------------------------------
#define CDeviceHeight [UIScreen mainScreen].bounds.size.height
#define Is_iPhone_4 CDeviceHeight == 480
#define Is_iPhone_5 CDeviceHeight == 568
#define Is_iPhone_6 CDeviceHeight == 667
#define Is_iPhone_6_PLUS CDeviceHeight == 736

#pragma mark - Fonts
//-----------------------------------------
#define CFontSolidoExtraBold(fontSize)                 [UIFont fontWithName:@"SolidoCondensed-ExtraBold" size:fontSize]
#define CFontSolidoMedium(fontSize)                    [UIFont fontWithName:@"SolidoCondensed-Medium" size:fontSize]
#define CFontSolidoBold(fontSize)                      [UIFont fontWithName:@"SolidoCondensed-Bold" size:fontSize]
#define CFontSolidoLight(fontSize)                      [UIFont fontWithName:@"SolidoCondensed-Light" size:fontSize]
#define CFontSolidoUltraLight(fontSize)                      [UIFont fontWithName:@"SolidoCondensed-UltraLight" size:fontSize]


#define CFontGilroyRegular(fontSize)                   [UIFont fontWithName:@"Gilroy-Regular" size:fontSize]
#define CFontGilroyMedium(fontSize)                    [UIFont fontWithName:@"Gilroy-Medium" size:fontSize]
#define CFontGilroyBold(fontSize)                      [UIFont fontWithName:@"Gilroy-Bold" size:fontSize]


/*======== LANGUAGE =========*/

#define CLanguageArebic @"ar"
#define CLanguageEnglish @"eng"
#define CLocalize(text) [[LocalizationSystem sharedLocalSystem] localizedStringForKey:text value:text]

