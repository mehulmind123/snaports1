//
//  SeasonListViewController.m
//  Snaports
//
//  Created by mac-00018 on 29/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SeasonListViewController.h"
#import "SeasonListTableViewCell.h"

@interface SeasonListViewController ()
{
    NSArray *arrSeasonData;
}

@end

@implementation SeasonListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavigationBarbuttons];
    // Do any additional setup after loading the view from its nib.
    [self.tblSeasonList setEstimatedRowHeight:68];
    [self.tblSeasonList setRowHeight:68];

    [self setSeasonData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Set Navigaiton bar buttons
- (void)setNavigationBarbuttons
{
    
    [self setTitle:CSeasonList];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search_black"]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(showSearchBar)];
    
    self.navigationItem.rightBarButtonItem = rightButton;
    
}

- (void)showSearchBar
{
    if (self.layoutSearchTop.constant<0) {
        self.layoutSearchTop.constant = 0;
    }
    else
    {
        self.layoutSearchTop.constant = -CSearchBarHeight;
    }
}
#pragma mark- TableView Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSeasonData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *strCellId = @"Cell";
    SeasonListTableViewCell *cell = (SeasonListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:strCellId];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"SeasonListTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //Set cicular shape for profile image and rank
    cell.imgProfilePicture.layer.cornerRadius = CViewWidth(cell.imgProfilePicture)/2.0;
    cell.lblRank.layer.cornerRadius = CViewWidth(cell.lblRank)/2.0;
    cell.viwPoints.layer.cornerRadius = CViewHeight(cell.viwPoints)/2.0;
    
    cell.imgProfilePicture.layer.masksToBounds = YES;
    cell.lblRank.layer.masksToBounds = YES;
    
    //Set white border for rank
    cell.lblRank.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.lblRank.layer.borderWidth = 1.0;
    
    NSDictionary *dictAtIndexPath = [arrSeasonData objectAtIndex:indexPath.row];
    
    //Set rank
    cell.lblRank.text = [dictAtIndexPath valueForKey:@"Rank"];
    
    //Set the name for season list users
    cell.lblName.text = [dictAtIndexPath valueForKey:@"Name"];
    
    //Set points for user
    cell.lblPoints.text = [NSString stringWithFormat:@"%@", [dictAtIndexPath valueForKey:@"Points"]];
    
    //Set profile image
    cell.imgProfilePicture.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d.jpg", indexPath.row%6]];
    
    return cell;
    
}


#pragma mark - Search bar delegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}



- (void)setSeasonData
{
    arrSeasonData = @[
                      @{
                          @"PorofileImage":@"",
                          @"Name":@"Lionel Messi",
                          @"Rank":@"1",
                          @"Points":@"450"
                          },
                      @{
                          @"PorofileImage":@"",
                          @"Name":@"Christiano Ronaldo",
                          @"Rank":@"2",
                          @"Points":@"440"
                          },
                      @{
                          @"PorofileImage":@"",
                          @"Name":@"Neymar Jr.",
                          @"Rank":@"3",
                          @"Points":@"400"
                          },
                      @{
                          @"PorofileImage":@"",
                          @"Name":@"Gareth Bale",
                          @"Rank":@"4",
                          @"Points":@"390"
                          },
                      @{
                          @"PorofileImage":@"",
                          @"Name":@"Wayne Rooney",
                          @"Rank":@"5",
                          @"Points":@"380"
                          },
                      @{
                          @"PorofileImage":@"",
                          @"Name":@"Luis Suarez",
                          @"Rank":@"6",
                          @"Points":@"375"
                          },
                      @{
                          @"PorofileImage":@"",
                          @"Name":@"Karim Benzema",
                          @"Rank":@"7",
                          @"Points":@"350"
                          },
                      @{
                          @"PorofileImage":@"",
                          @"Name":@"Gerard Pique",
                          @"Rank":@"8",
                          @"Points":@"320"
                          }
                    ];
}
@end
