//
//  SuperViewController.h
//  ChamRDV
//
//  Created by mac-0005 on 12/21/13.
//  Copyright (c) 2013 MIND INVENTORY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "SuperNavigationController.h"



@interface SuperViewController : UIViewController 
{
    
    UITableView *superTable;
    UICollectionView *superCollection;
    
    UILabel *updateLabel;
    UILabel *lastUpdateLabel;
    UIImageView *updateImageView;
    UIActivityIndicatorView *spinner;
    BOOL pullToRefresh;
    BOOL shouldUpdate;
    BOOL shouldPlaySound;
    NSOperationQueue *queue;
    float offset;
    BOOL isUpdating;
    
    AVAudioPlayer *audio;
    CGFloat lastScrollOffset;
    BOOL dragging;
    NSString *fileStorageName;
    UIScrollView *scrollNavigationHide;

}

- (void)scrollViewDidScroll:(UIScrollView *)sender;

@end
