//
//  ForgotPasswordViewController.h
//  Snaports
//
//  Created by mac-00018 on 23/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SuperViewController.h"

@interface ForgotPasswordViewController : SuperViewController
- (IBAction)btnSendResetCode:(id)sender;
@property (weak, nonatomic) IBOutlet CommonTextField *txtEmail;

@end
