//
//  EventsLiveTableViewCell.h
//  Snaports
//
//  Created by mac-00018 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *CollectionViewCellIdentifier = @"CollectionViewCellIdentifier";

@interface EventsLiveTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viwChannels;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionviwChannels;
@property (weak, nonatomic) IBOutlet UIView *viwRecentLive;
@property (weak, nonatomic) IBOutlet UIView *viwLIVE;
@property (weak, nonatomic) IBOutlet UIImageView *imgRecentLive;
@property (weak, nonatomic) IBOutlet UILabel *lblScoreTeamA;
@property (weak, nonatomic) IBOutlet UILabel *lblScoreTeamB;
@property (weak, nonatomic) IBOutlet UILabel *lblTeamB;
@property (weak, nonatomic) IBOutlet UILabel *lblTeamA;
@property (weak, nonatomic) IBOutlet UILabel *lblMatchStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblMatchDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblTeamNameA;
@property (weak, nonatomic) IBOutlet UILabel *lblTeamNameB;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogoA;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogoB;

@end
