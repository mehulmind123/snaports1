//
//  PictureDescriptionViewController.m
//  Snaports
//
//  Created by mac-00018 on 28/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "PictureDescriptionViewController.h"

@interface PictureDescriptionViewController ()

@end

@implementation PictureDescriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Add Swipe gesture only if details are available
    UISwipeGestureRecognizer *mSwipeUpRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissWebViw)];
    mSwipeUpRecognizer.direction = (UISwipeGestureRecognizerDirectionDown);
    [self.view addGestureRecognizer:mSwipeUpRecognizer];

    self.webviwDesciption.scrollView.delegate = self;
    [self.webviwDesciption loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.strURL]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (self.webviwDesciption.scrollView.contentOffset.y <= -100) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }

}

- (void)dismissWebViw
{
    if (self.webviwDesciption.scrollView.contentOffset.y <= 0) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
#pragma mark - Button Click Event
- (IBAction)btnBackClicked:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
