//
//  EventActivityTableViewCell.m
//  Snaports
//
//  Created by mac-0004 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "EventActivityTableViewCell.h"

@implementation EventActivityTableViewCell
@synthesize viewImgOuter,imgProfile;
- (void)awakeFromNib {
    [super awakeFromNib];
    if (appDelegate.configureFlipToMirror) {
        appDelegate.configureFlipToMirror(self.shadowView, true);
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    viewImgOuter.layer.cornerRadius = viewImgOuter.frame.size.width/2;
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width/2;
    imgProfile.clipsToBounds = true;
    viewImgOuter.clipsToBounds = true;
    viewImgOuter.layer.borderColor = [[UIColor colorWithWhite:0.5 alpha:0.08] CGColor];
    viewImgOuter.layer.borderWidth = 1;
    
}
@end
