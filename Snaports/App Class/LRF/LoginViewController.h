//
//  LoginViewController.h
//  Snaports
//
//  Created by mac-00018 on 22/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SuperViewController.h"

@interface LoginViewController : SuperViewController

@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UIButton *btnLogIn;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblSignUp;
@property (weak, nonatomic) IBOutlet CommonTextField *txtEmail;
@property (weak, nonatomic) IBOutlet CommonTextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIView *viwSignUp;

#pragma mark - Button Click Event
- (IBAction)btnForgotpasswordClicked:(id)sender;
- (IBAction)btnLogInClicked:(id)sender;
- (IBAction)btnSignUpClicked:(id)sender;

@end
