//
//  EventDetailsCollectionViewCell.m
//  Snaports
//
//  Created by mac-00018 on 27/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "EventDetailsCollectionViewCell.h"

@implementation EventDetailsCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    if (appDelegate.configureFlipToMirror) {
        appDelegate.configureFlipToMirror(self.viwForRotation, true);
        appDelegate.configureFlipToMirror(self.viwTitle, false);

    }

}

@end
