//
//  CameraViewController.h
//  Snaports
//
//  Created by mac-0004 on 29/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "MBCircularProgressBarView.h"
#import "TouchDetector.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h> // needed for video types

typedef AVCaptureDevice *(^getCaptureDevice)(AVCaptureDevicePosition Position);
typedef void(^showAlert)(NSString *strTitle, NSString *strMsg, UIViewController *vc);

@interface CameraViewController : UIViewController<UIGestureRecognizerDelegate>
{
    BOOL WeAreRecording;
    
    AVCaptureSession *CaptureSession;
    AVCaptureMovieFileOutput *MovieFileOutput;
    AVCaptureDeviceInput *VideoInputDevice;
    
    NSURL *outputURL;
    IBOutlet MBCircularProgressBarView *viewbtnRecord;
    IBOutlet UIView *viewInside,*viewTopShadow;
    IBOutlet UIButton *btnFlashOnOF,*btnSwichCamera,*btnSetting;
    
    NSTimer *timerProgress;
    
    NSInteger isFrontCamera;
    
}

@property AVAssetExportSession *assetExport;
@property(nonatomic, retain) AVCaptureStillImageOutput *stillImageOutput;
@property (retain) AVCaptureVideoPreviewLayer *PreviewLayer;

@property (nonatomic, copy) getCaptureDevice configureGetCaptureDevice;
@property (nonatomic, copy) showAlert configureShowAlert;

- (void) CameraSetOutputProperties;
- (AVCaptureDevice *)cameraWithPosition:(AVCaptureDevicePosition)position;
- (IBAction)CameraToggleButtonPressed:(id)sender;


@end
