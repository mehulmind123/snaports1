//
//  UIViewController+GetCountryInfo.m
//  Coumtry-List Demo
//
//  Created by mac-0003 on 04/12/14.
//  Copyright (c) 2014 MInd-MAC Mini 2. All rights reserved.
//

#import "UIViewController+GetCountryInfo.h"


static NSArray *arrCountries = nil;


@implementation UIViewController (GetCountryInfo)

+(NSArray*)getAllCountries
{
    return [self LoadDataFromPlist];
}

-(NSArray*)getAllCountries
{
    return [[self class] getAllCountries];
}

+(NSDictionary*)getCountryByCode:(NSString*)strCode
{
    return [[[self LoadDataFromPlist] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Code == %@", strCode]] firstObject];
}

+(NSDictionary*)getCountryByPhoneCode:(NSString*)strCode
{
    return [[[self LoadDataFromPlist] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"PhoneCode == %@", strCode]] firstObject];
}

-(NSDictionary*)getCountryByCode:(NSString*)strCode
{
    return [[self class] getCountryByCode:strCode];
}

+(NSDictionary*)getCountryByName:(NSString*)name
{
    return [[[self LoadDataFromPlist] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Name == %@", name]] firstObject];
}

-(NSDictionary*)getCountryByName:(NSString*)name
{
    return [[[[self class] LoadDataFromPlist] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Name == %@", name]] firstObject];
}


+(NSArray*)LoadDataFromPlist
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSData *plistXML;
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"plist"]])
            plistXML = [[NSFileManager defaultManager] contentsAtPath:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"plist"]];
        else
            plistXML = [[NSFileManager defaultManager] contentsAtPath:[[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"Countries" ofType:@"bundle"]] pathForResource:@"countries" ofType:@"plist"]];
        
        
        NSString *errorDesc = nil;
        NSError *error = nil;
        NSPropertyListFormat format;
        
        
        if ((((NSUInteger)[[[UIDevice currentDevice] systemVersion] doubleValue])>=8))
        {
            arrCountries = (NSArray *)[NSPropertyListSerialization propertyListWithData:plistXML options:0 format:&format error:&error];
        }
        else
        {
            arrCountries = (NSArray *)[NSPropertyListSerialization propertyListFromData:plistXML mutabilityOption:NSPropertyListMutableContainersAndLeaves format:&format errorDescription:&errorDesc];
        }
    });

    return [arrCountries sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"Name" ascending:YES]]];
}

+(NSDictionary *)getCurrentCountryDetails
{
    NSString *code = [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
    
    if (!code)
        code = [[NSLocale systemLocale] objectForKey:NSLocaleCountryCode];

    if (!code)
        code = @"US";
    
      return  [self getCountryByCode:code];
}

-(NSDictionary *)getCurrentCountryDetails
{
    return  [[self class] getCurrentCountryDetails];
}

@end
