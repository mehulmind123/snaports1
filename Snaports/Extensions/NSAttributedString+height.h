#import <CoreText/CoreText.h>
#import <Foundation/Foundation.h>



@interface NSAttributedString (Height)
-(CGFloat)boundingHeightForWidth:(CGFloat)inWidth;
@end