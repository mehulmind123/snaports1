//
//  EventsCollectionViewCell.m
//  Snaports
//
//  Created by mac-00018 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "EventsCollectionViewCell.h"

@implementation EventsCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    if (appDelegate.configureFlipToMirror) {
        appDelegate.configureFlipToMirror(self.contentView, true);
    }
    
}

@end
