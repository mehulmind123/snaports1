//
//  shadowView.m
//  GreenKart
//
//  Created by mac-0004 on 18/03/16.
//  Copyright © 2016 mac-00012. All rights reserved.
//

#import "shadowView.h"

@implementation shadowView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.backgroundColor =[UIColor whiteColor];
    self.layer.cornerRadius = 3;
    self.layer.shadowColor = [UIColor grayColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(1, 1);
    self.layer.shadowOpacity = .4f;

}
@end
