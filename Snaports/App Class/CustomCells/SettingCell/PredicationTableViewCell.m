//
//  PredicationTableViewCell.m
//  Snaports
//
//  Created by mac-0004 on 26/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "PredicationTableViewCell.h"

@implementation PredicationTableViewCell
@synthesize viewImgOuter,imgProfile;

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initialcode];
    if (appDelegate.configureFlipToMirror) {
        //@property IBOutlet UIView *viewTeam1,*ViewTeam2,*viewDraw,*viewTeam1Ins,*ViewTeam2Ins,*viewDrawIns;

//        appDelegate.configureFlipToMirror(self.viewTeam1, true);
        appDelegate.configureFlipToMirror(self.viwContent, true);
//        appDelegate.configureFlipToMirror(self.viewTeam1Ins, true);
        appDelegate.configureFlipToMirror(self.viwPointsBar, false);
        appDelegate.configureFlipToMirror(self.viewTeam1, false);
        appDelegate.configureFlipToMirror(self.viewDraw, false);
        appDelegate.configureFlipToMirror(self.ViewTeam2, false);

    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
-(void)initialcode
{
    [self setBorderToView:_viewDraw andInsideView:_viewDrawIns];
    [self setBorderToView:_viewTeam1 andInsideView:_viewTeam1Ins];
    [self setBorderToView:_ViewTeam2 andInsideView:_ViewTeam2Ins];
    
}
-(void)setBorderToView:(UIView *)viewOuter andInsideView:(UIView *)insideView
{
    viewOuter.layer.cornerRadius = viewOuter.frame.size.height/2;
    viewOuter.layer.borderWidth = 1;
    viewOuter.layer.borderColor = insideView.backgroundColor.CGColor;
    viewOuter.clipsToBounds = true;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    viewImgOuter.layer.cornerRadius = viewImgOuter.frame.size.width/2;
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width/2;
    imgProfile.clipsToBounds = true;
    viewImgOuter.clipsToBounds = true;
    viewImgOuter.layer.borderColor = [[UIColor colorWithWhite:0.5 alpha:0.08] CGColor];
    viewImgOuter.layer.borderWidth = 1;
    
}

@end
