//
//  NSString+Extension.m
//  Master
//
//  Created by mac-0001 on 12/12/14.
//  Copyright (c) 2014 mac-0001. All rights reserved.
//

#import "NSString+Extension.h"

#import "Master.h"


#import "NSObject+NewProperty.h"


@implementation NSString (Extension)

-(void)setApplicationLifecycleComplete
{
    [[UIApplication sharedApplication] setBoolean:YES forKey:self];
}

-(void)setInstallationLifecycleComplete
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:self];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(void)performBlockForApplicaitonLifecycle:(Block)block
{
    [self performBlockForApplicaitonLifecycleWithoutAutoComplete:block];
    [self setApplicationLifecycleComplete];
}

-(void)performBlockForInstallationLifecycle:(Block)block
{
    [self performBlockForInstallationLifecycleWithoutAutoComplete:block];
    [self setInstallationLifecycleComplete];
}

-(void)performBlockForApplicaitonLifecycleWithoutAutoComplete:(Block)block
{
    if(![[UIApplication sharedApplication] booleanForKey:self])
        block();
}

-(void)performBlockForInstallationLifecycleWithoutAutoComplete:(Block)block
{
    if(![[NSUserDefaults standardUserDefaults] boolForKey:self])
        block();
}


@end
