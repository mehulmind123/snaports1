//
//  ForgotPasswordViewController.m
//  Snaports
//
//  Created by mac-00018 on 23/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "ResetPasswordViewController.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"FORGOT PASSWORD";
    if (appDelegate.configureFlipToMirror) {
        appDelegate.configureFlipToMirror(self.view, true);
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button click event

- (IBAction)btnSendResetCode:(id)sender {
    
    if(![self.txtEmail.text isBlankValidationPassed])
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageEmail withTitle:@""];
    else if(![self.txtEmail.text isValidEmailAddress])
        [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageValidEmail withTitle:@""];
    else
    {
        ResetPasswordViewController *vcResetPassword = [[ResetPasswordViewController alloc] initWithNibName:@"ResetPasswordViewController" bundle:nil];
        [self.navigationController pushViewController:vcResetPassword animated:YES];

    }

}
@end
