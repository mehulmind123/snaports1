//
//  PredicationViewController.h
//  Snaports
//
//  Created by mac-0004 on 27/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SuperViewController.h"

@interface PredicationViewController : SuperViewController<UIGestureRecognizerDelegate>
{
    IBOutlet UITableView *tblPredicatoin;
    IBOutlet UILabel *lblPredictionMessage;
    IBOutlet NSLayoutConstraint *layoutTblTopSpacing;
    IBOutlet NSLayoutConstraint *layoutLblProportionalHeight;
}
@end
