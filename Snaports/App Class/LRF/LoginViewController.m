//
//  LoginViewController.m
//  Snaports
//
//  Created by mac-00018 on 22/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "LoginViewController.h"
#import "ForgotPasswordViewController.h"
#import "RegisterViewController.h"
#import "CameraViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (appDelegate.configureFlipToMirror) {
        appDelegate.configureFlipToMirror(self.view, true);
        appDelegate.configureFlipToMirror(self.viwSignUp, false);
    }
    
    NSString *strTextView = CAskSignUp;
    NSRange range = [strTextView rangeOfString:CBoldSignUp];
    
    NSMutableAttributedString *strBlackSignUp = [[NSMutableAttributedString alloc] initWithString:strTextView];
    [strBlackSignUp addAttributes:@{
                                    NSForegroundColorAttributeName : [UIColor blackColor]
                                    } range:range];
    
    self.lblSignUp.attributedText = strBlackSignUp;


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;

}

#pragma mark - Button Click Action

//Set button Action for Forgot Password
- (IBAction)btnForgotpasswordClicked:(id)sender {
    

    ForgotPasswordViewController *vcForgotPassword = [[ForgotPasswordViewController alloc] initWithNibName:@"ForgotPasswordViewController" bundle:nil];
    [self.navigationController pushViewController:vcForgotPassword animated:YES];

}

//Set button Action for Log In
- (IBAction)btnLogInClicked:(id)sender {
    
    if (IS_IPHONE_SIMULATOR) {
        CameraViewController *vcCameraController = [[CameraViewController alloc] initWithNibName:@"CameraViewController" bundle:nil];
        
        [self.navigationController pushViewController:vcCameraController animated:YES];

    }
    else
    {
        if(![self.txtEmail.text isBlankValidationPassed])
            [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageEmail withTitle:@""];
        else if(![self.txtEmail.text isValidEmailAddress])
            [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageValidEmail withTitle:@""];
        else if(![self.txtPassword.text isBlankValidationPassed])
            [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessagePassword withTitle:@""];
        else if (![self.txtPassword.text isValidPassword])
        {
            [[PPAlerts sharedAlerts] showAlertWithType:0 withMessage:CMessageValidPassword withTitle:@""];
            
        }
        else
        {
            CameraViewController *vcCameraController = [[CameraViewController alloc] initWithNibName:@"CameraViewController" bundle:nil];
            
            [self.navigationController pushViewController:vcCameraController animated:YES];
            
        }

    }
}

//Set button Action for Sign Up
- (IBAction)btnSignUpClicked:(id)sender {
    
    RegisterViewController *vcRegister = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:vcRegister animated:YES];

}
@end
