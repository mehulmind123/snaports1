//
//  UILabel+Extension.m
//  Master
//
//  Created by mac-0001 on 12/06/15.
//  Copyright (c) 2015 mac-0001. All rights reserved.
//

#import "UILabel+Extension.h"

#import "NSObject+NewProperty.h"

static NSString *const TTTATTRIBUTEDLABELCLICKHANDLER = @"blockKey";

@interface NSObject ()

-(void)setDelegate:(id)delegate;

-(void)addLinkWithTextCheckingResult:(NSTextCheckingResult*)match attributes:(NSDictionary*)attributes;

@end


static NSRegularExpression *hashtagExpression = nil;


@implementation UILabel (Extension)

+(void)load
{
    [super load];
    
    if(!hashtagExpression)
        hashtagExpression = [NSRegularExpression regularExpressionWithPattern:@"(?:^|\\s)(#\\w+|@\\w+)" options:NO error:nil];
}

-(void)detectHashTagAndUserTag:(void(^)(NSString *string))block;
{
    if (![self isKindOfClass:NSClassFromString(@"TTTAttributedLabel")])
        NSAssert(nil, @"This will support only  TTTAttributedLabel.");
    
    
    NSString *string = self.text;
    NSArray *matches = [hashtagExpression matchesInString:string options:0 range:NSMakeRange(0, string.length)];
    
    
    for (NSTextCheckingResult *match in matches)
    {
        [self addLinkWithTextCheckingResult:match attributes:nil];
    }
    
    self.userInteractionEnabled = YES;
    [self setDelegate:self];
    
    [self setObject:block forKey:TTTATTRIBUTEDLABELCLICKHANDLER];
}

- (void)attributedLabel:(UILabel *)label didSelectLinkWithTextCheckingResult:(NSTextCheckingResult *)result
{
    NSString *absoluteString = [label.text substringWithRange:result.range];
    
    void(^handler)(NSString *string) = [self objectForKey:TTTATTRIBUTEDLABELCLICKHANDLER];
    
    if (handler)
        handler(absoluteString);
}



@end


// Update SetText: method so, new url scanning can be applied to automatically when new text is set and only when different text is set.
// So, method will renamed to register HastageAndUserTag detector.