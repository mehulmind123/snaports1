//
//  CommonTextField.m
//  Snaports
//
//  Created by mac-00018 on 23/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "CommonTextField.h"

@implementation CommonTextField

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.placeholder = CLocalize(self.placeholder);
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    [self addBorder:UIPositionBottom color:CTextFieldBorderColor width:1];
    if (self.tag == 99)
        self.font = CFontGilroyMedium(16);
    else
        self.font = CFontSolidoMedium(20);
    NSLog(@"0-----%@",self.placeholder);
}

@end
