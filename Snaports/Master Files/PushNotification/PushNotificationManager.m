//
//  PushNotificationManager.m
//  MI API Example
//
//  Created by mac-0001 on 25/11/14.
//  Copyright (c) 2014 MI. All rights reserved.
//

#import "PushNotificationManager.h"
#import "Master.h"
#import "NSObject+NewProperty.h"


//#import <Parse/Parse.h>

@interface NSObject ()


// ParseCrashReporting

+ (void)enable;



// Parse

+ (void)setApplicationId:(NSString *)applicationId clientKey:(NSString *)clientKey;



// PFAnalytics

+ (void)trackAppOpenedWithLaunchOptionsInBackground:(NSDictionary *)launchOptions block:(id)block;



// PFInstallation

+ (instancetype)currentInstallation;

@property (nonatomic, strong) NSString *deviceToken;

@property (nonatomic, assign) NSInteger badge;

@property (nonatomic, strong) NSArray *channels;

- (void)setDeviceTokenFromData:(NSData *)deviceTokenData;




// PFObject

- (void)saveEventually;

- (void)removeObjectForKey:(NSString *)key;



// PFQuery

-(id)queryWithClassName:(NSString *)className;

- (void)whereKey:(NSString *)key equalTo:(id)object;

- (void)findObjectsInBackgroundWithBlock:(id)block;

@end




@interface NSObject ()

- (void)applicationDidReceiveRemoteNotification:(NSDictionary *)userInfo fromState:(UIApplicationState)state;

@end





@interface PushNotificationManager ()
{
    Class PFInstallation;
    Class PFQuery;
    
    NSMutableArray *arrNotificationOnservers;
}
@end


@implementation PushNotificationManager

+ (instancetype)sharedInstance
{
    static PushNotificationManager *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        _sharedInstance = [[PushNotificationManager alloc] init];
        
        if ([[UIApplication sharedApplication] objectForKey:@"ParseApplicationId"] && [[UIApplication sharedApplication] objectForKey:@"ParseClientKey"])
        {
            if (!IS_IPHONE_SIMULATOR)
            {
                if (NSClassFromString(@"Parse"))
                {
                    [NSClassFromString(@"Parse") setApplicationId:[[UIApplication sharedApplication] objectForKey:@"ParseApplicationId"] clientKey:[[UIApplication sharedApplication] objectForKey:@"ParseClientKey"]];
                    
                    _sharedInstance->PFInstallation = NSClassFromString(@"PFInstallation");
                    _sharedInstance->PFQuery = NSClassFromString(@"PFQuery");

                }
            }
        }
    });
    
    return _sharedInstance;
}

-(NSMutableArray *)arrNotificationOnservers
{
    if (!arrNotificationOnservers)
        arrNotificationOnservers = [[NSMutableArray alloc] init];

    return arrNotificationOnservers;
}

#pragma mark - Appdelegate Handlers

//#ifdef PARSE_DEPRECATED

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    [self registerForPushNotification];

    if ([[UIApplication sharedApplication] objectForKey:@"ParseApplicationId"] && [[UIApplication sharedApplication] objectForKey:@"ParseClientKey"])
    {
        if (!IS_IPHONE_SIMULATOR)
            if (NSClassFromString(@"PFAnalytics"))
            {
                [NSClassFromString(@"PFAnalytics") trackAppOpenedWithLaunchOptionsInBackground:launchOptions block:nil];
            }
    }

    

    if ([launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey])
    {
        NSDictionary *dic = (NSDictionary *) [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        [self actionOnPushNotification:dic withLaunch:YES];
    }

    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken
{
    NSString *tokenString = [[[newDeviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"Push Notification tokenstring is %@",tokenString);
    
    [UIApplication setDeviceToken:tokenString];
    
    if ([[UIApplication sharedApplication] objectForKey:@"ParseApplicationId"] && [[UIApplication sharedApplication] objectForKey:@"ParseClientKey"])
    {
        if (!IS_IPHONE_SIMULATOR) {
            if (NSClassFromString(@"PFInstallation"))
            {
//                if ([UIApplication isDeviceTokenUpdated])
                if (![[[PFInstallation currentInstallation] deviceToken] isEqualToString:tokenString])
                {
                    [[PFInstallation currentInstallation] setDeviceTokenFromData:newDeviceToken];
                    [[PFInstallation currentInstallation] saveEventually];
                }
            }
        }
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [self actionOnPushNotification:userInfo withLaunch:NO];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Push Notification Error %@",error.localizedDescription);
}

//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler
//{
//
//    NSLog(@"didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:");
//
//
//    [UIApplication parseApplicaitonResponse:[userInfo objectForKey:@"aps"]];
//
//    // To-Do call following methods after process completed.
//
//    handler(UIBackgroundFetchResultNewData);
//}

//#endif



#pragma mark - Parse Push Methods


-(void)registerForPushNotification
{
    if (IS_IPHONE_SIMULATOR)
        return;
    
    
    if (!NSClassFromString(@"Parse"))
        NSAssert(nil, @"Please import Parse.framework, Bolts.framework");
    
    
    if (((NSString *)[[UIApplication sharedApplication] objectForKey:@"ParseApplicationId"]).length<=0)
//        NSAssert(nil, @"Add ParseApplicationId using [[UIApplication sharedApplication] setParseApplicationId:]");
        NSLog(@"Add ParseApplicationId using [[UIApplication sharedApplication] setParseApplicationId:]");

    if (((NSString *)[[UIApplication sharedApplication] objectForKey:@"ParseClientKey"]).length<=0)
//        NSAssert(nil, @"Add ParseClientKey using [[UIApplication sharedApplication] setParseClientKey:]");
        NSLog(@"Add ParseClientKey using [[UIApplication sharedApplication] setParseClientKey:]");
    
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else if([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotificationTypes:)])
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:1];
    }
}

#pragma mark - # MI-ParsePush

- (void)addNotificationObserver:(CanNotificaitonHandledWithBlock)observer
{
    if (![[self arrNotificationOnservers] containsObject:observer])
        [[self arrNotificationOnservers] addObject:observer];
        
}

- (void)removeNotificationObserver:(CanNotificaitonHandledWithBlock)observer
{
    if ([[self arrNotificationOnservers] containsObject:observer])
        [[self arrNotificationOnservers] removeObject:observer];
}


-(void)actionOnPushNotification:(NSDictionary *)dic withLaunch:(BOOL)launch
{
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    
    if (!launch && [[UIApplication sharedApplication] applicationState]!=UIApplicationStateActive)
        state = UIApplicationStateBackground;
        
    
    BOOL isNotificationHandledWithBlock = NO;
    
    for (CanNotificaitonHandledWithBlock block in [self arrNotificationOnservers])
    {
        if (isNotificationHandledWithBlock)
            block(dic,state);
        else
            isNotificationHandledWithBlock = block(dic,state);
    }

    if (isNotificationHandledWithBlock)
        return;
    
    id delegate = [[UIApplication sharedApplication] delegate];
    
    if ([delegate canPerformAction:@selector(applicationDidReceiveRemoteNotification:fromState:) withSender:nil])
        [delegate applicationDidReceiveRemoteNotification:dic fromState:state];
    else
        NSLog(@"Implment - (void)applicationDidReceiveRemoteNotification:(NSDictionary *)userInfo fromState:(UIApplicationState)state on AppDelegate");
}

-(void)refreshChannelsWithInsert
{
    if ([[UIApplication sharedApplication] objectForKey:@"ParseApplicationId"] && [[UIApplication sharedApplication] objectForKey:@"ParseClientKey"])
    {
        if (!IS_IPHONE_SIMULATOR)
        {
            if (NSClassFromString(@"PFInstallation"))
            {
                if ([UIApplication userId])
                    [[PFInstallation currentInstallation] setValue:[UIApplication userId] forKey:@"userID"];
                else
                    [[PFInstallation currentInstallation] removeObjectForKey:@"userID"];
                    
                [[PFInstallation currentInstallation] saveEventually];
            }
        }
    }
}


-(void)incrementBadge
{
    [self setBadge:[[UIApplication sharedApplication] applicationIconBadgeNumber]+1];
}

-(void)decrementBadge
{
    [self setBadge:[[UIApplication sharedApplication] applicationIconBadgeNumber]-1];
}

-(void)setBadge:(NSInteger)badge
{
    if (badge<0)
        badge = 0;
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badge];
    
    if ([[UIApplication sharedApplication] objectForKey:@"ParseApplicationId"] && [[UIApplication sharedApplication] objectForKey:@"ParseClientKey"])
    {
        if (!IS_IPHONE_SIMULATOR)
        {
            if (NSClassFromString(@"PFInstallation"))
            {
                if (![UIApplication userId])
                {
                    [[PFInstallation currentInstallation] setBadge:badge];
                    [[PFInstallation currentInstallation] saveEventually];
                }
                else
                {
                    // This is due to bug in parse, when same user is logged in multiple devices than parse mixing badge numbers while sending push notificaiotns.
                    // So, following code is to prevent that bug
                    
                    id query = [PFQuery queryWithClassName:@"PFInstallation"];
                    [query whereKey:@"userID" equalTo:[UIApplication userId]];
                    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                        for (id object in objects)
                        {
                            [object setBadge:badge];
                            [object saveEventually];
                        }
                    }];
                }
            }
        }
    }
}

@end
