//
//  UIViewController+GetCountryInfo.h
//  Coumtry-List Demo
//
//  Created by mac-0003 on 04/12/14.
//  Copyright (c) 2014 MInd-MAC Mini 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (GetCountryInfo)

-(NSArray*)getAllCountries;
-(NSDictionary*)getCountryByCode:(NSString*)strCode;
+(NSDictionary*)getCountryByPhoneCode:(NSString*)strCode;
-(NSDictionary*)getCountryByName:(NSString*)name;
-(NSDictionary *)getCurrentCountryDetails;

+(NSArray*)getAllCountries;
+(NSDictionary*)getCountryByCode:(NSString*)strCode;
+(NSDictionary*)getCountryByName:(NSString*)name;
+(NSDictionary *)getCurrentCountryDetails;

@end
