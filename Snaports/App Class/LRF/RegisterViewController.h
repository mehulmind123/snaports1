//
//  RegisterViewController.h
//  Snaports
//
//  Created by mac-00018 on 23/12/16.
//  Copyright © 2016 mac-00018. All rights reserved.
//

#import "SuperViewController.h"
#import "CommonTextField.h"
#import "CommonButton.h"
@interface RegisterViewController : SuperViewController
{
    IBOutlet CommonTextField *txtFirstName,*txtLastName,*txtUserName,*txtEmailAddress,*txtPaasword,*txtConfiPassword;
    IBOutlet UIImageView *imgProfile;
    IBOutlet UILabel *lblAddPicture;
    IBOutlet UIButton *btnProfilePic;
    IBOutlet CommonButton *btnSignUp;
    
    IBOutlet UIView *viwContent;
}
@end
